/*
 * ER/Studio Developer Edition SQL Code Generation
 * Project :      BancoEngenharia.DM1
 *
 * Date Created : Wednesday, October 04, 2017 10:17:25
 * Target DBMS : Microsoft SQL Server 7.x
 */

/* 
 * TABLE: acesso 
 */

CREATE TABLE acesso(
    ac_cod       int            IDENTITY(0,0),
    ac_data      datetime       NULL,
    usr_login    varchar(50)    NOT NULL,
    CONSTRAINT PK10 PRIMARY KEY NONCLUSTERED (ac_cod)
)
go



IF OBJECT_ID('acesso') IS NOT NULL
    PRINT '<<< CREATED TABLE acesso >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE acesso >>>'
go

/* 
 * TABLE: bairro 
 */

CREATE TABLE bairro(
    bai_cod     int             IDENTITY(0,0),
    bai_nome    varchar(100)    NULL,
    cid_cod     int             NOT NULL,
    CONSTRAINT PK9 PRIMARY KEY NONCLUSTERED (bai_cod)
)
go



IF OBJECT_ID('bairro') IS NOT NULL
    PRINT '<<< CREATED TABLE bairro >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE bairro >>>'
go

/* 
 * TABLE: cidade 
 */

CREATE TABLE cidade(
    cid_cod     int             IDENTITY(0,0),
    cid_nome    varchar(100)    NOT NULL,
    est_cod     int             NOT NULL,
    CONSTRAINT PK1 PRIMARY KEY NONCLUSTERED (cid_cod)
)
go



IF OBJECT_ID('cidade') IS NOT NULL
    PRINT '<<< CREATED TABLE cidade >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE cidade >>>'
go

/* 
 * TABLE: cliente 
 */

CREATE TABLE cliente(
    cli_cpf             varchar(50)      NOT NULL,
    cli_rg              varchar(50)      NOT NULL,
    cli_foto            varbinary(18)    NULL,
    cli_nome            varchar(50)      NOT NULL,
    cli_telefone        varchar(20)      NULL,
    cli_email           varchar(50)      NULL,
    cli_endereco        varchar(100)     NULL,
    cli_obs             varchar(500)     NULL,
    cli_datacadastro    datetime         NULL,
    end_cep             varchar(10)      NOT NULL,
    CONSTRAINT PK1_1 PRIMARY KEY NONCLUSTERED (cli_cpf)
)
go



IF OBJECT_ID('cliente') IS NOT NULL
    PRINT '<<< CREATED TABLE cliente >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE cliente >>>'
go

/* 
 * TABLE: compra 
 */

CREATE TABLE compra(
    com_cod              int               IDENTITY(0,0),
    compra_valor         decimal(10, 2)    NULL,
    compra_quantLotes    int               NULL,
    usr_login            varchar(50)       NOT NULL,
    for_cnpj             varchar(50)       NOT NULL,
    CONSTRAINT PK5 PRIMARY KEY NONCLUSTERED (com_cod)
)
go



IF OBJECT_ID('compra') IS NOT NULL
    PRINT '<<< CREATED TABLE compra >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE compra >>>'
go

/* 
 * TABLE: empresa 
 */

CREATE TABLE empresa(
    emp_razaosocial    varchar(50)      NOT NULL,
    emp_endereco       varchar(50)      NOT NULL,
    emp_telefone       varchar(10)      NOT NULL,
    emp_nome           varchar(50)      NOT NULL,
    emp_cnpj           varchar(50)      NOT NULL,
    emp_logo           varbinary(18)    NULL
)
go



IF OBJECT_ID('empresa') IS NOT NULL
    PRINT '<<< CREATED TABLE empresa >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE empresa >>>'
go

/* 
 * TABLE: endereco 
 */

CREATE TABLE endereco(
    end_cep      varchar(10)     NOT NULL,
    end_local    varchar(100)    NOT NULL,
    bai_cod      int             NOT NULL,
    CONSTRAINT PK8 PRIMARY KEY NONCLUSTERED (end_cep)
)
go



IF OBJECT_ID('endereco') IS NOT NULL
    PRINT '<<< CREATED TABLE endereco >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE endereco >>>'
go

/* 
 * TABLE: estado 
 */

CREATE TABLE estado(
    est_cod     int             IDENTITY(0,0),
    est_uf      char(2)         NOT NULL,
    est_nome    varchar(100)    NOT NULL,
    pais_cod    int             NOT NULL,
    CONSTRAINT PK2 PRIMARY KEY NONCLUSTERED (est_cod)
)
go



IF OBJECT_ID('estado') IS NOT NULL
    PRINT '<<< CREATED TABLE estado >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE estado >>>'
go

/* 
 * TABLE: fabricante 
 */

CREATE TABLE fabricante(
    fab_cnpj        varchar(50)      NOT NULL,
    fab_foto        varbinary(18)    NULL,
    fab_obs         varchar(255)     NULL,
    fab_email       varchar(100)     NULL,
    fab_nome        varchar(50)      NULL,
    fab_telefone    varchar(50)      NULL,
    end_cep         varchar(10)      NOT NULL,
    CONSTRAINT PK14 PRIMARY KEY NONCLUSTERED (fab_cnpj)
)
go



IF OBJECT_ID('fabricante') IS NOT NULL
    PRINT '<<< CREATED TABLE fabricante >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE fabricante >>>'
go

/* 
 * TABLE: fornecedor 
 */

CREATE TABLE fornecedor(
    for_cnpj        varchar(50)      NOT NULL,
    for_email       varchar(50)      NULL,
    for_obs         varchar(255)     NULL,
    for_foto        varbinary(18)    NULL,
    for_nome        varchar(50)      NOT NULL,
    for_telefone    varchar(50)      NULL,
    end_cep         varchar(10)      NOT NULL,
    CONSTRAINT PK7_1 PRIMARY KEY NONCLUSTERED (for_cnpj)
)
go



IF OBJECT_ID('fornecedor') IS NOT NULL
    PRINT '<<< CREATED TABLE fornecedor >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE fornecedor >>>'
go

/* 
 * TABLE: lote 
 */

CREATE TABLE lote(
    lot_cod                int         IDENTITY(0,0),
    prod_cod               int         NOT NULL,
    lote_estoqueinicial    int         NOT NULL,
    lote_estoque           int         NULL,
    lote_validade          datetime    NULL,
    CONSTRAINT PK2_1 PRIMARY KEY NONCLUSTERED (lot_cod, prod_cod)
)
go



IF OBJECT_ID('lote') IS NOT NULL
    PRINT '<<< CREATED TABLE lote >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE lote >>>'
go

/* 
 * TABLE: Mensagem 
 */

CREATE TABLE Mensagem(
    mens_cod    int             IDENTITY(1,1),
    Mensagem    varchar(255)    NULL,
    prod_cod    int             NOT NULL,
    cli_cpf     varchar(50)     NOT NULL,
    CONSTRAINT PK66 PRIMARY KEY NONCLUSTERED (mens_cod)
)
go



IF OBJECT_ID('Mensagem') IS NOT NULL
    PRINT '<<< CREATED TABLE Mensagem >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Mensagem >>>'
go

/* 
 * TABLE: nivel 
 */

CREATE TABLE nivel(
    nivel_cod          int            IDENTITY(0,0),
    nivel_nome         varchar(50)    NULL,
    nivel_permissao    char(50)       NULL,
    nivel_visivel      char(2)        NULL,
    CONSTRAINT PK7 PRIMARY KEY NONCLUSTERED (nivel_cod)
)
go



IF OBJECT_ID('nivel') IS NOT NULL
    PRINT '<<< CREATED TABLE nivel >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE nivel >>>'
go

/* 
 * TABLE: Observer 
 */

CREATE TABLE Observer(
    prod_cod    int            NOT NULL,
    cli_cpf     varchar(50)    NOT NULL,
    CONSTRAINT PK67 PRIMARY KEY NONCLUSTERED (prod_cod, cli_cpf)
)
go



IF OBJECT_ID('Observer') IS NOT NULL
    PRINT '<<< CREATED TABLE Observer >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Observer >>>'
go

/* 
 * TABLE: pagamento 
 */

CREATE TABLE pagamento(
    pag_cod      int               IDENTITY(0,0),
    pag_data     datetime          NULL,
    pag_valor    decimal(10, 2)    NULL,
    com_cod      int               NOT NULL,
    CONSTRAINT PK11 PRIMARY KEY NONCLUSTERED (pag_cod)
)
go



IF OBJECT_ID('pagamento') IS NOT NULL
    PRINT '<<< CREATED TABLE pagamento >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE pagamento >>>'
go

/* 
 * TABLE: pais 
 */

CREATE TABLE pais(
    pais_cod     int            IDENTITY(0,0),
    pais_sgl     char(2)        NOT NULL,
    pais_nome    varchar(50)    NOT NULL,
    CONSTRAINT PK3 PRIMARY KEY NONCLUSTERED (pais_cod)
)
go



IF OBJECT_ID('pais') IS NOT NULL
    PRINT '<<< CREATED TABLE pais >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE pais >>>'
go

/* 
 * TABLE: produto 
 */

CREATE TABLE produto(
    prod_cod                   int               IDENTITY(0,0),
    tipo_desc                  varchar(125)      NULL,
    local_desc                 varchar(125)      NULL,
    local_num                  varchar(20)       NULL,
    medida_prod                varchar(125)      NULL,
    prod_foto                  varbinary(18)     NULL,
    prod_nome                  varchar(50)       NOT NULL,
    prod_obs                   varchar(50)       NULL,
    prod_nome_farmacologico    varchar(50)       NULL,
    prod_preco_fabricacao      decimal(10, 2)    NULL,
    prod_preco_comercial       decimal(10, 2)    NULL,
    fab_cnpj                   varchar(50)       NOT NULL,
    CONSTRAINT PK9_1 PRIMARY KEY NONCLUSTERED (prod_cod)
)
go



IF OBJECT_ID('produto') IS NOT NULL
    PRINT '<<< CREATED TABLE produto >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE produto >>>'
go

/* 
 * TABLE: produtos_compra 
 */

CREATE TABLE produtos_compra(
    com_cod     int    NOT NULL,
    prod_cod    int    NOT NULL,
    lot_cod     int    NOT NULL,
    CONSTRAINT PK63 PRIMARY KEY NONCLUSTERED (com_cod, prod_cod)
)
go



IF OBJECT_ID('produtos_compra') IS NOT NULL
    PRINT '<<< CREATED TABLE produtos_compra >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE produtos_compra >>>'
go

/* 
 * TABLE: produtos_venda 
 */

CREATE TABLE produtos_venda(
    venda_cod         int    NOT NULL,
    lot_cod           int    NOT NULL,
    prod_cod          int    NOT NULL,
    produtos_quant    int    NOT NULL,
    CONSTRAINT PK12 PRIMARY KEY NONCLUSTERED (venda_cod, lot_cod, prod_cod)
)
go



IF OBJECT_ID('produtos_venda') IS NOT NULL
    PRINT '<<< CREATED TABLE produtos_venda >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE produtos_venda >>>'
go

/* 
 * TABLE: recebimento 
 */

CREATE TABLE recebimento(
    rec_cod      int               IDENTITY(0,0),
    rec_data     datetime          NULL,
    rec_valor    numeric(10, 2)    NULL,
    venda_cod    int               NOT NULL,
    CONSTRAINT PK10_1 PRIMARY KEY NONCLUSTERED (rec_cod)
)
go



IF OBJECT_ID('recebimento') IS NOT NULL
    PRINT '<<< CREATED TABLE recebimento >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE recebimento >>>'
go

/* 
 * TABLE: usuario 
 */

CREATE TABLE usuario(
    usr_login           varchar(50)       NOT NULL,
    usr_foto            varbinary(18)     NULL,
    usr_senha           int               NULL,
    usr_rg              varchar(50)       NOT NULL,
    usr_email           varchar(50)       NULL,
    usr_cpf             varchar(50)       NOT NULL,
    usr_nome            varchar(50)       NOT NULL,
    usr_obs             varchar(500)      NULL,
    usr_telefone        varchar(20)       NULL,
    usr_endereco        varchar(50)       NULL,
    usr_Salario         decimal(10, 2)    NOT NULL,
    usr_datacadastro    datetime          NULL,
    nivel_cod           int               NOT NULL,
    end_cep             varchar(10)       NOT NULL,
    CONSTRAINT PK4 PRIMARY KEY NONCLUSTERED (usr_login)
)
go



IF OBJECT_ID('usuario') IS NOT NULL
    PRINT '<<< CREATED TABLE usuario >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE usuario >>>'
go

/* 
 * TABLE: venda 
 */

CREATE TABLE venda(
    venda_cod      int               IDENTITY(0,0),
    venda_valor    numeric(10, 2)    NULL,
    cli_cpf        varchar(50)       NOT NULL,
    usr_login      varchar(50)       NOT NULL,
    CONSTRAINT PK3_1 PRIMARY KEY NONCLUSTERED (venda_cod)
)
go



IF OBJECT_ID('venda') IS NOT NULL
    PRINT '<<< CREATED TABLE venda >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE venda >>>'
go

/* 
 * TABLE: acesso 
 */

ALTER TABLE acesso ADD CONSTRAINT Refusuario56 
    FOREIGN KEY (usr_login)
    REFERENCES usuario(usr_login)
go


/* 
 * TABLE: bairro 
 */

ALTER TABLE bairro ADD CONSTRAINT Refcidade55 
    FOREIGN KEY (cid_cod)
    REFERENCES cidade(cid_cod)
go


/* 
 * TABLE: cidade 
 */

ALTER TABLE cidade ADD CONSTRAINT Refestado52 
    FOREIGN KEY (est_cod)
    REFERENCES estado(est_cod)
go


/* 
 * TABLE: cliente 
 */

ALTER TABLE cliente ADD CONSTRAINT Refendereco58 
    FOREIGN KEY (end_cep)
    REFERENCES endereco(end_cep)
go


/* 
 * TABLE: compra 
 */

ALTER TABLE compra ADD CONSTRAINT Refusuario66 
    FOREIGN KEY (usr_login)
    REFERENCES usuario(usr_login)
go

ALTER TABLE compra ADD CONSTRAINT Reffornecedor71 
    FOREIGN KEY (for_cnpj)
    REFERENCES fornecedor(for_cnpj)
go


/* 
 * TABLE: endereco 
 */

ALTER TABLE endereco ADD CONSTRAINT Refbairro54 
    FOREIGN KEY (bai_cod)
    REFERENCES bairro(bai_cod)
go


/* 
 * TABLE: estado 
 */

ALTER TABLE estado ADD CONSTRAINT Refpais51 
    FOREIGN KEY (pais_cod)
    REFERENCES pais(pais_cod)
go


/* 
 * TABLE: fabricante 
 */

ALTER TABLE fabricante ADD CONSTRAINT Refendereco67 
    FOREIGN KEY (end_cep)
    REFERENCES endereco(end_cep)
go


/* 
 * TABLE: fornecedor 
 */

ALTER TABLE fornecedor ADD CONSTRAINT Refendereco68 
    FOREIGN KEY (end_cep)
    REFERENCES endereco(end_cep)
go


/* 
 * TABLE: lote 
 */

ALTER TABLE lote ADD CONSTRAINT Refproduto118 
    FOREIGN KEY (prod_cod)
    REFERENCES produto(prod_cod)
go


/* 
 * TABLE: Mensagem 
 */

ALTER TABLE Mensagem ADD CONSTRAINT RefObserver124 
    FOREIGN KEY (prod_cod, cli_cpf)
    REFERENCES Observer(prod_cod, cli_cpf)
go


/* 
 * TABLE: Observer 
 */

ALTER TABLE Observer ADD CONSTRAINT Refproduto120 
    FOREIGN KEY (prod_cod)
    REFERENCES produto(prod_cod)
go

ALTER TABLE Observer ADD CONSTRAINT Refcliente121 
    FOREIGN KEY (cli_cpf)
    REFERENCES cliente(cli_cpf)
go


/* 
 * TABLE: pagamento 
 */

ALTER TABLE pagamento ADD CONSTRAINT Refcompra93 
    FOREIGN KEY (com_cod)
    REFERENCES compra(com_cod)
go


/* 
 * TABLE: produto 
 */

ALTER TABLE produto ADD CONSTRAINT Reffabricante117 
    FOREIGN KEY (fab_cnpj)
    REFERENCES fabricante(fab_cnpj)
go


/* 
 * TABLE: produtos_compra 
 */

ALTER TABLE produtos_compra ADD CONSTRAINT Refcompra90 
    FOREIGN KEY (com_cod)
    REFERENCES compra(com_cod)
go

ALTER TABLE produtos_compra ADD CONSTRAINT Reflote91 
    FOREIGN KEY (lot_cod, prod_cod)
    REFERENCES lote(lot_cod, prod_cod)
go

ALTER TABLE produtos_compra ADD CONSTRAINT Refproduto116 
    FOREIGN KEY (prod_cod)
    REFERENCES produto(prod_cod)
go


/* 
 * TABLE: produtos_venda 
 */

ALTER TABLE produtos_venda ADD CONSTRAINT Reflote119 
    FOREIGN KEY (lot_cod, prod_cod)
    REFERENCES lote(lot_cod, prod_cod)
go

ALTER TABLE produtos_venda ADD CONSTRAINT Refvenda78 
    FOREIGN KEY (venda_cod)
    REFERENCES venda(venda_cod)
go


/* 
 * TABLE: recebimento 
 */

ALTER TABLE recebimento ADD CONSTRAINT Refvenda84 
    FOREIGN KEY (venda_cod)
    REFERENCES venda(venda_cod)
go


/* 
 * TABLE: usuario 
 */

ALTER TABLE usuario ADD CONSTRAINT Refnivel53 
    FOREIGN KEY (nivel_cod)
    REFERENCES nivel(nivel_cod)
go

ALTER TABLE usuario ADD CONSTRAINT Refendereco57 
    FOREIGN KEY (end_cep)
    REFERENCES endereco(end_cep)
go


/* 
 * TABLE: venda 
 */

ALTER TABLE venda ADD CONSTRAINT Refcliente69 
    FOREIGN KEY (cli_cpf)
    REFERENCES cliente(cli_cpf)
go

ALTER TABLE venda ADD CONSTRAINT Refusuario72 
    FOREIGN KEY (usr_login)
    REFERENCES usuario(usr_login)
go


