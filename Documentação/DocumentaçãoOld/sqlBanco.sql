--
-- ER/Studio Data Architect 10.0 SQL Code Generation
-- Company :      jm
-- Project :      Model1.DM1
-- Author :       hbk
--
-- Date Created : Tuesday, May 02, 2017 02:05:30
-- Target DBMS : PostgreSQL 8.0
--

-- 
-- TABLE: cliente 
--

CREATE TABLE cliente(
    cli_rg          varchar(50)     NOT NULL,
    cli_nome        varchar(50),
    cli_cpf         varchar(50),
    cli_telefone    varchar(20),
    cli_email       varchar(50),
    cli_endereco    varchar(100),
    cli_obs         varchar(500),
    CONSTRAINT "PK1" PRIMARY KEY (cli_rg)
)
;



-- 
-- TABLE: compra 
--

CREATE TABLE compra(
    com_cod              serial            NOT NULL,
    for_cnpj             varchar(50)       NOT NULL,
    usr_cod              int4              NOT NULL,
    compra_valor         decimal(10, 2),
    "compra_quantLotes"  int4,
    CONSTRAINT "PK5" PRIMARY KEY (com_cod, for_cnpj, usr_cod)
)
;



-- 
-- TABLE: fornecedor 
--

CREATE TABLE fornecedor(
    for_cnpj        varchar(50)    NOT NULL,
    for_endereco    varchar(50),
    for_nome        varchar(50),
    for_telefone    varchar(50),
    CONSTRAINT "PK7" PRIMARY KEY (for_cnpj)
)
;



-- 
-- TABLE: lote 
--

CREATE TABLE lote(
    lot_cod                serial         NOT NULL,
    com_cod                int4           NOT NULL,
    prod_cod               int4           NOT NULL,
    for_cnpj               varchar(50)    NOT NULL,
    usr_cod                int4           NOT NULL,
    lote_estoqueinicial    int4           NOT NULL,
    lote_estoque           int4,
    lote_validade          date,
    CONSTRAINT "PK2" PRIMARY KEY (lot_cod, com_cod, prod_cod, for_cnpj, usr_cod)
)
;
create table empresa(	emp_razaosocial varchar(50) null,	emp_endereco varchar(50) null,	emp_telefone varchar(50) null,	emp_nome varchar(50) null,	emp_cnpj varchar(50) null);


-- 
-- TABLE: lotes_venda 
--

CREATE TABLE lotes_venda(
    lotes_cod    serial         NOT NULL,
    lot_cod      int4           NOT NULL,
    com_cod      int4           NOT NULL,
    prod_cod     int4           NOT NULL,
    venda_cod    int4           NOT NULL,
    cli_rg       varchar(50)    NOT NULL,
    for_cnpj     varchar(50)    NOT NULL,
    usr_cod      int4           NOT NULL,
    usr_obs      int4,
    CONSTRAINT "PK12" PRIMARY KEY (lotes_cod, lot_cod, com_cod, prod_cod, venda_cod, cli_rg, for_cnpj, usr_cod)
)
;



-- 
-- TABLE: pagamento 
--

CREATE TABLE pagamento(
    pag_cod      serial            NOT NULL,
    com_cod      int4              NOT NULL,
    for_cnpj     varchar(50)       NOT NULL,
    usr_cod      int4              NOT NULL,
    pag_valor    decimal(10, 2),
    CONSTRAINT "PK11" PRIMARY KEY (pag_cod, com_cod, for_cnpj, usr_cod)
)
;



-- 
-- TABLE: produto 
--

CREATE TABLE produto(
    prod_cod      serial         NOT NULL,
    prod_nome     varchar(50),
    prod_obs      varchar(50),
    prod_marca    varchar(50),
    CONSTRAINT "PK9" PRIMARY KEY (prod_cod)
)
;



-- 
-- TABLE: recebimento 
--

CREATE TABLE recebimento(
    rec_cod      serial            NOT NULL,
    venda_cod    int4              NOT NULL,
    cli_rg       varchar(50)       NOT NULL,
    usr_cod      int4              NOT NULL,
    rec_valor    numeric(10, 2),
    CONSTRAINT "PK10" PRIMARY KEY (rec_cod, venda_cod, cli_rg, usr_cod)
)
;



-- 
-- TABLE: usuario 
--

CREATE TABLE usuario(
    usr_cod         serial            NOT NULL,
    usr_senha       int4,
    usr_rg          varchar(50)       NOT NULL,
    usr_email       varchar(50),
    usr_cpf         varchar(50),
    usr_nome        varchar(50),
    usr_nivel       char(1),
    usr_obs         varchar(500),
    usr_telefone    varchar(20),
    usr_endereco    varchar(50),
    "usr_salario"   decimal(10, 2),
    CONSTRAINT "PK4" PRIMARY KEY (usr_cod)
)
;



-- 
-- TABLE: venda 
--

CREATE TABLE venda(
    venda_cod      serial            NOT NULL,
    cli_rg         varchar(50)       NOT NULL,
    usr_cod        int4              NOT NULL,
    venda_valor    numeric(10, 2),
    CONSTRAINT "PK3" PRIMARY KEY (venda_cod, cli_rg, usr_cod)
)
;



-- 
-- TABLE: compra 
--

ALTER TABLE compra ADD CONSTRAINT "Reffornecedor9" 
    FOREIGN KEY (for_cnpj)
    REFERENCES fornecedor(for_cnpj)
;

ALTER TABLE compra ADD CONSTRAINT "Refusuario19" 
    FOREIGN KEY (usr_cod)
    REFERENCES usuario(usr_cod)
;


-- 
-- TABLE: lote 
--

ALTER TABLE lote ADD CONSTRAINT "Refcompra22" 
    FOREIGN KEY (com_cod, for_cnpj, usr_cod)
    REFERENCES compra(com_cod, for_cnpj, usr_cod)
;

ALTER TABLE lote ADD CONSTRAINT "Refproduto23" 
    FOREIGN KEY (prod_cod)
    REFERENCES produto(prod_cod)
;


-- 
-- TABLE: lotes_venda 
--

ALTER TABLE lotes_venda ADD CONSTRAINT "Reflote32" 
    FOREIGN KEY (lot_cod, com_cod, prod_cod, for_cnpj, usr_cod)
    REFERENCES lote(lot_cod, com_cod, prod_cod, for_cnpj, usr_cod)
;

ALTER TABLE lotes_venda ADD CONSTRAINT "Refvenda33" 
    FOREIGN KEY (venda_cod, cli_rg, usr_cod)
    REFERENCES venda(venda_cod, cli_rg, usr_cod)
;


-- 
-- TABLE: pagamento 
--

ALTER TABLE pagamento ADD CONSTRAINT "Refcompra28" 
    FOREIGN KEY (com_cod, for_cnpj, usr_cod)
    REFERENCES compra(com_cod, for_cnpj, usr_cod)
;


-- 
-- TABLE: recebimento 
--

ALTER TABLE recebimento ADD CONSTRAINT "Refvenda29" 
    FOREIGN KEY (venda_cod, cli_rg, usr_cod)
    REFERENCES venda(venda_cod, cli_rg, usr_cod)
;


-- 
-- TABLE: venda 
--

ALTER TABLE venda ADD CONSTRAINT "Refcliente11" 
    FOREIGN KEY (cli_rg)
    REFERENCES cliente(cli_rg)
;

ALTER TABLE venda ADD CONSTRAINT "Refusuario13" 
    FOREIGN KEY (usr_cod)
    REFERENCES usuario(usr_cod)
;


