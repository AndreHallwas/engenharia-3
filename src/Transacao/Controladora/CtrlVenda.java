/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transacao.Controladora;

import Transacao.Dal.CtrVenda;
import Transacao.Entidade.Venda;
import Utils.Carrinho;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class CtrlVenda extends CtrlTransacao {

    public static CtrlVenda create() {
        return new CtrlVenda();
    }

    private CtrVenda cv;

    private CtrlVenda() {
    }

    @Override
    public boolean cancelar(String Vendacod) {
        transacao = new Venda();
        transacao.CancelarTransacao(Vendacod);
        return transacao.isResultado();
    }

    @Override
    public ArrayList<Object> Pesquisar(String filtro) {
        cv = new CtrVenda();
        ArrayList<Object> Vendas = new ArrayList();
        Vendas.addAll(cv.get(filtro, 1));
        return Vendas;
    }

    @Override
    public boolean registar(Object... Params) {
        String usuario = "";
        String cliente = "";
        String Parcelas = "";
        Carrinho carrinho = null;

        if (Params != null) {
            if (Params[0] != null) {
                usuario = (String) Params[0];
            }
            if (Params[1] != null) {
                cliente = (String) Params[1];
            }
            if (Params[2] != null) {
                Parcelas = (String) Params[2];
            }
            if (Params[3] != null) {
                carrinho = (Carrinho) Params[3];
            }
        }

        transacao = new Venda(usuario, cliente, Parcelas, carrinho);
        transacao.RealizarTransacao();
        return transacao.isResultado();
    }

}
