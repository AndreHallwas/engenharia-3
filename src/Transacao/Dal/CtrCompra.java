package Transacao.Dal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Fornecedor.Entidade.Fornecedor;
import Fornecedor.Controladora.CtrlFornecedor;
import Pessoa.Controladora.CtrlPessoa;
import Pessoa.Entidade.Usuario;
import Transacao.Entidade.Compra;
import Utils.Banco;
import java.sql.ResultSet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;

/**
 *
 * @author Drake
 */
public class CtrCompra {

    public boolean Salvar(Compra c) {
        Alert a;
        boolean flag;
        String sql = null;

        sql = "insert into Compra(for_cnpj, usr_login, compra_valor, compra_quantlotes)"
                + " values('$1', '$2', '$3', '$4')";
        sql = sql.replace("$1", c.getFornecedor().getCNPJ());
        sql = sql.replace("$2", c.getUsuario().getLogin());
        sql = sql.replace("$3", Double.toString(c.getCompra_valor()));
        sql = sql.replace("$4", Integer.toString(c.getCompra_quantLotes()));

        if (Banco.con.manipular(sql)) {
            a = new Alert(Alert.AlertType.INFORMATION);
            a.setContentText("Cadastro Efetuado");
            flag = true;
        } else {
            a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Cadastro Não Concluido Erro: " + Banco.con.getMensagemErro());
            flag = false;
        }
        a.showAndWait();
        return flag;
    }

    public boolean Alterar(Compra c) {
        String sql = null;

        sql = "update Compra set for_cnpj = '$1', usr_cod = '$2', compra_valor = '$3', compra_quantlotes = '$4' where com_cod = '" + c.getCodigo() + "'";
        sql = sql.replace("$1", c.getFornecedor().getCNPJ());
        sql = sql.replace("$2", c.getUsuario().getLogin());
        sql = sql.replace("$3", Double.toString(c.getCompra_valor()));
        sql = sql.replace("$4", Integer.toString(c.getCompra_quantLotes()));

        return Banco.con.manipular(sql);
    }

    public boolean Apagar(Compra c) {
        String sql = "delete from Compra where com_cod = '" + c.getCodigo() + "'";

        Alert a;
        if (Banco.con.manipular(sql)) {
            a = new Alert(Alert.AlertType.INFORMATION);
            a.setContentText("Operação Efetuada com Sucesso");
        } else {
            a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Operação Não Efetuada");
        }
        a.showAndWait();
        return Banco.con.manipular(sql);
    }

    public ObservableList<Compra> getCompra(String filtro, int op) {
        String sql = null;
        ObservableList<Compra> lista = FXCollections.observableArrayList();
        ResultSet rs;
        sql = "select * from Compra";
        if (!filtro.isEmpty()) {
            sql += " where upper(com_cod) like upper('" + filtro + "%')";
        }
        Banco.conectar();
        if (op == 1) {
            rs = Banco.con.consultar(sql);
            try {
                while (rs.next()) {
                    CtrlFornecedor cf = CtrlFornecedor.create();
                    CtrlPessoa cp = CtrlPessoa.create();
                    Fornecedor fornecedor = (Fornecedor) cf.Pesquisar(rs.getString("for_cnpj")).get(0);
                    Usuario usuario = (Usuario) cp.Pesquisar(rs.getString("usr_login"), 1).get(0);
                    lista.add(new Compra(rs.getString("com_cod"), fornecedor, usuario, rs.getDouble("compra_valor"), rs.getInt("compra_quantlotes")));
                }
            } catch (Exception ex) {
                System.out.println("Erro: " + ex.getMessage());
            }
        } else {
        }
        return lista;
    }

    public Compra BuscaCompraCodigo(String Codigo) {
        String sql = null;
        Compra c = null;
        ResultSet rs;
        sql = "select * from Compra";
        if (!Codigo.isEmpty()) {
            sql += " where com_cod = '" + Codigo + "'";
        }
        Banco.conectar();
        rs = Banco.con.consultar(sql);
        try {
            if (rs.next()) {
                CtrlFornecedor cf = CtrlFornecedor.create();
                CtrlPessoa cp = CtrlPessoa.create();
                Fornecedor fornecedor = (Fornecedor) cf.Pesquisar(rs.getString("for_cnpj")).get(0);
                Usuario usuario = (Usuario) cp.Pesquisar(rs.getString("usr_login"), 1).get(0);
                c = new Compra(rs.getString("com_cod"), fornecedor, usuario, rs.getDouble("compra_valor"), rs.getInt("compra_quantlotes"));
            }
        } catch (Exception ex) {
            System.out.println("Erro: " + ex.getMessage());
        }
        return c;
    }
}
