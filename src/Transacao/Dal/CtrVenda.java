package Transacao.Dal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Padroes.Dao.ControledeEntidade;
import Transacao.Entidade.Venda;
import Pessoa.Controladora.CtrlPessoa;
import Pessoa.Entidade.Cliente;
import Pessoa.Entidade.Usuario;
import Utils.Banco;
import Utils.Mensagem;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Drake
 */
public class CtrVenda extends ControledeEntidade
{
 
    public CtrVenda() {
        flag = false;
        pstmt = null;
    }
    
    public boolean Salvar(Venda v)
    {
        pstmt = Utils.Banco.con.geraStatement("insert into Venda(cli_cpf, usr_login, venda_valor) "
                + "values(?,?,?)");

        setParametros(v);
        setMsg(Utils.Banco.con.manipular(pstmt), "Cadastro");

        return flag;
    }

    public boolean Alterar(Venda v)
    {
        pstmt = Utils.Banco.con.geraStatement("update Venda set cli_rg = ?, usr_login = ? venda_valor = ?"
                + " where venda_cod = '" + v.getCodigo() + "'");

        setParametros(v);
        setMsg(Utils.Banco.con.manipular(pstmt), "Alteração");

        return flag;
    }

    public boolean Apagar(Venda v)
    {
        pstmt = Utils.Banco.con.geraStatement("delete from Venda where venda_cod = '" + v.getCodigo() + "'");

        setMsg(Utils.Banco.con.manipular(pstmt), "Exclusão");

        return flag;
    }
   
    private void setParametros(Venda v)
    {
        int count = 0;
        try
        {
            pstmt.setString(++count, v.getCliente().getCPF());
            pstmt.setString(++count, v.getUsuario().getLogin());
            pstmt.setDouble(++count, v.getValor());       
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }
    
    public ArrayList<Venda> get(String filtro, int op)
    {
        String sql = null;
        ArrayList<Venda> lista = new ArrayList();
        ResultSet rs;
        sql = "select * from Venda where upper(usr_login) like upper('" + filtro + "%') ";
        sql += getCodigo("venda_cod", filtro);
        
        rs = Banco.con.consultar(sql);
        try
        {
            CtrlPessoa cp = CtrlPessoa.create();
            while (rs.next())
            {
                lista.add(new Venda(rs.getString("venda_cod"), (Cliente)cp.Pesquisar(rs.getString("cli_cpf"),0).get(0),
                        (Usuario)cp.Pesquisar(rs.getString("usr_login"),1).get(0), rs.getDouble("venda_valor")));
            }
        } catch (Exception ex)
        {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }
        return lista;
    }
    
}
