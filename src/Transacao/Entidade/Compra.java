package Transacao.Entidade;

import Fornecedor.Controladora.CtrlFornecedor;
import Fornecedor.Entidade.Fornecedor;
import Interface.Funcional.TelaCompra;
import Interface.Funcional.TelaVenda;
import Movimentacao.Entidade.Movimentacao;
import Movimentacao.Entidade.Pagamento;
import Movimentacao.Entidade.Recebimento;
import Pessoa.Controladora.CtrlPessoa;
import Pessoa.Entidade.Usuario;
import Produto.Controladora.CtrlLote;
import Produto.Entidade.Lote;
import Transacao.Dal.CtrCompra;
import Transacao.Dal.CtrVenda;
import Utils.Banco;
import Utils.Carrinho;
import java.util.ArrayList;

public class Compra extends Transacao {

    private String Codigo;
    private Fornecedor fornecedor;
    private Usuario usuario;
    private double compra_valor;
    private int compra_quantLotes;
    private Carrinho carrinho;
    private int Parcelas;

    public Compra(String Codigo, Fornecedor fornecedor, Usuario usuario, double compra_valor, int compra_quantLotes) {
        this.Codigo = Codigo;
        this.fornecedor = fornecedor;
        this.usuario = usuario;
        this.compra_valor = compra_valor;
        this.compra_quantLotes = compra_quantLotes;
    }

    public Compra(Fornecedor fornecedor, Usuario usuario, double compra_valor, int compra_quantLotes) {
        this.fornecedor = fornecedor;
        this.usuario = usuario;
        this.compra_valor = compra_valor;
        this.compra_quantLotes = compra_quantLotes;
    }

    @Override
    protected boolean OperacaoDaMovimentacao(Movimentacao movimentacao) {
        mergeResultado(movimentacao.RealizarMovimentacao());
        return isResultado();
    }

    @Override
    protected boolean OperacaoItensDaTransacao(ProdutoDaTransacao produtoDaTransacao) {
        CtrlLote CLote = CtrlLote.create();
        Lote lote = (Lote) ((ProdutoDaCompra) produtoDaTransacao).getLote();
        mergeResultado(CLote.Salvar(lote.getCodigo(), lote.getEstoqueInicial(), lote.getEstoque(),
                lote.getValidade(), lote.getValidade()));
        return isResultado();
    }

    @Override
    protected boolean GeraMovimentacao() {
        movimentacao = new Movimentacao[Parcelas];
        for (int i = 0; i < Parcelas; i++) {
            movimentacao[i] = new Pagamento(compra_valor, this);
        }
        return isResultado();
    }

    @Override
    protected boolean GeraProdutodaTransacao() {
        ArrayList<TelaCompra> tc = carrinho.geraLotesCompra();
        produtosTransacao = new ProdutoDaTransacao[tc.size()];
        for (int i = 0; i < tc.size(); i++) {
            produtosTransacao[i] = new ProdutoDaCompra(this, tc.get(i).getLote(), tc.get(i).getProduto(), tc.get(i).getEstoque());
        }
        return isResultado();
    }

    @Override
    protected boolean RegistraTransacao() {
        CtrCompra cc = new CtrCompra();
        setCompra_quantLotes(carrinho.getLength());
        setCodigo(Banco.con.getMaxPK("compra", "compra_cod") + "");
        mergeResultado(cc.Salvar(this));
        return isResultado();
    }

    @Override
    protected boolean IniciaTransacao(Object... Params) {
        CtrlPessoa cp = CtrlPessoa.create();
        CtrlFornecedor cf = CtrlFornecedor.create();
        if (Params != null) {
            if (Params[0] != null) {
                String Usuario = (String) Params[0];
                setUsuario((Usuario) cp.Pesquisar(Usuario, 1).get(0));
                mergeResultado(true);
            }
            if (Params[1] != null) {
                String Fornecedor = (String) Params[1];
                setFornecedor((Fornecedor) cf.Pesquisar(Fornecedor).get(0));
                mergeResultado(true);
            }
        } else {
            mergeResultado(false);
        }
        return isResultado();
    }

    @Override
    protected boolean OperacaoDeCancelamento() {
        CtrCompra cc = new CtrCompra();
        return cc.Apagar(this);
    }

    /**
     * @return the Codigo
     */
    public String getCodigo() {
        return Codigo;
    }

    /**
     * @param Codigo the Codigo to set
     */
    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    /**
     * @return the fornecedor
     */
    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    /**
     * @param fornecedor the fornecedor to set
     */
    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the compra_valor
     */
    public double getCompra_valor() {
        return compra_valor;
    }

    /**
     * @param compra_valor the compra_valor to set
     */
    public void setCompra_valor(double compra_valor) {
        this.compra_valor = compra_valor;
    }

    /**
     * @return the compra_quantLotes
     */
    public int getCompra_quantLotes() {
        return compra_quantLotes;
    }

    /**
     * @param compra_quantLotes the compra_quantLotes to set
     */
    public void setCompra_quantLotes(int compra_quantLotes) {
        this.compra_quantLotes = compra_quantLotes;
    }

}
