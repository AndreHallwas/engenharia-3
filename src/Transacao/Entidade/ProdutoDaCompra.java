/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transacao.Entidade;

/**
 *
 * @author Raizen
 */
public class ProdutoDaCompra extends ProdutoDaTransacao{
    private Object lote;
    private String Quantidade;

    public ProdutoDaCompra(Transacao compra, Object lote, Object produto, String Quantidade) {
        this.transacao = compra;
        this.lote = lote;
        this.produto = produto;
        this.Quantidade = Quantidade;
    }

    /**
     * @return the lote
     */
    public Object getLote() {
        return lote;
    }

    /**
     * @param lote the lote to set
     */
    public void setLote(Object lote) {
        this.lote = lote;
    }

    public Transacao getCompra() {
        return transacao;
    }

    /**
     * @param transacao
     */
    public void setCompra(Transacao transacao) {
        this.transacao = transacao;
    }

    public String getQuantidade() {
        return Quantidade;
    }

    /**
     * @param Quantidade the Quantidade to set
     */
    public void setQuantidade(String Quantidade) {
        this.Quantidade = Quantidade;
    }
}
