/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import Interface.Funcional.TelaCompra;
import Transacao.Entidade.ProdutoDaVenda;
import Produto.Controladora.CtrlLote;
import Produto.Entidade.Lote;
import Produto.Entidade.Produto;
import Interface.Funcional.TelaVenda;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Raizen
 */
public class Carrinho
{

    private ArrayList<Object> Itens;

    public Carrinho()
    {
        Itens = new ArrayList();
    }

    public boolean add(Object item, Object item2)
    {
        int Pos;
        if (item instanceof Lote)
        {
            Lote lote = (Lote) item;
            Pos = busca_p(((Produto)item2).getCodigo());
            if (Pos != -1)
                ((Item) Itens.get(Pos)).incEstoque(lote.getEstoque());
            else
                Itens.add(new Item((Lote) item));
        }else
        if (item instanceof ProdutoDaVenda)
        {
            ProdutoDaVenda prod = (ProdutoDaVenda) item;
            Pos = busca_l(CtrlLote.getLoteCodigo(prod.getLote()));
            if (Pos != -1)
                return ((Item) Itens.get(Pos)).incQuantidade(prod.getQuantidade());
            else{
                Item obj;
                Itens.add(obj = new Item((Lote) ((ProdutoDaVenda) item).getLote(),
                        ((Produto)((ProdutoDaVenda) item).getProduto()), 
                        (ProdutoDaVenda) item));
            }
        }
        return true;
    }
    
    public void set(Object item, Object item2)
    {
        int Pos;
        if (item instanceof Lote)
        {
            Lote lote = (Lote) item;
            Pos = busca_p(((Produto)item2).getCodigo());
            if (Pos != -1)
                ((Item) Itens.get(Pos)).incEstoque(lote.getEstoque());
            else
                Itens.add(new Item((Lote) item));
        }else
        if (item instanceof ProdutoDaVenda)
        {
            ProdutoDaVenda prod = (ProdutoDaVenda) item;
            Pos = busca_l(CtrlLote.getLoteCodigo(prod.getLote()));
            if (Pos != -1)
                Itens.set(Pos,new Item((Lote) ((ProdutoDaVenda) item).getLote(),
                        ((Produto)((ProdutoDaVenda) item).getProduto()), 
                        (ProdutoDaVenda) item));
        }
    }

    public void remove(Object Item)
    {
        int Pos;
        if(Item instanceof Lote){
            Pos = busca_l(((Lote)Item).getCodigo());
            if(Pos != -1)
                Itens.remove(Pos);
        }else
        if(Item instanceof Produto){
            Pos = busca_l(((Produto)Item).getCodigo());
            if(Pos != -1)
                Itens.remove(Pos);
        }
    }

    public void remove(String Filtro)
    {
        int Pos = busca_p(Filtro);
        if (Pos != -1)
            Itens.remove(Pos);
    }

    public void clear()
    {
        Itens = new ArrayList();
    }

    public ObservableList<TelaVenda> refresh()
    {
        return FXCollections.observableList(geraLotes());
    }

    public int getLength()
    {
        return Itens.size();
    }

    public boolean isEmpty()
    {
        return Itens.isEmpty();
    }

    public int busca_p(String Filtro)
    {
        int Pos = 0;
        while (Pos < Itens.size() && !((Item) Itens.get(Pos)).getProduto().getCodigo().equals(Filtro))
        {
            Pos++;
        }
        return Pos < Itens.size() ? Pos : -1;
    }
    
    public int busca_l(String Filtro)
    {
        int Pos = 0;
        while (Pos < Itens.size() && !((Item) Itens.get(Pos)).getLote().getCodigo().equals(Filtro))
        {
            Pos++;
        }
        return Pos < Itens.size() ? Pos : -1;
    }

    public float getValorTotal()
    {
        float ValorTotal = 0;
        for (Object item : Itens)
        {
            ValorTotal += (((Item) item).getProduto().getPrecoCom()/10) * Integer.parseInt(((Item) item).getQuantidade());
        }
        return ValorTotal;
    }

    public ArrayList<TelaVenda> geraLotes()
    {
        ArrayList<TelaVenda> lotes = new ArrayList();
        for (Object item : Itens)
        {
            lotes.add(new TelaVenda(((Item) item).getLote(),
                    ((Item) item).getProduto(),
                    ((Item) item).getQuantidade()));
        }
        return lotes;
    }

    public ArrayList<TelaCompra> geraLotesCompra()
    {
        ArrayList<TelaCompra> lotes = new ArrayList();
        for (Object item : Itens)
        {
            lotes.add(new TelaCompra(((Item) item).getLote(),
                    ((Item) item).getProduto(),
                    ((Item) item).getQuantidade()));
        }
        return lotes;
    }

    public class Item
    {

        private Lote lote;
        private Produto produto;
        private ProdutoDaVenda prod;

        public Item(Lote lote)
        {
            this.lote = lote;
        }

        public Item(ProdutoDaVenda prod) {
            this.prod = prod;
        }

        public Item(ProdutoDaVenda prod, Lote lote) {
            this.lote = lote;
            this.prod = prod;
        }

        public Item(Lote lote, Produto produto, ProdutoDaVenda prod) {
            this.lote = lote;
            this.produto = produto;
            this.prod = prod;
        }
        
        public Lote getLote()
        {
            return lote;
        }

        public int getEstoque()
        {
            return lote.getEstoque();
        }
        
        public String getQuantidade(){
            return prod.getQuantidade();
        }

        public void incEstoque(int Quantidade)
        {
            if (Quantidade > 0)
            {
                lote.setEstoqueInicial(lote.getEstoque() + Quantidade);
                lote.setEstoque(lote.getEstoqueInicial());
            } else
                Mensagem.ExibirLog("Quantidade a incrementar no estoque menor que zero");
        }

        public void decEstoque(int Quantidade)
        {
            if (Quantidade <= lote.getEstoque())
            {
                lote.setEstoqueInicial(lote.getEstoque() - Quantidade);
                lote.setEstoque(lote.getEstoqueInicial());
            } else
                Mensagem.ExibirLog("Estoque menor que a quantidade a decrementar");
        }

        public Produto getProduto()
        {
            return produto;
        }

        private boolean incQuantidade(String quantidade) {
            int Quant = Integer.parseInt(prod.getQuantidade())+Integer.parseInt(quantidade);
            if(Quant < lote.getEstoque()){
                prod.setQuantidade(Quant+"");
                return true;
            }
            return false;
        }
    }
}
