/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.sql.ResultSet;
import javafx.embed.swing.SwingNode;
import javax.swing.SwingUtilities;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JRViewer;
import net.sf.jasperreports.view.JasperViewer;

public class Relatorio
{

    public static void gerarRelatorio(String sql, String relat, String Titulo)
    {
        try
        {
            ResultSet rs = Banco.con.consultar(sql);
            //implementação da interface JRDataSource para DataSource ResultSet
            JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
            //preenchendo e chamando o relatório
            String jasperPrint = JasperFillManager.fillReportToFile(relat, null, jrRS);
            JasperViewer viewer = new JasperViewer(jasperPrint, false, false);

            viewer.setExtendedState(JasperViewer.MAXIMIZED_BOTH);//maximizado
            viewer.setTitle(Titulo);
            viewer.setVisible(true);
        } catch (Exception erro)
        {
            Mensagem.ExibirException(erro, "Erro ao Gerar Relatório");
        }
    }

    public static SwingNode gerarRelatorio(String sql, String relat)//dentro de um painel
    {
        try
        {
            ResultSet rs = Banco.con.consultar(sql);
            JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
            JasperPrint print = JasperFillManager.fillReport(relat, null, jrRS);
            JRViewer viewer = new JRViewer(print);
            viewer.setOpaque(true);
            viewer.setVisible(true);
            viewer.setZoomRatio(0.5f);
            viewer.setVisible(true);
            viewer.getHeight();
            SwingNode swingNode = new SwingNode();
            SwingUtilities.invokeLater(() ->
            {
                swingNode.setContent(viewer);
            });
            return swingNode;
        } catch (JRException erro)
        {
            Mensagem.ExibirException(erro, "Erro ao Gerar Relatório");
            return null;
        }
    }
}
