/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import Pessoa.Entidade.Acesso;
import Pessoa.Entidade.Nivel;
import Pessoa.Dao.CtrNivel;
import engenharia.ui.MainFXMLController;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javafx.scene.Node;

/**
 *
 * @author Aluno
 */
public class ControledeAcesso
{

    public static Acesso Controle;
    public static ArrayList<Node> arr;
    public static ArrayList<String> nom;
    public static ArrayList<Object> tip;
    public static Nivel nivel;

    public static void Inicializa()
    {
        arr = new ArrayList<>();
        nom = new ArrayList<>();
        tip = new ArrayList<>();
    }

    public static void Default()
    {
        CtrNivel cn = new CtrNivel();
        nivel = cn.get("Default").get(0);
        ControledeAcesso.selecionaPermissao();
    }

    public static void add(Node Obj, String Nome, int Pos, int tip)
    {

        if (arr.size() > Pos)
            arr.set(Pos, Obj);
        else
            arr.add(Pos, Obj);
        if (nom.size() > Pos)
            nom.set(Pos, Nome);
        else
            nom.add(Pos, Nome);
        if (ControledeAcesso.tip.size() > Pos)
            ControledeAcesso.tip.set(Pos, tip);
        else
            ControledeAcesso.tip.add(Pos, tip);
        if (nivel != null)
            selecionaPermissao();
    }

    public static boolean logar(String Senha, String Usuario)
    {
        Boolean auxRetorno = false;
        String sql = "select * from usuario where usr_login = '" + Usuario
                + "' and usr_senha = '" + Senha + "'";
        ResultSet rs = Banco.con.consultar(sql);
        if (rs != null)
            try
            {
                if (rs.next())
                {
                    CtrNivel cn = new CtrNivel();
                    nivel = cn.get(rs.getString("nivel_cod")).get(0);
                    MainFXMLController._pndados.getChildren().clear();
                    MainFXMLController._txUsuarioInterface.setText(Usuario);
                    MainFXMLController._txNivelInterface.setText(nivel.getNome());
                    selecionaPermissao();
                    auxRetorno = true;
                    Mensagem.Armazenar("Login Efetuado");

                } else
                    Mensagem.Armazenar("Login Não Efetuado");
            } catch (SQLException ex)
            {
                Mensagem.ExibirException(ex, "Erro no Login");
            }
        return auxRetorno;
    }

    public static void selecionaPermissao()
    {
        int count = 0;
        try
        {
            for (Node novo : arr)
            {
                if (nivel.getPermissao().charAt(count) == ' ')
                    if ((int) tip.get(count) == 1)
                        novo.setVisible(false);
                    else
                        novo.setDisable(true);
                else if ((int) tip.get(count) == 1)
                    novo.setVisible(Integer.parseInt(nivel.getPermissao().charAt(count) + "") != 0);
                else
                    novo.setDisable(Integer.parseInt(nivel.getPermissao().charAt(count) + "") == 0);
                count++;
            }
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Erro no Seleciona Permissao do Acesso");
        }
    }

    public static Acesso getControle()
    {
        return Controle;
    }

    public static void DefaultEmpresa()
    {
        CtrNivel cn = new CtrNivel();
        ArrayList<Nivel> arr = cn.get("DefaultEmpresa");
        if (!arr.isEmpty())
        {
            nivel = arr.get(0);
            ControledeAcesso.selecionaPermissao();
        }
    }

    public static ArrayList<String> getNom()
    {
        return nom;
    }

}
