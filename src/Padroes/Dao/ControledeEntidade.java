/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Padroes.Dao;

import Utils.Mensagem;
import java.sql.PreparedStatement;

/**
 *
 * @author Raizen
 */
public abstract class ControledeEntidade {
    protected boolean flag;
    protected PreparedStatement pstmt;
    protected String Msg;

    public ControledeEntidade() {
        
    }
    
    protected void setMsg(boolean Condition, String Op)
    {
        if (Condition)
        {
            Msg = Op + " Efetuado";
            flag = true;
        } else
            Msg = Op + " Não Efetuado Erro: " + Utils.Banco.con.getMensagemErro();
        Mensagem.ExibirLog(Msg);
    }
    
    protected String getCodigo(String Campo, String filtro)
    {
        String sql = "";
        if (!filtro.isEmpty())
            try
            {
                int aux = Integer.parseInt(0 + filtro);
                sql = sql + " or " + Campo + " = '" + aux + "'";
                return sql;
            } catch (NumberFormatException ex)
            {
                Mensagem.ExibirException(ex, "Erro no Nivel Cod");
            }
        return "";
    }
    
    public static boolean isNumeric(String str)
    {
        if (str == null)
            return false;
        int sz = str.length();
        for (int i = 0; i < sz; i++)
        {
            if (Character.isDigit(str.charAt(i)) == false)
                return false;
        }
        return true;
    }

    public boolean isFlag() {
        return flag;
    }

    public PreparedStatement getPstmt() {
        return pstmt;
    }

    public String getMsg() {
        return Msg;
    }
    
}
