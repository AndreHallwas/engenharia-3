/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Padroes.Dao;

import Pessoa.Entidade.Cliente;
import Produto.Entidade.Produto;
import Utils.Banco;
import Utils.Mensagem;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class CtrObserver extends ControledeEntidade {
    

    public boolean Salvar(Object Individuo, Object Objeto)
    {
        if(Individuo instanceof Cliente && Objeto instanceof Produto){
            pstmt = Banco.con.geraStatement(
                    "insert into Observer(prod_cod, cli_cpf) values(?,?)");

            setParametros(Individuo,Objeto);
            setMsg(Banco.con.manipular(pstmt), "Cadastro");
        }
        return flag;
    }

    public boolean Alterar(Object Individuo, Object Objeto)
    {
        if(Individuo instanceof Cliente && Objeto instanceof Produto){
            pstmt = Banco.con.geraStatement("update Observer set prod_cod = ?,"
                    + " cli_cpf = ? where prod_cod = '" + ((Produto)Objeto).getCodigo() + "'");

            setParametros(Individuo,Objeto);
            setMsg(Banco.con.manipular(pstmt), "Alteração");
        }
        return flag;
    }

    public boolean Apagar(Object Individuo, Object Objeto)
    {
        if(Individuo instanceof Cliente){
            pstmt = Banco.con.geraStatement("delete from Observer where cli_cpf = '" + ((Cliente)Individuo).getCPF() + "' "
                    + " and prod_cod = '" + ((Produto)Objeto).getCodigo() + "'");

            setMsg(Banco.con.manipular(pstmt), "Exclusão");
        }
        return flag;
    }
    
    private void setParametros(Object Individuo, Object Objeto)
    {
        try
        {
            pstmt.setInt(1,Integer.parseInt(((Produto)Objeto).getCodigo()));
            pstmt.setString(2, ((Cliente)Individuo).getCPF());
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }
    
    public ArrayList<String> get(String filtro, String filtro2, int op)
    {
        String sql = null;
        ArrayList<String> n = new ArrayList<>();
        ResultSet rs;
        try
        {
            if(op == 1){
                sql = "select * from Observer where upper(cli_cpf) like upper('" + filtro + "%') ";
                sql += getCodigo("prod_cod", filtro);
            }else
            if(op == 2){
                sql = "select * from Observer where upper(cli_cpf) like upper('" + filtro + "%') and "
                        + "upper(prod_cod) like upper('" + filtro2 + "%')";
            }
            
            rs = Banco.con.consultar(sql);

            while (rs.next())
            {
                n.add(rs.getString("cli_cpf"));
            }
        } catch (Exception ex)
        {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }
        return n;
    }
    
}
