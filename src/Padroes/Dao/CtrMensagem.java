/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Padroes.Dao;

import Pessoa.Entidade.Cliente;
import Produto.Entidade.Produto;
import Utils.Banco;
import Utils.Mensagem;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class CtrMensagem extends ControledeEntidade{
    

    public boolean Salvar(Object Individuo, Object Objeto, String Mensagem)
    {
        if(Individuo instanceof Cliente && Objeto instanceof Produto){
            pstmt = Banco.con.geraStatement(
                    "insert into Mensagem(prod_cod, cli_cpf, Mensagem) values(?,?,?)");

            setParametros(Individuo,Objeto,Mensagem);
            setMsg(Banco.con.manipular(pstmt), "Cadastro");
        }
        return flag;
    }

    public boolean Alterar(Object Individuo, Object Objeto, String Mensagem)
    {
        if(Individuo instanceof Cliente && Objeto instanceof Produto){
            pstmt = Banco.con.geraStatement("update Mensagem set prod_cod = ?,"
                    + " cli_cpf = ?, Mensagem = ? where prod_cod = '" + ((Produto)Objeto).getCodigo() + "' and "
                            + "cli_cpf = '"+((Cliente)Individuo).getCPF()+"' and Mensagem = '"+Mensagem+"'");
            setParametros(Individuo,Objeto,Mensagem);
            setMsg(Banco.con.manipular(pstmt), "Alteração");
        }
        return flag;
    }

    public boolean Apagar(Object Individuo, Object Objeto)
    {
        if(Individuo instanceof Cliente){
            pstmt = Banco.con.geraStatement("delete from Mensagem where cli_cpf = '" + ((Cliente)Individuo).getCPF() + "'");

            setMsg(Banco.con.manipular(pstmt), "Exclusão");
        }
        return flag;
    }
    
    private void setParametros(Object Individuo, Object Objeto, String Message)
    {
        try
        {
            pstmt.setInt(1, Integer.parseInt(((Produto)Objeto).getCodigo()));
            pstmt.setString(2, ((Cliente)Individuo).getCPF());
            pstmt.setString(3, Message);
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }
    
    public ArrayList<String> get(String filtro)
    {
        String sql = null;
        ArrayList<String> n = new ArrayList<>();
        ResultSet rs;
        sql = "select * from Mensagem where upper(cli_cpf) like upper('" + filtro + "%') ";
        sql += getCodigo("prod_cod", filtro);
        sql += getCodigo("mens_cod", filtro);

        rs = Banco.con.consultar(sql);
        try
        {
            while (rs.next())
            {
                n.add(rs.getString("Mensagem"));
            }
        } catch (Exception ex)
        {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }

        return n;
    }
    
}
