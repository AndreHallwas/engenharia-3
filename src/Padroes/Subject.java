/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Padroes;

/**
 *
 * @author Raizen
 */
public interface Subject {
    public boolean register(Object Individuo,Object Objeto);
    public boolean remove(Object Individuo,Object Objeto);
    public void Notify(Object Objeto);
}
