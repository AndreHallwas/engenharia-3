/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engenharia;

import java.io.FileInputStream;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author Raizen
 */
public class Engenharia extends Application
{

    private static Stage _stage;

    @Override
    public void start(Stage stage) throws Exception
    {
        Parent root = FXMLLoader.load(getClass().getResource("/engenharia/ui/MainFXML.fxml"));
        /////Parent root = FXMLLoader.load(getClass().getResource("/engenharia/ui/Funcoes/TelaCompra.fxml"));

        Scene scene = new Scene(root);
        stage.setTitle("Engenharia Projeto");
        stage.getIcons().add(new Image(new FileInputStream("Img/Logo.png")));
        stage.setResizable(false);
        stage.setWidth(1350);
        stage.setHeight(700);
        _stage = stage;
        scene.getStylesheets().add("engenharia/ui/estilo/DarkTheme.css");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

}
