/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package engenharia.ui;

import Empresa.Dao.CtrEmpresa;
import Interface.Configuracao.TelaConfiguracaoGeralController;
import Interface.Configuracao.TelaEmpresaController;
import Interface.Consulta.TelaConsultaUsuarioController;
import Utils.Banco;
import Utils.ControledeAcesso;
import Utils.Imagem;
import Utils.Mensagem;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class MainFXMLController implements Initializable
{

    public static HBox _pndados;

    @FXML
    private Label txUsuarioInterface;
    @FXML
    private Label txNivelInterface;
    @FXML
    private HBox pndados;
    @FXML
    private ProgressBar pbProgresso;
    @FXML
    private HBox Menu;
    @FXML
    private Accordion MenuLateral;
    @FXML
    private TitledPane TabGerenciamento;
    @FXML
    private Label opcUsuario;
    @FXML
    private Label opcCliente;
    @FXML
    private Label opcFornecedor;
    @FXML
    private Label opcProduto;
    @FXML
    private TitledPane TabFuncoes;
    @FXML
    private TitledPane TabRelatorio;
    @FXML
    private Label opcCompra;
    @FXML
    private Label opcPagamento;
    @FXML
    private TitledPane TabCadastro;
    @FXML
    private Label opcBackup;
    @FXML
    private Label opcRestore;
    public static Stage _login;
    @FXML
    private ImageView imgLogo;
    @FXML
    private Label lbTitulo;
    @FXML
    private Label opcConsultaCompra;
    public static Label _txUsuarioInterface;
    public static Label _txNivelInterface;
    @FXML
    private Button btnLogin;
    private int login;
    @FXML
    private Label opcNivel;
    @FXML
    private TitledPane TabConsulta;
    @FXML
    private Label opcConsultaUsuario;
    @FXML
    private Label opcConsultaCliente;
    @FXML
    private Label opcFichaUsuario;
    @FXML
    private Label opcFabricante;
    @FXML
    private Label opcVenda;
    @FXML
    private Label opcLote;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {

        ControledeAcesso.Inicializa();
        ControledeAcesso.add(TabCadastro, TabCadastro.getText(), 0, 1);
        ControledeAcesso.add(opcUsuario, opcUsuario.getText(), 1, 1);
        ControledeAcesso.add(opcNivel, opcNivel.getText(), 2, 1);
        ControledeAcesso.add(TabFuncoes, TabFuncoes.getText(), 3, 1);
        ControledeAcesso.add(opcBackup, opcBackup.getText(), 4, 1);
        ControledeAcesso.add(opcRestore, opcRestore.getText(), 5, 1);
        ControledeAcesso.add(TabGerenciamento, TabGerenciamento.getText(), 6, 1);
        ControledeAcesso.add(TabRelatorio, TabRelatorio.getText(), 7, 1);
        ControledeAcesso.add(TabConsulta, TabConsulta.getText(), 8, 1);
        ControledeAcesso.add(opcConsultaUsuario, opcConsultaUsuario.getText(), 9, 1);
        ControledeAcesso.add(opcConsultaCliente, opcConsultaCliente.getText(), 10, 1);

        if(Banco.iniciarConexao("engenharia","engenharia.txt") != 1){
            evtConfiguracaoInicial(null);
        }

        ControledeAcesso.Default();

        ////Banco.conectar();
        try
        {
            imgLogo.setImage(Imagem.BufferedImageToImage(new CtrEmpresa().get().getImagem()));
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Erro na Imagem da Empresa no inicialize do Main");
        }
        _pndados = pndados;
        _txUsuarioInterface = txUsuarioInterface;
        _txNivelInterface = txNivelInterface;
        login = 1;
        
        /*CtrlLote cl = new CtrlLote();
        cl.Salvar(new Lote(new Produto("3", null, null, null), login, login, null));*/

    }

    private void carrega(String Tela)
    {
        try
        {
            Parent root = FXMLLoader.load(getClass().getResource(Tela));
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
    }

    @FXML
    private boolean evtLogin(Event event)
    {
        Parent root = null;
        try
        {
            /*stage.getIcons().add(new Image(getClass().getResourceAsStream("/IMG/logo_atlx.png")));*/
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("TelaLoginFXML.fxml"));
            Scene scene = new Scene(root);
            stage.setTitle("Login");
            stage.getIcons().add(new Image(new FileInputStream("Img/Logo.png")));
            scene.getStylesheets().add("engenharia/ui/estilo/DarkTheme.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.initStyle(StageStyle.DECORATED);
            _login = stage;
            stage.showAndWait();
            return TelaLoginFXMLController.isRetorno();
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela de Login");
        }
        return false;
    }

    @FXML
    private void evtDeslogar(Event event)
    {
        txNivelInterface.setText("Nível");
        txUsuarioInterface.setText("Usuario");
        _pndados.getChildren().clear();
        ControledeAcesso.Default();
    }

    @FXML
    private void evtBackup()
    {
        VBox painel = new VBox();
        TextArea ta = new TextArea();
        painel.getChildren().add(ta);
        _pndados.getChildren().clear();
        _pndados.getChildren().add(painel);
        new Thread(() ->
        {
            if (Banco.realizaBackupRestauracao("copiar.bat", ta))
                Mensagem.Exibir("Backup efetuado!", 1);
            else
                Mensagem.Exibir("Erro ao efetuar o backup!", 2);
        }).start();
    }

    @FXML
    private void evtRestore()
    {
        VBox painel = new VBox();
        TextArea ta = new TextArea();
        painel.getChildren().add(ta);
        _pndados.getChildren().clear();
        _pndados.getChildren().add(painel);
        new Thread(() ->
        {
            if (Banco.realizaBackupRestauracao("restaurar.bat", ta))
                Mensagem.Exibir("Restauração efetuada!", 1);
            else
                Mensagem.Exibir("Erro ao efetuar a restauração!", 2);
        }).start();
    }

    @FXML
    private void evtConfiguracao(Event event)
    {
        TelaConfiguracaoGeralController.setRoot(pndados);
        carrega("TelaConfiguracaoGeral.fxml");/*
        if(Banco.conectar(TelaConfiguracaoGeralController.getStringConexao(),
        TelaConfiguracaoGeralController.getEndereco(),
        TelaConfiguracaoGeralController.getPorta(),
        TelaConfiguracaoGeralController.getUsuarioBanco(),
        TelaConfiguracaoGeralController.getSenhaBanco(),
        TelaConfiguracaoGeralController.getBanco())){
            Mensagem.Exibir("Conexão Efetuada COm Sucesso", 1);
        }else{
            Mensagem.Exibir("Conexão Não EFEtuada", 1);
        }*/
    }

    @FXML
    private void evtBtnLogin(Event event)
    {
        if (login == 1)
        {
            if (evtLogin(event))
            {
                btnLogin.setText("Deslogar");
                login = 0;
            }
        } else
        {
            evtDeslogar(event);
            btnLogin.setText("Login");
            login = 1;
        }

    }

    @FXML
    private void evtGerenciamentoUsuario(Event event)
    {
        carrega("Cadastro/TelaCadUsuario.fxml");
    }

    @FXML
    private void evtInfo(Event event)
    {
        TelaEmpresaController.setRoot(pndados);
        carrega("TelaEmpresa.fxml");
    }

    @FXML
    private void evtCompra(Event event)
    {
        carrega("Funcoes/TelaCompra.fxml");
    }

    @FXML
    private void evtPagamento(Event event)
    {

    }

    private void evtRecebimento(Event event)
    {
        carrega("Funcoes/TelaRecebimento.fxml");
    }

    @FXML
    private void evtConfiguracaoInicial(Event event)
    {
        Parent root = null;
        try
        {
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("TelaConfiguracaoInicial.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("engenharia/ui/estilo/DarkTheme.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.showAndWait();
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela de Configuração Inicial");
        }
    }

    @FXML
    private void evtConsultaCompra(Event event)
    {

    }

    @FXML
    private void evtGerenciamentoNivel(Event event)
    {
        carrega("Cadastro/TelaCadNivel.fxml");
    }

    @FXML
    private void evtConsultaUsuario(Event event)
    {
        TelaConsultaUsuarioController.setBtnPainel(false);
        carrega("Consulta/TelaConsultaUsuario.fxml");
    }

    @FXML
    private void evtConsultaCliente(Event event)
    {
    }

    @FXML
    private void evtRelFichadoUsuario(MouseEvent event)
    {
        carrega("Relatorio/TelaRelatFichaUsuario.fxml");
    }

    @FXML
    private void evtGerenciamentoFabricante(MouseEvent event) {
        carrega("Cadastro/TelaCadFabricante.fxml");
    }

    @FXML
    private void evtGerenciamentoFornecedor(MouseEvent event) {
        carrega("Cadastro/TelaCadFornecedor.fxml");
    }

    @FXML
    private void evtGerenciamentoProduto(MouseEvent event) {
        carrega("Cadastro/TelaCadProduto.fxml");
    }

    @FXML
    private void evtVenda(MouseEvent event) {
        carrega("Funcoes/TelaVenda.fxml");
    }

    @FXML
    private void evtGerenciamentoCliente(MouseEvent event) {
        carrega("Cadastro/TelaCadCliente.fxml");
    }

    @FXML
    private void evtGerenciamentoLote(MouseEvent event) {
        carrega("Cadastro/TelaCadLote.fxml");
    }

}
