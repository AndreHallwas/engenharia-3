/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Configuracao;

import Utils.Arquivo;
import static Utils.Banco.conectar;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConfiguracaoGeralController implements Initializable
{

    @FXML
    private TextField txbEndereco;
    @FXML
    private TextField txbPorta;
    @FXML
    private TextField txbBanco;
    @FXML
    private TextField txbUsuarioBanco;
    @FXML
    private PasswordField txbSenhaBanco;
    @FXML
    private TextField txbStringConexao;
    @FXML
    private Label lbErroStringConexao;
    @FXML
    private Label lbErroEndereco;
    @FXML
    private Label lbErroPorta;
    @FXML
    private Label lbErroBanco;
    @FXML
    private Label lbErroUsuarioBanco;
    @FXML
    private Label lbErroSenhaBanco;
    private static String Endereco;
    private static String Porta;
    private static String Banco;
    private static String UsuarioBanco;
    private static String SenhaBanco;
    private static String StringConexao;
    private static boolean Next;
    private static Node root;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        ArrayList<String> Lista = Arquivo.leArquivoDeStringUTF("Config.txt", 6);
        MaskFieldUtil.numericField(txbPorta);
        if (Lista != null && !Lista.isEmpty())
        {
            txbEndereco.setText(Endereco = Lista.get(0));
            txbPorta.setText(Porta = Lista.get(1));
            txbBanco.setText(Banco = Lista.get(2));
            txbUsuarioBanco.setText(UsuarioBanco = Lista.get(3));
            txbSenhaBanco.setText(SenhaBanco = Lista.get(4));
            txbStringConexao.setText(StringConexao = Lista.get(5));
            Next = true;
        } else
            Next = false;

    }

    @FXML
    private void btnConfirmar(ActionEvent event)
    {
        ArrayList<String> Lista = new ArrayList<>();
        EstadoOriginal();
        if (validaCampos())
        {
            Lista.add(Endereco = txbEndereco.getText());
            Lista.add(Porta = txbPorta.getText());
            Lista.add(Banco = txbBanco.getText());
            Lista.add(UsuarioBanco = txbUsuarioBanco.getText());
            Lista.add(SenhaBanco = txbSenhaBanco.getText());
            Lista.add(StringConexao = txbStringConexao.getText());
            if (iniciarConexao())
            {
                Arquivo.gravaArquivoDeStringUTF("Config.txt", Lista);
                btnCancelar(event);
            }
            Next = true;
        } else
            Next = false;
    }

    @FXML
    private void btnCancelar(ActionEvent event)
    {
        Pane n = (Pane) root;
        n.getChildren().clear();
        /*
        Node p = ((Node)event.getSource());
        Pane aux = null;
        while(p != null){
            if(p instanceof HBox){
                aux = (Pane) p;
            }
            p = p.getParent();
        }
        if(aux != null){
            aux.getChildren().clear();
        }
         */
    }

    private void setErro(Label lb, String Msg)
    {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        if (txbEndereco.getText().trim().isEmpty())
        {
            setErro(lbErroEndereco, "Campo Obrigatório Vazio");
            txbEndereco.setText("");
            txbEndereco.requestFocus();
            fl = false;
        }
        if (txbBanco.getText().trim().isEmpty())
        {
            setErro(lbErroBanco, "Campo Obrigatório Vazio");
            txbBanco.setText("");
            txbBanco.requestFocus();
            fl = false;
        }
        if (txbPorta.getText().trim().isEmpty())
        {
            setErro(lbErroPorta, "Campo Obrigatório Vazio");
            txbPorta.setText("");
            txbPorta.requestFocus();
            fl = false;
        }
        if (txbUsuarioBanco.getText().trim().isEmpty())
        {
            setErro(lbErroUsuarioBanco, "Campo Obrigatório Vazio");
            txbUsuarioBanco.setText("");
            txbUsuarioBanco.requestFocus();
            fl = false;
        }
        if (txbSenhaBanco.getText().trim().isEmpty())
        {
            setErro(lbErroSenhaBanco, "Campo Obrigatório Vazio");
            txbSenhaBanco.setText("");
            txbSenhaBanco.requestFocus();
            fl = false;
        }
        if (txbStringConexao.getText().trim().isEmpty())
        {
            setErro(lbErroStringConexao, "Campo Obrigatório Vazio");
            txbStringConexao.setText("");
            txbStringConexao.requestFocus();
            fl = false;
        }
        return fl;
    }

    private void EstadoOriginal()
    {
        lbErroEndereco.setVisible(false);
        lbErroBanco.setVisible(false);
        lbErroPorta.setVisible(false);
        lbErroUsuarioBanco.setVisible(false);
        lbErroSenhaBanco.setVisible(false);
        lbErroStringConexao.setVisible(false);
    }

    public static String getEndereco()
    {
        return Endereco;
    }

    public static String getPorta()
    {
        return Porta;
    }

    public static String getBanco()
    {
        return Banco;
    }

    public static String getUsuarioBanco()
    {
        return UsuarioBanco;
    }

    public static String getSenhaBanco()
    {
        return SenhaBanco;
    }

    public static String getStringConexao()
    {
        return StringConexao;
    }

    public static boolean isNext()
    {
        return Next;
    }

    private boolean iniciarConexao()
    {
        boolean flag = false;
        if (conectar(StringConexao, Endereco, Porta, UsuarioBanco, SenhaBanco, Banco))
        {
            Mensagem.Exibir("Conexão Efetuada com Sucesso", 1);
            flag = true;
        } else
            Mensagem.Exibir("Conexão Não Efetuada", 2);
        return flag;
    }

    public static void setRoot(Node root)
    {
        TelaConfiguracaoGeralController.root = root;
    }
}
