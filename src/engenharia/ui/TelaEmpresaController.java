/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Configuracao;

import Utils.ControledeAcesso;
import Empresa.Controladora.CtrlEmpresa;
import Empresa.Entidade.Empresa;
import Empresa.Dao.CtrEmpresa;
import Utils.Imagem;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Stack;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaEmpresaController implements Initializable
{

    @FXML
    private TextField txbNome;
    @FXML
    private TextField txbCNPJ;
    @FXML
    private TextField txbRazao2;
    @FXML
    private TextField txbFone;
    @FXML
    private TextField txbEndereco;
    private char flag;
    @FXML
    private Button btnCancelar;
    @FXML
    private ImageView imgEmpresa;
    private BufferedImage ima;
    private static boolean statusEmp;
    private static Node root;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        // TODO
        CtrlEmpresa ce = CtrlEmpresa.create();
        Object p = ce.Pesquisar();
        MaskFieldUtil.foneField(txbFone);
        MaskFieldUtil.cpfCnpjField(txbCNPJ);
        if (p != null)
        {
            CtrlEmpresa.setCampos(p, txbNome, txbCNPJ, txbEndereco, txbFone, txbRazao2);
            WritableImage im = Imagem.BufferedImageToImage(CtrlEmpresa.getImagem(p));
            if (im != null)
                imgEmpresa.setImage(im);
            btnCancelar.setDisable(false);
            flag = 1;
            statusEmp = true;
        } else
        {
            ControledeAcesso.DefaultEmpresa();
            btnCancelar.setDisable(true);
            flag = 0;
            statusEmp = false;
        }
    }

    private boolean verificaVazio(TextField Campo)
    {
        if (Campo.getText().trim().equals("") || Campo.getText().trim().equals(" "))
        {
            Mensagem.ExibirLog("Erro: Campo Vazio");
            return true;
        }
        return false;
    }

    @FXML
    private void evtConcluir(MouseEvent event)
    {
        boolean teste;
        CtrlEmpresa cp = CtrlEmpresa.create();
        Stack pilha = new Stack();
        pilha.push(teste = verificaVazio(txbNome));
        pilha.push(teste = verificaVazio(txbCNPJ));
        pilha.push(teste = verificaVazio(txbEndereco));
        pilha.push(teste = verificaVazio(txbFone));
        pilha.push(teste = verificaVazio(txbRazao2));
        while (!pilha.isEmpty())
        {
            if ((boolean) pilha.pop() == true)
                teste = true;
        }
        try
        {
            if (teste == false)
            {
                if (flag == 0)
                    cp.Salvar(txbNome.getText(), txbCNPJ.getText(), txbEndereco.getText(), txbFone.getText(), txbRazao2.getText(), ima);
                else
                {
                    if (ima == null)
                        ima = Imagem.ImageToBufferedImage(imgEmpresa.getImage());
                    cp.Alterar(txbNome.getText(), txbCNPJ.getText(), txbEndereco.getText(), txbFone.getText(), txbRazao2.getText(), ima);
                }
                statusEmp = true;
                evtCancelar((Event) event);
            } else
                Mensagem.ExibirLog("Erro: Campo Vazio");
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Erro na Tela de Empresa");
        }

    }

    @FXML
    private void evtCancelar(Event event)
    {
        /////((Pane)((Node)event.getSource()).getParent().getParent().getParent()).getChildren().clear();
        Pane n = (Pane) root;
        n.getChildren().clear();

        /////((Pane)root).getChildren().clear();/*
        ///Stage stage = ((Stage)((Node)event.getSource()).getScene().getWindow());
        ////stage.close();/////fexa janela*/
        ControledeAcesso.Default();
        ControledeAcesso.selecionaPermissao();
    }

    @FXML
    private void evtimgClick(MouseEvent event)
    {
        Image im = Imagem.capturaImagem();
        if (im != null)
        {
            ima = Imagem.ImageToBufferedImage(im);
            imgEmpresa.setImage(im);
        } else
            Mensagem.Exibir("Não Foi Possivel Abrir a Imagem", 2);
    }

    public static boolean isStatusEmp()
    {
        return statusEmp;
    }

    public static void setRoot(Node root)
    {
        TelaEmpresaController.root = root;
    }

}
