/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Configuracao;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebView;

/**
 * FXML Controller class
 *
 * @author Luish
 */
public class TelaHelpController implements Initializable
{

    @FXML
    private WebView telahtml;
    @FXML
    private HBox pndado;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        telahtml.getEngine().load("file:///Users/Aluno/Downloads/Picinin-ferro-3-trabalho-sisdentalfx-441680587e2c/src/sisdentalfx/Help/public_html/index.html");
    }

}
