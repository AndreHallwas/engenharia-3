/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Gerenciamento;

import Produto.Controladora.CtrlLote;
import Produto.Entidade.Produto;
import Utils.Mensagem;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaCadLoteController implements Initializable {

    @FXML
    private TextField txbProduto;
    @FXML
    private TextField txbQuantidade;
    private Object produto;
    @FXML
    private TextField txbValidade;
    @FXML
    private VBox pndados;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        produto = null;
    }    

    @FXML
    private void evtCadastrar(ActionEvent event) {
        CtrlLote cl = CtrlLote.create();
        if(produto != null && !txbQuantidade.getText().isEmpty() && !txbValidade.getText().isEmpty()){
            cl.Salvar(null , Integer.parseInt(txbQuantidade.getText()), Integer.parseInt(txbQuantidade.getText()),
                    txbValidade.getText(), CtrlLote.getProdutoCodigo(produto));
            clear(pndados.getChildren());
            Mensagem.Exibir("Cadastro Efetuado", 1);
        }else{
            Mensagem.Exibir("Dados Inválidos", 3);
        }
    }

    private void evtCancelar(ActionEvent event) {
        Stage stage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
        stage.close();/////fexa janela
    }

    public void clear(ObservableList<Node> pn)
    {
        ObservableList<Node> componentes = pn; //”limpa” os componentes
        for (Node n : componentes)
        {
            if (n instanceof Pane)
                clear(((Pane) n).getChildren());
            if (n instanceof TextInputControl) // textfield, textarea e htmleditor

                ((TextInputControl) n).setText("");
            if (n instanceof ComboBox)
                ((ComboBox) n).getItems().clear();
            if (n instanceof ImageView)
                ((ImageView) n).setImage(null);
        }
    }

    @FXML
    private void evtSelecionaProduto(MouseEvent event) {
        Parent root = null;
        try
        {
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/engenharia/ui/Consulta/TelaConsultaProduto.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("engenharia/ui/estilo/tema1.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.showAndWait();
            produto = TelaConsultaProdutoController.getProduto();
            if (produto != null)
            {
                CtrlLote.setProduto(produto, txbProduto);
            }

        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao consultar produto na compra");
        }
    }
    
}
