/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engenharia.ui.Cadastro;

import Utils.ControledeAcesso;
import Pessoa.Controladora.CtrlPessoa;
import Endereco.Entidade.Bairro;
import Endereco.Entidade.Cidade;
import Endereco.Entidade.Endereco;
import Endereco.Entidade.Estado;
import Pessoa.Entidade.Nivel;
import Endereco.Entidade.Pais;
import Pessoa.Entidade.Usuario;
import Pessoa.Dao.CtrNivel;
import Pessoa.Dao.CtrPessoa;
import Interface.Consulta.TelaConsultaUsuarioController;
import Interface.Gerenciamento.TelaEnderecoController;
import Utils.Imagem;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.Relatorio;
import Utils.Validar;
import engenharia.ui.MainFXMLController;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadUsuarioController implements Initializable
{

    @FXML
    private TextField txBusca;
    @FXML
    private TextField txbNome;
    @FXML
    private TextField txbRG;
    @FXML
    private TextField txbCPF;
    @FXML
    private TextField txbTelefone;
    @FXML
    private TextField txbEndereco;
    @FXML
    private TextField txbEmail;
    @FXML
    private TextField txbSalario;
    @FXML
    private ComboBox<String> cbNivel;
    @FXML
    private TextArea taObs;
    @FXML
    private TableColumn<Object, String> tcNome;
    @FXML
    private TableColumn<Object, String> tcRG;
    @FXML
    private TableColumn<Object, String> tcCPF;
    @FXML
    private TableColumn<Object, String> tcTelefone;
    @FXML
    private TableColumn<Object, String> tcEndereco;
    @FXML
    private TableColumn<Object, String> tcEmail;
    @FXML
    private TableColumn<Object, Double> tcSalario;
    @FXML
    private TableColumn<Object, String> tcNivel;
    @FXML
    private TableColumn<Object, String> tcObs;
    @FXML
    private Button btnovo;
    @FXML
    private Button btalterar;
    @FXML
    private Button btapagar;
    @FXML
    private Button btconfirmar;
    @FXML
    private Button btcancelar;
    @FXML
    private HBox pndados;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private PasswordField txbSenha;
    @FXML
    private TextField txbLogin;
    @FXML
    private TableColumn<Object, String> tcLogin;

    private char flag;
    private boolean EOriginal;
    private Usuario usuario;
    @FXML
    private Label lbErroLogin;
    @FXML
    private Label lbErroSenha;
    @FXML
    private Label lbErroNome;
    @FXML
    private Label lbErroRG;
    @FXML
    private Label lbErroCPF;
    @FXML
    private Label lbErroTelefone;
    @FXML
    private Label lbErroEMail;
    @FXML
    private Label lbErroEndereco;
    @FXML
    private Label lbErroSalario;
    @FXML
    private Label lbErroObs;
    @FXML
    private TextField txbCep;
    @FXML
    private Button btnConsultarCep;
    @FXML
    private Label lblErroCep;
    @FXML
    private ComboBox<Pais> cbPais;
    @FXML
    private Label lblErroPais;
    @FXML
    private Label lblErroEstado;
    @FXML
    private ComboBox<Estado> cbEstado;
    @FXML
    private ComboBox<Cidade> cbCidade;
    @FXML
    private Label lblErroCidade;
    @FXML
    private ComboBox<Bairro> cbBairro;
    @FXML
    private Label lblErroBairro;
    @FXML
    private ComboBox<Endereco> cbRua;
    @FXML
    private Label lblErroRua;
    @FXML
    private ImageView imgImagemUsuario;
    @FXML
    private Label lbErroImagem;
    @FXML
    private Button btnImagem;
    private BufferedImage imagem;
    @FXML
    private Button btnFichadoUsuario;
    @FXML
    private Group gpNivel;
    @FXML
    private Group gpTabela;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {

        tcNome.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcEmail.setCellValueFactory(new PropertyValueFactory("Email"));
        tcEndereco.setCellValueFactory(new PropertyValueFactory("Endereco"));
        tcNivel.setCellValueFactory(new PropertyValueFactory("Nivel"));
        tcCPF.setCellValueFactory(new PropertyValueFactory("CPF"));
        tcObs.setCellValueFactory(new PropertyValueFactory("Obs"));
        tcRG.setCellValueFactory(new PropertyValueFactory("RG"));
        tcSalario.setCellValueFactory(new PropertyValueFactory("Salario"));
        tcTelefone.setCellValueFactory(new PropertyValueFactory("Telefone"));
        tcLogin.setCellValueFactory(new PropertyValueFactory("Login"));

        estadoOriginal();
        CarregaTabela("");

        MaskFieldUtil.foneField(txbTelefone);
        MaskFieldUtil.cpfField(txbCPF);
        MaskFieldUtil.numericField(txbSalario);
        MaskFieldUtil.numericField(txbRG);
        MaskFieldUtil.maxField(txbRG, 10);
        MaskFieldUtil.maxField(txbSalario, 8);
        MaskFieldUtil.numericField(txbCep);

        ControledeAcesso.add(gpNivel, "Nivel", 11, 0);
        ControledeAcesso.add(gpTabela, "Tabela de Usuarios", 12, 0);

    }

    private void setAllErro(boolean Elogin, boolean Esenha, boolean Enome, boolean Erg, boolean Ecpf, boolean Etelefone, boolean Eemail, boolean Eendereco, boolean Esalario, boolean Eobs, boolean Ecep)
    {
        lbErroLogin.setVisible(Elogin);
        lbErroSenha.setVisible(Esenha);
        lbErroNome.setVisible(Enome);
        lbErroRG.setVisible(Erg);
        lbErroCPF.setVisible(Ecpf);
        lbErroTelefone.setVisible(Etelefone);
        lbErroEMail.setVisible(Eemail);
        lbErroEndereco.setVisible(Eendereco);
        lbErroSalario.setVisible(Esalario);
        lbErroObs.setVisible(Eobs);
        lblErroCep.setVisible(Ecep);
    }

    private void setErro(Label lb, String Msg)
    {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        CtrPessoa cp = new CtrPessoa();
        if (txbLogin.getText().trim().isEmpty() || !cp.get(txbLogin.getText(), 2, 1).isEmpty() || cp.get(txbLogin.getText(), 2, 1).isEmpty())
            if (txbLogin.getText().trim().isEmpty())
            {
                setErro(lbErroLogin, "Campo Obrigatório Vazio");
                fl = false;
                txbLogin.setText("");
                txbLogin.requestFocus();
            } else if (cp.get(txbLogin.getText(), 2, 1).isEmpty())
            {
                if (flag == 0)
                {
                    setErro(lbErroLogin, "Login Não Existente");
                    fl = false;
                    txbLogin.setText("");
                    txbLogin.requestFocus();
                }
            } else if (flag == 1)
            {
                setErro(lbErroLogin, "Login Ja Existe");
                fl = false;
                txbLogin.setText("");
                txbLogin.requestFocus();
            }
        if (txbRG.getText().trim().isEmpty())
        {
            setErro(lbErroRG, "Campo Obrigatório Vazio");
            txbRG.setText("");
            txbRG.requestFocus();
            fl = false;
        }
        if (txbCPF.getText().trim().isEmpty() || !Validar.isValidCPF(txbCPF.getText()))
        {
            if (txbCPF.getText().trim().isEmpty())
                setErro(lbErroCPF, "Campo Obrigatório Vazio");
            else
                setErro(lbErroCPF, "CPF Inválido");
            txbCPF.setText("");
            txbCPF.requestFocus();
            fl = false;
        }
        if (txbNome.getText().trim().isEmpty())
        {
            setErro(lbErroNome, "Campo Obrigatório Vazio");
            txbNome.setText("");
            txbNome.requestFocus();
            fl = false;
        }
        if (txbSalario.getText().trim().isEmpty())
        {
            setErro(lbErroSalario, "Campo Obrigatório Vazio");
            txbSalario.setText("");
            txbSalario.requestFocus();
            fl = false;
        }
        if (txbSenha.getText().trim().isEmpty())
        {
            setErro(lbErroSenha, "Campo Obrigatório Vazio");
            txbSenha.setText("");
            txbSenha.requestFocus();
            fl = false;
        }
        fl = TelaEnderecoController.valida(txbCep, lblErroCep);
        return fl;
    }

    public void selecionaPermissao()
    {
        if (ControledeAcesso.getControle().getTxNivelInterface().getText() == "Administrador")
            cbNivel.setDisable(false);
        else if (ControledeAcesso.getControle().getTxNivelInterface().getText() == "Gerente")
            cbNivel.setDisable(false);
        else
            cbNivel.setDisable(true);
    }

    @FXML
    private void evtNovo(ActionEvent event)
    {
        clear(pndados.getChildren());
        EstadoEdicao();
        txbLogin.setDisable(false);
        flag = 1;
    }

    @FXML
    private void evtAlterar(ActionEvent event)
    {
        EstadoEdicao();
        txbLogin.setDisable(false);
        flag = 0;
    }

    private Usuario geraUsuario()
    {
        CtrNivel cn = new CtrNivel();
        return usuario = new Usuario(Double.parseDouble(txbSalario.getText()),
                cn.get(cbNivel.getSelectionModel().getSelectedItem()).get(0),
                Integer.parseInt(txbSenha.getText()), txbNome.getText(),
                txbCPF.getText(), txbRG.getText(), txbTelefone.getText(),
                txbCep.getText(), txbEmail.getText(), taObs.getText(), txbLogin.getText(), imagem);
    }

    @FXML
    private void evtApagar(ActionEvent event)
    {
        CtrlPessoa cp = CtrlPessoa.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir"))
            cp.Remover(txbCPF.getText(), "Usuario");
        estadoOriginal();
        CarregaTabela("");
    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        CtrNivel cn = new CtrNivel();
        setAllErro(false, false, false, false, false, false, false, false, false, false, false);
        CtrlPessoa cp = CtrlPessoa.create();
        if (validaCampos())
            if (flag == 0)//alterar

                if (!cp.Alterar(txbNome.getText(), txbCPF.getText(), txbRG.getText(), txbTelefone.getText(),
                txbCep.getText(), txbEmail.getText(), taObs.getText(),Double.parseDouble(txbSalario.getText()),
                cn.get(cbNivel.getSelectionModel().getSelectedItem()).get(0),
                Integer.parseInt(txbSenha.getText()),  txbLogin.getText(), imagem))
                    Mensagem.Exibir(cp.getMsg(), 2);
                else
                {
                    Mensagem.Exibir(cp.getMsg(), 1);
                    estadoOriginal();
                }
            else//cadastrar
            if (!cp.Salvar(txbNome.getText(), txbCPF.getText(), txbRG.getText(), txbTelefone.getText(),
                txbCep.getText(), txbEmail.getText(), taObs.getText(),Double.parseDouble(txbSalario.getText()),
                cn.get(cbNivel.getSelectionModel().getSelectedItem()).get(0),
                Integer.parseInt(txbSenha.getText()),  txbLogin.getText(), imagem))
                Mensagem.Exibir(cp.getMsg(), 2);
            else
            {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginal();
            }
        else
            Mensagem.Exibir("Campos Inválidos!", 2);
        CarregaTabela("");
    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        if (EOriginal)
            MainFXMLController._pndados.getChildren().clear();
        else
            estadoOriginal();
    }

    @FXML
    private void evtBuscar()
    {
        CarregaTabela(txBusca.getText());
    }

    public void EstadoEdicao()
    {
        EOriginal = false;
        setAllErro(false, false, false, false, false, false, false, false, false, false, false);
        setButton(true, false, true, true, false);
        tabela.setDisable(true);
        pndados.setDisable(false);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar)
    {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
    }

    private void CarregaTabela(String filtro)
    {
        CtrlPessoa ctm = CtrlPessoa.create();
        try
        {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro, 1)));
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    private void estadoOriginal()
    {
        EOriginal = true;
        tabela.setDisable(false);

        setAllErro(false, false, false, false, false, false, false, false, false, false, false);
        setButton(false, true, true, true, false);
        pndados.setDisable(true);
        imagem = null;

        clear(pndados.getChildren());
        cbNivel.getItems().clear();
        CtrNivel cn = new CtrNivel();
        for (Nivel aux : cn.get(""))
        {
            if (aux.getVisivel() == '1')
                cbNivel.getItems().add(aux.getNome());
        }
        cbNivel.getSelectionModel().select(0);
        CarregaTabela("");
    }

    private void setCampos(Object p)
    {
        if (p != null)
        {
            CtrlPessoa.setCampos(p, txbNome, txbCPF, txbEmail, txbRG, txbTelefone, taObs, txbSalario, txbSenha, txbLogin, cbNivel);
            WritableImage im = Imagem.BufferedImageToImage(CtrlPessoa.getImagem(p));
            imgImagemUsuario.setImage(im);
            imagem = CtrlPessoa.getImagem(p);
            TelaEnderecoController.buscaEndereco(CtrlPessoa.getEndereco(p), txbCep, lblErroCep, cbBairro, cbRua, cbCidade, cbEstado, cbPais);
        }
    }

    @FXML
    private void ClicknaTabela(MouseEvent event)
    {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1)
        {
            Object p = tabela.getItems().get(lin);

            setCampos(p);

            setButton(true, true, false, false, false);

            flag = 0;
            EOriginal = false;

        }
    }

    public void clear(ObservableList<Node> pn)
    {
        ObservableList<Node> componentes = pn; //”limpa” os componentes
        for (Node n : componentes)
        {
            if (n instanceof Pane)
                clear(((Pane) n).getChildren());
            if (n instanceof TextInputControl) // textfield, textarea e htmleditor

                ((TextInputControl) n).setText("");
            if (n instanceof ComboBox)
                ((ComboBox) n).getItems().clear();
            if (n instanceof ImageView)
                ((ImageView) n).setImage(null);
        }
    }

    @FXML
    private void evtEscolheImagem(Event event)
    {
        Image im = Imagem.capturaImagem();
        if (im != null)
        {
            imagem = Imagem.ImageToBufferedImage(im);
            imgImagemUsuario.setImage(im);
        } else
            Mensagem.Exibir("Não Foi Possivel Abrir a Imagem", 2);
    }

    @FXML
    private void evtConsultaCep(ActionEvent event)
    {
        TelaEnderecoController.ConsultaCep(txbCep.getText(), txbCep, lblErroCep, cbBairro, cbRua, cbCidade, cbEstado, cbPais);
    }

    @FXML
    private void evtConsultaAvancada(MouseEvent event)
    {
        Parent root = null;
        try
        {
            /*stage.getIcons().add(new Image(getClass().getResourceAsStream("/IMG/logo_atlx.png")));*/
            TelaConsultaUsuarioController.setBtnPainel(true);
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/engenharia/ui/Consulta/TelaConsultaUsuario.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("engenharia/ui/estilo/DarkTheme.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.initStyle(StageStyle.DECORATED);
            stage.showAndWait();
            setCampos(TelaConsultaUsuarioController.getUsuario());
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela de Consulta");
        }
    }

    @FXML
    private void evtFichadoUsuario(MouseEvent event)
    {
        String sql = "select * from (select * from usuario where usr_login = '" + (String) (txbLogin.getText() != null ? txbLogin.getText() : txBusca.getText()) + "' ) as usuario inner join endereco on usuario.end_cep = endereco.end_cep inner join bairro on endereco.bai_cod = bairro.bai_cod inner join cidade on bairro.cid_cod = cidade.cid_cod inner join nivel on usuario.nivel_cod = nivel.nivel_cod";
        Relatorio.gerarRelatorio(sql, "Relatorios/MyReports/Rel_FichaUsuario.jasper", "Ficha do Usuario");
    }

}
