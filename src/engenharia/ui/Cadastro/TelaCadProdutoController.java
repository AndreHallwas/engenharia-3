/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engenharia.ui.Cadastro;

import Produto.Controladora.CtrlProduto;
import Produto.Entidade.Produto;
import Interface.Consulta.TelaConsultaUsuarioController;
import Utils.Imagem;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import engenharia.ui.MainFXMLController;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadProdutoController implements Initializable {

    @FXML
    private HBox pndados;
    @FXML
    private TextField txbNome;
    @FXML
    private Label lbErroNome;
    @FXML
    private TextField txbPreco;
    @FXML
    private Label lbErroPreco;
    @FXML
    private TextField txbNomeFarmaceutico;
    @FXML
    private Label lbErroNomeFarmaceutico;
    @FXML
    private TextField txbPrecoCon;
    @FXML
    private Label lbErroPrecoCon;
    @FXML
    private TextArea taObs;
    @FXML
    private Label lbErroObs;
    @FXML
    private TextField txbLocalNum;
    @FXML
    private Label lbErroLocalNum;
    @FXML
    private TextField txbLocalDesc;
    @FXML
    private Label lbErroLocalDesc;
    @FXML
    private TextField txbCNPJ;
    @FXML
    private Label lbErroCNPJ;
    @FXML
    private TextField txbTipo;
    @FXML
    private Label lbErroTipo;
    @FXML
    private TextField txbObsMedida;
    @FXML
    private Label lbErroObsMedida;
    @FXML
    private ImageView imgImagemProduto;
    @FXML
    private Label lbErroImagem;
    @FXML
    private Button btnImagem;
    @FXML
    private Button btnovo;
    @FXML
    private Button btalterar;
    @FXML
    private Button btapagar;
    @FXML
    private Button btconfirmar;
    @FXML
    private Button btcancelar;
    @FXML
    private TextField txBusca;
    @FXML
    private Group gpTabela;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcNome;
    @FXML
    private TableColumn<Object, String> tcNomeFarmacologico;
    @FXML
    private TableColumn<Object, Double> tcPreco;
    @FXML
    private TableColumn<Object, Double> tcPrecoCom;
    @FXML
    private TableColumn<Object, String> tcTipo;
    @FXML
    private TableColumn<Object, String> tcMedida;
    @FXML
    private TableColumn<Object, String> tcObs;
    private boolean EOriginal;
    private int flag;
    private BufferedImage imagem;
    private static Produto produto;
    private String codigo;

    /**
     * Initializes the controller class.
     */
    
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcNome.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcNomeFarmacologico.setCellValueFactory(new PropertyValueFactory("NomeF"));
        tcPreco.setCellValueFactory(new PropertyValueFactory("PrecoFab"));
        tcPrecoCom.setCellValueFactory(new PropertyValueFactory("PrecoCom"));
        tcTipo.setCellValueFactory(new PropertyValueFactory("Tipo"));
        tcMedida.setCellValueFactory(new PropertyValueFactory("Medida_Prod"));
        tcObs.setCellValueFactory(new PropertyValueFactory("Obs"));
        flag = 2;

        estadoOriginal();
        CarregaTabela("");

        MaskFieldUtil.cnpjField(txbCNPJ);
        MaskFieldUtil.numericField(txbPreco);
        MaskFieldUtil.numericField(txbPrecoCon);
        MaskFieldUtil.numericField(txbObsMedida);

        /////CtrlAcesso.add(gpTabela, "Tabela de Produto", 13, 0);
    }

    private void setAllErro(boolean Ecnpj, boolean Enome, boolean Eobs, boolean Eimagem, boolean Elocaldesc, boolean Elocalnum, boolean EnomeF, boolean EobsMedida, boolean Epreco, boolean Eprecocon, boolean Etipo)
    {
        lbErroCNPJ.setVisible(Ecnpj);
        lbErroNome.setVisible(Enome);
        lbErroObs.setVisible(Eobs);
        lbErroImagem.setVisible(Eimagem);
        lbErroLocalDesc.setVisible(Elocaldesc);
        lbErroLocalNum.setVisible(Elocalnum);
        lbErroNomeFarmaceutico.setVisible(EnomeF);
        lbErroObsMedida.setVisible(EobsMedida);
        lbErroPreco.setVisible(Epreco);
        lbErroPrecoCon.setVisible(Eprecocon);
        lbErroTipo.setVisible(Etipo);
    }

    private void setErro(Label lb, String Msg)
    {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    @FXML
    private void evtEscolheImagem(Event event)
    {
        Image im = Imagem.capturaImagem();
        if (im != null)
        {
            imagem = Imagem.ImageToBufferedImage(im);
            imgImagemProduto.setImage(im);
        } else
            Mensagem.Exibir("Não Foi Possivel Abrir a Imagem", 2);
    }

    @FXML
    private void evtNovo(ActionEvent event)
    {
        clear(pndados.getChildren());
        EstadoEdicao();
        txbNome.setDisable(false);
        flag = 1;
    }

    @FXML
    private void evtAlterar(ActionEvent event)
    {
        EstadoEdicao();
        txbCNPJ.setDisable(true);
        flag = 0;
    }

    @FXML
    private void evtApagar(ActionEvent event)
    {
        CtrlProduto cf = CtrlProduto.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir"))
            cf.Remover(codigo);
        estadoOriginal();
        CarregaTabela("");
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        CtrlProduto cf = CtrlProduto.create();
        if (txbNome.getText().trim().isEmpty() || !cf.Pesquisar(txbNome.getText()).isEmpty())
            if (txbCNPJ.getText().trim().isEmpty())
            {
                setErro(lbErroNome, "Campo Obrigatório Vazio");
                fl = false;
                txbCNPJ.setText("");
                txbCNPJ.requestFocus();
            } else if (cf.Pesquisar(txbNome.getText()).isEmpty())
            {
                if (flag == 0)
                {
                    setErro(lbErroNome, "Produto Não Existente");
                    fl = false;
                    txbCNPJ.setText("");
                    txbCNPJ.requestFocus();
                }
            } else if (flag == 1)
            {
                setErro(lbErroNome, "Produto Ja Existe");
                fl = false;
                txbCNPJ.setText("");
                txbCNPJ.requestFocus();
            }
        if (txbPreco.getText().trim().isEmpty())
        {
            setErro(lbErroPreco, "Campo Obrigatório Vazio");
            txbPreco.setText("");
            txbPreco.requestFocus();
            fl = false;
        }
        if (txbTipo.getText().trim().isEmpty())
        {
            setErro(lbErroTipo, "Campo Obrigatório Vazio");
            txbTipo.setText("");
            txbTipo.requestFocus();
            fl = false;
        }
        if (txbPrecoCon.getText().trim().isEmpty())
        {
            setErro(lbErroPrecoCon, "Campo Obrigatório Vazio");
            txbPrecoCon.setText("");
            txbPrecoCon.requestFocus();
            fl = false;
        }
        if (txbObsMedida.getText().trim().isEmpty())
        {
            setErro(lbErroObsMedida, "Campo Obrigatório Vazio");
            txbObsMedida.setText("");
            txbObsMedida.requestFocus();
            fl = false;
        }
        return fl;
    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        setAllErro(false, false, false, false, false, false, false, false, false, false, false);
        CtrlProduto cf = CtrlProduto.create();
        if (validaCampos())
            if (flag == 0)//alterar

                if (!cf.Alterar(codigo, txbNome.getText(), taObs.getText(),
                txbNomeFarmaceutico.getText(), Double.parseDouble(txbPreco.getText()),
                Double.parseDouble(txbPrecoCon.getText()), txbCNPJ.getText(),
                txbTipo.getText(), txbLocalDesc.getText(), txbLocalNum.getText(),
                txbObsMedida.getText(), imagem))
                    Mensagem.Exibir(cf.getMsg(), 2);
                else
                {
                    Mensagem.Exibir(cf.getMsg(), 1);
                    estadoOriginal();
                }
            else//cadastrar
            if (!cf.Salvar(codigo, txbNome.getText(), taObs.getText(),
                txbNomeFarmaceutico.getText(), Double.parseDouble(txbPreco.getText()),
                Double.parseDouble(txbPrecoCon.getText()), txbCNPJ.getText(),
                txbTipo.getText(), txbLocalDesc.getText(), txbLocalNum.getText(),
                txbObsMedida.getText(), imagem))
                Mensagem.Exibir(cf.getMsg(), 2);
            else
            {
                Mensagem.Exibir(cf.getMsg(), 1);
                estadoOriginal();
            }
        else
            Mensagem.Exibir("Campos Inválidos!", 2);
        CarregaTabela("");
    }

    private Produto geraProduto()
    {
        /*CtrlFabricante cf = new CtrlFabricante();*/
        return produto = new Produto(codigo, txbNome.getText(), taObs.getText(),
                txbNomeFarmaceutico.getText(), Double.parseDouble(txbPreco.getText()),
                Double.parseDouble(txbPrecoCon.getText()), /*cf.Pesquisar(txbCNPJ.getText()).get(0)*/null,
                txbTipo.getText(), txbLocalDesc.getText(), txbLocalNum.getText(),
                txbObsMedida.getText(), imagem);
    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        if (EOriginal)
            MainFXMLController._pndados.getChildren().clear();
        else
            estadoOriginal();
    }

    @FXML
    private void evtBuscar(Event event)
    {
        CarregaTabela(txBusca.getText());
    }

    public void EstadoEdicao()
    {
        EOriginal = false;
        setAllErro(false, false, false, false, false, false, false, false, false, false, false);
        setButton(true, false, true, true, false);
        tabela.setDisable(true);
        pndados.setDisable(false);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar)
    {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
    }

    private void CarregaTabela(String filtro)
    {
        CtrlProduto ctf = CtrlProduto.create();
        try
        {
            tabela.setItems(FXCollections.observableList(ctf.Pesquisar(filtro)));
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    private void estadoOriginal()
    {
        EOriginal = true;
        tabela.setDisable(false);

        setAllErro(false, false, false, false, false, false, false, false, false, false, false);
        setButton(false, true, true, true, false);
        pndados.setDisable(true);
        imagem = null;

        clear(pndados.getChildren());
        CarregaTabela("");
    }

    private void setCampos(Object f)
    {
        if (f != null)
        {
            CtrlProduto.setCampos(f, txbNome, txbPreco, txbPrecoCon, txbObsMedida, txbNomeFarmaceutico, txbLocalNum, txbLocalDesc, txbObsMedida, txbTipo, txbCNPJ, taObs);
            codigo = CtrlProduto.getCodigo(f);
            
            /////WritableImage im = Imagem.BufferedImageToImage(((Produto) f).getImagem());
            /////imgImagemProduto.setImage(im);
            /////imagem = ((Produto) f).getImagem();
        }
    }

    @FXML
    private void evtConsultaAvancada(MouseEvent event)
    {
        Parent root = null;
        try
        {
            /*stage.getIcons().add(new Image(getClass().getResourceAsStream("/IMG/logo_atlx.png")));*/
            TelaConsultaUsuarioController.setBtnPainel(true);
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/engenharia/ui/Consulta/TelaConsultaProduto.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("engenharia/ui/estilo/DarkTheme.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.initStyle(StageStyle.DECORATED);
            stage.showAndWait();
            /////setCampos(TelaConsultaProdutoController.getProduto());
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela de Consulta");
        }
    }


    @FXML
    private void ClicknaTabela(MouseEvent event)
    {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1)
        {
            Object f = tabela.getItems().get(lin);

            setCampos(f);

            setButton(true, true, false, false, false);

            flag = 0;
            EOriginal = false;

        }
    }

    public void clear(ObservableList<Node> pn)
    {
        ObservableList<Node> componentes = pn; //”limpa” os componentes
        for (Node n : componentes)
        {
            if (n instanceof Pane)
                clear(((Pane) n).getChildren());
            if (n instanceof TextInputControl) // textfield, textarea e htmleditor
                ((TextInputControl) n).setText("");
            if (n instanceof ComboBox)
                ((ComboBox) n).getItems().clear();
            if (n instanceof ImageView)
                ((ImageView) n).setImage(null);
        }
    }

    @FXML
    private void evtEspera(ActionEvent event) {
        if(flag == 0){
            geraProduto();
            Parent root = null;
            try
            {
                /*stage.getIcons().add(new Image(getClass().getResourceAsStream("/IMG/logo_atlx.png")));*/
                Stage stage = new Stage();
                root = FXMLLoader.load(getClass().getResource("/engenharia/ui/Cadastro/TelaCadObserver.fxml"));
                Scene scene = new Scene(root);
                scene.getStylesheets().add("engenharia/ui/estilo/DarkTheme.css");
                stage.setScene(scene);
                stage.setMaximized(false);
                stage.initStyle(StageStyle.DECORATED);
                stage.showAndWait();
            } catch (IOException ex)
            {
                Mensagem.ExibirException(ex, "Erro ao Carregar Tela de Consulta");
            }
        }else{
            Mensagem.Exibir("Selecione um Produto !", 1);
        }
    }

    public static Produto getProduto() {
        return produto;
    }

    
}
