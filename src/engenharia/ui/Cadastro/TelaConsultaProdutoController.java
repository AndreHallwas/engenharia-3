/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Gerenciamento;

import Produto.Controladora.CtrlProduto;
import Produto.Entidade.Produto;
import Utils.Mensagem;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Drake
 */
public class TelaConsultaProdutoController implements Initializable
{

    @FXML
    private TextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcNome;
    @FXML
    private TableColumn<Object, String> tcObs;
    @FXML
    private TableColumn<Object, String> tcNomeFarmacologico;
    @FXML
    private TableColumn<Object, String> tcPreco;
    @FXML
    private TableColumn<Object, String> tcPrecoCom;
    @FXML
    private TableColumn<Object, String> tcTipo;
    @FXML
    private TableColumn<Object, String> tcMedida;
    
    private static Object produto;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcNome.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcNomeFarmacologico.setCellValueFactory(new PropertyValueFactory("NomeF"));
        tcPreco.setCellValueFactory(new PropertyValueFactory("PrecoFab"));
        tcPrecoCom.setCellValueFactory(new PropertyValueFactory("PrecoCom"));
        tcTipo.setCellValueFactory(new PropertyValueFactory("Tipo"));
        tcMedida.setCellValueFactory(new PropertyValueFactory("Medida_Prod"));
        tcObs.setCellValueFactory(new PropertyValueFactory("Obs"));
        CarregaTabela("");
        produto = null;
    }

    @FXML
    private void evtBuscar()
    {
        CarregaTabela(txBusca.getText());
    }

    private void CarregaTabela(String filtro)
    {
        CtrlProduto ctm = CtrlProduto.create();
        try
        {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro)));
        }
        catch(Exception ex)
        {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void ClicknaTabela(MouseEvent event)
    {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1)
            produto = tabela.getItems().get(lin);
    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        if (produto != null)
            evtCancelar(event);

    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        Stage stage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
        stage.close();/////fexa janela
    }

    public static Object getProduto()
    {
        return produto;
    }

}
