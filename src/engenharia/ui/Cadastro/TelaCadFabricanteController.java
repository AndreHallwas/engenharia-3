/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engenharia.ui.Cadastro;

import Produto.Controladora.CtrlFabricante;
import Endereco.Entidade.Bairro;
import Endereco.Entidade.Cidade;
import Endereco.Entidade.Endereco;
import Endereco.Entidade.Estado;
import Endereco.Entidade.Pais;
import Produto.Entidade.Fabricante;
import Produto.Dao.CtrFabricante;
import Interface.Consulta.TelaConsultaUsuarioController;
import Interface.Gerenciamento.TelaEnderecoController;
import Utils.Imagem;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import engenharia.ui.MainFXMLController;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadFabricanteController implements Initializable
{

    @FXML
    private HBox pndados;
    @FXML
    private TextField txbNome;
    @FXML
    private Label lbErroNome;
    @FXML
    private TextField txbCNPJ;
    @FXML
    private Label lbErroCNPJ;
    @FXML
    private TextField txbTelefone;
    @FXML
    private Label lbErroTelefone;
    @FXML
    private TextField txbEmail;
    @FXML
    private Label lbErroEMail;
    @FXML
    private TextArea taObs;
    @FXML
    private Label lbErroObs;
    @FXML
    private TextField txbCep;
    @FXML
    private Button btnConsultarCep;
    @FXML
    private Label lblErroCep;
    @FXML
    private ComboBox<Pais> cbPais;
    @FXML
    private Label lblErroPais;
    @FXML
    private Label lblErroEstado;
    @FXML
    private ComboBox<Estado> cbEstado;
    @FXML
    private ComboBox<Cidade> cbCidade;
    @FXML
    private Label lblErroCidade;
    @FXML
    private ComboBox<Bairro> cbBairro;
    @FXML
    private Label lblErroBairro;
    @FXML
    private ComboBox<Endereco> cbRua;
    @FXML
    private Label lblErroRua;
    @FXML
    private ImageView imgImagemFabricante;
    @FXML
    private Label lbErroImagem;
    @FXML
    private Button btnImagem;
    @FXML
    private Button btnovo;
    @FXML
    private Button btalterar;
    @FXML
    private Button btapagar;
    @FXML
    private Button btconfirmar;
    @FXML
    private Button btcancelar;
    @FXML
    private TextField txBusca;
    @FXML
    private Button btnFichadoFabricante;
    @FXML
    private Group gpTabela;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcNome;
    @FXML
    private TableColumn<Object, String> tcCNPJ;
    @FXML
    private TableColumn<Object, String> tcTelefone;
    @FXML
    private TableColumn<Object, String> tcEndereco;
    @FXML
    private TableColumn<Object, String> tcEmail;
    @FXML
    private TableColumn<Object, String> tcObs;
    private char flag;
    private boolean EOriginal;
    private BufferedImage imagem;
    private Fabricante fornecedor;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcNome.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcEmail.setCellValueFactory(new PropertyValueFactory("Email"));
        tcCNPJ.setCellValueFactory(new PropertyValueFactory("CNPJ"));
        tcObs.setCellValueFactory(new PropertyValueFactory("Obs"));
        tcTelefone.setCellValueFactory(new PropertyValueFactory("Telefone"));

        estadoOriginal();
        CarregaTabela("");

        MaskFieldUtil.cnpjField(txbCNPJ);
        MaskFieldUtil.foneField(txbTelefone);
        MaskFieldUtil.numericField(txbCep);

        //////CtrlAcesso.add(gpTabela, "Tabela de Fabricante", 13, 0);
    }

    private void setAllErro(boolean Ecnpj, boolean Eemail, boolean Etelefone, boolean Enome, boolean Eobs, boolean Ecep)
    {
        lbErroCNPJ.setVisible(Ecnpj);
        lbErroEMail.setVisible(Eemail);
        lbErroTelefone.setVisible(Etelefone);
        lbErroNome.setVisible(Enome);
        lbErroObs.setVisible(Eobs);
        lblErroCep.setVisible(Ecep);
    }

    private void setErro(Label lb, String Msg)
    {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    @FXML
    private void evtConsultaCep(ActionEvent event)
    {
        TelaEnderecoController.ConsultaCep(txbCep.getText(), txbCep, lblErroCep, cbBairro, cbRua, cbCidade, cbEstado, cbPais);
    }

    @FXML
    private void evtEscolheImagem(Event event)
    {
        Image im = Imagem.capturaImagem();
        if (im != null)
        {
            imagem = Imagem.ImageToBufferedImage(im);
            imgImagemFabricante.setImage(im);
        } else
            Mensagem.Exibir("Não Foi Possivel Abrir a Imagem", 2);
    }

    @FXML
    private void evtNovo(ActionEvent event)
    {
        clear(pndados.getChildren());
        EstadoEdicao();
        txbCNPJ.setDisable(false);
        flag = 1;
    }

    @FXML
    private void evtAlterar(ActionEvent event)
    {
        EstadoEdicao();
        txbCNPJ.setDisable(true);
        flag = 0;
    }

    @FXML
    private void evtApagar(ActionEvent event)
    {
        CtrlFabricante cf = CtrlFabricante.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir"))
            cf.Remover(txbCNPJ.getText());
        estadoOriginal();
        CarregaTabela("");
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        CtrFabricante cf = new CtrFabricante();
        if (txbCNPJ.getText().trim().isEmpty() || !cf.get(txbCNPJ.getText()).isEmpty())
            if (txbCNPJ.getText().trim().isEmpty())
            {
                setErro(lbErroCNPJ, "Campo Obrigatório Vazio");
                fl = false;
                txbCNPJ.setText("");
                txbCNPJ.requestFocus();
            } else if (cf.get(txbCNPJ.getText()).isEmpty())
            {
                if (flag == 0)
                {
                    setErro(lbErroCNPJ, "CNPJ Não Existente");
                    fl = false;
                    txbCNPJ.setText("");
                    txbCNPJ.requestFocus();
                }
            } else if (flag == 1)
            {
                setErro(lbErroCNPJ, "CNPJ Ja Existe");
                fl = false;
                txbCNPJ.setText("");
                txbCNPJ.requestFocus();
            }
        if (txbNome.getText().trim().isEmpty())
        {
            setErro(lbErroNome, "Campo Obrigatório Vazio");
            txbNome.setText("");
            txbNome.requestFocus();
            fl = false;
        }
        if(!TelaEnderecoController.valida(txbCep, lblErroCep)){
            fl = false;
        }
        return fl;
    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        setAllErro(false, false, false, false, false, false);
        CtrlFabricante cf = CtrlFabricante.create();
        if (validaCampos())
            if (flag == 0)//alterar

                if (!cf.Alterar(txbNome.getText(), txbCNPJ.getText(), txbCep.getText(),
                txbTelefone.getText(), txbEmail.getText(), tcObs.getText(), imagem))
                    Mensagem.Exibir(cf.getMsg(), 2);
                else
                {
                    Mensagem.Exibir(cf.getMsg(), 1);
                    estadoOriginal();
                }
            else//cadastrar
            if (!cf.Salvar(txbNome.getText(), txbCNPJ.getText(), txbCep.getText(),
                txbTelefone.getText(), txbEmail.getText(), tcObs.getText(), imagem))
                Mensagem.Exibir(cf.getMsg(), 2);
            else
            {
                Mensagem.Exibir(cf.getMsg(), 1);
                estadoOriginal();
            }
        else
            Mensagem.Exibir("Campos Inválidos!", 2);
        CarregaTabela("");
    }

    private Fabricante geraFabricante()
    {
        return fornecedor = new Fabricante(txbNome.getText(), txbCNPJ.getText(), txbCep.getText(),
                txbTelefone.getText(), txbEmail.getText(), tcObs.getText(), imagem);
    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        if (EOriginal)
            MainFXMLController._pndados.getChildren().clear();
        else
            estadoOriginal();
    }

    @FXML
    private void evtBuscar(Event event)
    {
        CarregaTabela(txBusca.getText());
    }

    public void EstadoEdicao()
    {
        EOriginal = false;
        setAllErro(false, false, false, false, false, false);
        setButton(true, false, true, true, false);
        tabela.setDisable(true);
        pndados.setDisable(false);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar)
    {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
    }

    private void CarregaTabela(String filtro)
    {
        CtrlFabricante ctf = CtrlFabricante.create();
        try
        {
            tabela.setItems(FXCollections.observableList(ctf.Pesquisar(filtro)));
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    private void estadoOriginal()
    {
        EOriginal = true;
        tabela.setDisable(false);

        setAllErro(false, false, false, false, false, false);
        setButton(false, true, true, true, false);
        pndados.setDisable(true);
        imagem = null;

        clear(pndados.getChildren());
        CarregaTabela("");
    }

    private void setCampos(Fabricante f)
    {
        if (f != null)
        {
            txbNome.setText(f.getNome());
            txbCNPJ.setText(f.getCNPJ());
            txbEmail.setText(f.getEmail());
            txbTelefone.setText(f.getTelefone());
            taObs.setText(f.getObs());
            WritableImage im = Imagem.BufferedImageToImage(((Fabricante) f).getImagem());
            imgImagemFabricante.setImage(im);
            imagem = ((Fabricante) f).getImagem();
            TelaEnderecoController.buscaEndereco(((Fabricante) f).getCep(), txbCep, lblErroCep, cbBairro, cbRua, cbCidade, cbEstado, cbPais);
        }
    }

    @FXML
    private void evtConsultaAvancada(MouseEvent event)
    {
        Parent root = null;
        try
        {
            /*stage.getIcons().add(new Image(getClass().getResourceAsStream("/IMG/logo_atlx.png")));*/
            TelaConsultaUsuarioController.setBtnPainel(true);
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/engenharia/ui/Consulta/TelaConsultaFabricante.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("engenharia/ui/estilo/DarkTheme.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.initStyle(StageStyle.DECORATED);
            stage.showAndWait();
            /////setCampos(TelaConsultaFabricanteController.getFabricante());
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela de Consulta");
        }
    }

    @FXML
    private void evtFichadoFabricante(MouseEvent event)
    {
        /*String sql = "select * from (select * from usuario where usr_login = '"+(String)(txbLogin.getText()!=null?txbLogin.getText() : txBusca.getText())+"' ) as usuario inner join endereco on usuario.end_cep = endereco.end_cep inner join bairro on endereco.bai_cod = bairro.bai_cod inner join cidade on bairro.cid_cod = cidade.cid_cod inner join nivel on usuario.nivel_cod = nivel.nivel_cod";
        Relatorio.gerarRelatorio(sql, "Relatorios/MyReports/Rel_FichaUsuario.jasper","Ficha do Usuario");*/
    }

    @FXML
    private void ClicknaTabela(MouseEvent event)
    {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1)
        {
            Fabricante f = (Fabricante) tabela.getItems().get(lin);

            setCampos(f);

            setButton(true, true, false, false, false);

            flag = 0;
            EOriginal = false;

        }
    }

    public void clear(ObservableList<Node> pn)
    {
        ObservableList<Node> componentes = pn; //”limpa” os componentes
        for (Node n : componentes)
        {
            if (n instanceof Pane)
                clear(((Pane) n).getChildren());
            if (n instanceof TextInputControl) // textfield, textarea e htmleditor

                ((TextInputControl) n).setText("");
            if (n instanceof ComboBox)
                ((ComboBox) n).getItems().clear();
            if (n instanceof ImageView)
                ((ImageView) n).setImage(null);
        }
    }

}
