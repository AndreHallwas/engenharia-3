/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engenharia.ui.Cadastro;

import Pessoa.Controladora.CtrlPessoa;
import Endereco.Entidade.Bairro;
import Endereco.Entidade.Cidade;
import Pessoa.Entidade.Cliente;
import Endereco.Entidade.Endereco;
import Endereco.Entidade.Estado;
import Endereco.Entidade.Pais;
import Pessoa.Dao.CtrPessoa;
import Interface.Consulta.TelaConsultaClienteController;
import Interface.Consulta.TelaConsultaUsuarioController;
import Interface.Gerenciamento.TelaEnderecoController;
import Utils.Imagem;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.Validar;
import engenharia.ui.MainFXMLController;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadClienteController implements Initializable {

    @FXML
    private HBox pndados;
    @FXML
    private TextField txbNome;
    @FXML
    private Label lbErroNome;
    @FXML
    private TextField txbRG;
    @FXML
    private Label lbErroRG;
    @FXML
    private TextField txbCPF;
    @FXML
    private Label lbErroCPF;
    @FXML
    private TextField txbTelefone;
    @FXML
    private Label lbErroTelefone;
    @FXML
    private TextField txbEmail;
    @FXML
    private Label lbErroEMail;
    @FXML
    private TextField txbEndereco;
    @FXML
    private Label lbErroEndereco;
    @FXML
    private TextArea taObs;
    @FXML
    private Label lbErroObs;
    @FXML
    private TextField txbCep;
    @FXML
    private Button btnConsultarCep;
    @FXML
    private Label lblErroCep;
    @FXML
    private ComboBox<Pais> cbPais;
    @FXML
    private Label lblErroPais;
    @FXML
    private Label lblErroEstado;
    @FXML
    private ComboBox<Estado> cbEstado;
    @FXML
    private ComboBox<Cidade> cbCidade;
    @FXML
    private Label lblErroCidade;
    @FXML
    private ComboBox<Bairro> cbBairro;
    @FXML
    private Label lblErroBairro;
    @FXML
    private ComboBox<Endereco> cbRua;
    @FXML
    private Label lblErroRua;
    @FXML
    private ImageView imgImagemUsuario;
    @FXML
    private Label lbErroImagem;
    @FXML
    private Button btnImagem;
    @FXML
    private Button btnovo;
    @FXML
    private Button btalterar;
    @FXML
    private Button btapagar;
    @FXML
    private Button btconfirmar;
    @FXML
    private Button btcancelar;
    @FXML
    private TextField txBusca;
    @FXML
    private Button btnFichadoUsuario;
    @FXML
    private Group gpTabela;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcNome;
    @FXML
    private TableColumn<Object, String> tcRG;
    @FXML
    private TableColumn<Object, String> tcCPF;
    @FXML
    private TableColumn<Object, String> tcTelefone;
    @FXML
    private TableColumn<Object, String> tcEndereco;
    @FXML
    private TableColumn<Object, String> tcEmail;
    @FXML
    private TableColumn<Object, String> tcObs;
    private int flag;
    private static Cliente cliente;
    private BufferedImage imagem;
    private boolean EOriginal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcNome.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcEmail.setCellValueFactory(new PropertyValueFactory("Email"));
        tcEndereco.setCellValueFactory(new PropertyValueFactory("Endereco"));
        tcCPF.setCellValueFactory(new PropertyValueFactory("CPF"));
        tcObs.setCellValueFactory(new PropertyValueFactory("Obs"));
        tcRG.setCellValueFactory(new PropertyValueFactory("RG"));
        tcTelefone.setCellValueFactory(new PropertyValueFactory("Telefone"));
        flag = 2;
        
        estadoOriginal();
        CarregaTabela("");

        MaskFieldUtil.foneField(txbTelefone);
        MaskFieldUtil.cpfField(txbCPF);
        MaskFieldUtil.numericField(txbRG);
        MaskFieldUtil.maxField(txbRG, 10);
        MaskFieldUtil.numericField(txbCep);
        
        /////CtrlAcesso.add(gpTabela, "Tabela de Clientes", 12, 0);
    }    
    
    private void setAllErro(boolean Enome, boolean Erg, boolean Ecpf, boolean Etelefone, boolean Eemail, boolean Eendereco, boolean Eobs, boolean Ecep)
    {
        lbErroNome.setVisible(Enome);
        lbErroRG.setVisible(Erg);
        lbErroCPF.setVisible(Ecpf);
        lbErroTelefone.setVisible(Etelefone);
        lbErroEMail.setVisible(Eemail);
        lbErroEndereco.setVisible(Eendereco);
        lbErroObs.setVisible(Eobs);
        lblErroCep.setVisible(Ecep);
    }
    
    private void setErro(Label lb, String Msg)
    {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }
    
    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        CtrPessoa cp = new CtrPessoa();
        if (txbCPF.getText().trim().isEmpty() || !Validar.isValidCPF(txbCPF.getText()))
        {
            if (txbCPF.getText().trim().isEmpty())
                setErro(lbErroCPF, "Campo Obrigatório Vazio");
            else
                setErro(lbErroCPF, "CPF Inválido");
            txbCPF.setText("");
            txbCPF.requestFocus();
            fl = false;
        }
        if (txbCPF.getText().trim().isEmpty() || !cp.get(txbCPF.getText(), 2, 2).isEmpty() || cp.get(txbCPF.getText(), 2, 2).isEmpty())
            if (txbCPF.getText().trim().isEmpty())
            {
                setErro(lbErroCPF, "Campo Obrigatório Vazio");
                fl = false;
                txbCPF.setText("");
                txbCPF.requestFocus();
            } else if (cp.get(txbCPF.getText(), 2, 2).isEmpty())
            {
                if (flag == 0)
                {
                    setErro(lbErroCPF, "CPF Não Existente");
                    fl = false;
                    txbCPF.setText("");
                    txbCPF.requestFocus();
                }
            } else if (flag == 1)
            {
                setErro(lbErroCPF, "CPF Ja Existe");
                fl = false;
                txbCPF.setText("");
                txbCPF.requestFocus();
            }
        if (txbRG.getText().trim().isEmpty())
        {
            setErro(lbErroRG, "Campo Obrigatório Vazio");
            txbRG.setText("");
            txbRG.requestFocus();
            fl = false;
        }
        if (txbNome.getText().trim().isEmpty())
        {
            setErro(lbErroNome, "Campo Obrigatório Vazio");
            txbNome.setText("");
            txbNome.requestFocus();
            fl = false;
        }
        fl = !fl? false : TelaEnderecoController.valida(txbCep, lblErroCep);
        return fl;
    }

    @FXML
    private void evtConsultaCep(ActionEvent event) {
        TelaEnderecoController.ConsultaCep(txbCep.getText(), txbCep, lblErroCep, cbBairro, cbRua, cbCidade, cbEstado, cbPais);
    }

    @FXML
    private void evtEscolheImagem(Event event) {
        Image im = Imagem.capturaImagem();
        if (im != null)
        {
            imagem = Imagem.ImageToBufferedImage(im);
            imgImagemUsuario.setImage(im);
        } else
            Mensagem.Exibir("Não Foi Possivel Abrir a Imagem", 2);
    }
    @FXML
    private void evtNovo(ActionEvent event) {
        clear(pndados.getChildren());
        EstadoEdicao();
        txbCPF.setDisable(false);
        flag = 1;
    }

    @FXML
    private void evtAlterar(ActionEvent event) {
        EstadoEdicao();
        txbCPF.setDisable(true);
        flag = 0;
    }
    
    private Cliente geraCliente()
    {
        return cliente = new Cliente(txbNome.getText(),
                txbCPF.getText(), txbRG.getText(), txbTelefone.getText(),
                txbCep.getText(), txbEmail.getText(), taObs.getText()/*, imagem*/);
    }
    
    @FXML
    private void evtApagar(ActionEvent event) {
        CtrlPessoa cp = CtrlPessoa.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir"))
            cp.Remover(txbCPF.getText(),"Cliente");
        estadoOriginal();
        CarregaTabela("");
    }

    @FXML
    private void evtConfirmar(ActionEvent event) {
        
        setAllErro(false, false, false, false, false, false, false, false);
        CtrlPessoa cp = CtrlPessoa.create();
        if (validaCampos())
            if (flag == 0)//alterar

                if (!cp.Alterar(txbNome.getText(),
                txbCPF.getText(), txbRG.getText(), txbTelefone.getText(),
                txbCep.getText(), txbEmail.getText(), taObs.getText(), 0, null, 0, null, imagem))
                    Mensagem.Exibir(cp.getMsg(), 2);
                else
                {
                    Mensagem.Exibir(cp.getMsg(), 1);
                    estadoOriginal();
                }
            else//cadastrar
            if (!cp.Salvar(txbNome.getText(),
                txbCPF.getText(), txbRG.getText(), txbTelefone.getText(),
                txbCep.getText(), txbEmail.getText(), taObs.getText(), 0, null, 0, null, imagem))
                Mensagem.Exibir(cp.getMsg(), 2);
            else
            {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginal();
            }
        else
            Mensagem.Exibir("Campos Inválidos!", 2);
        CarregaTabela("");
    }

    @FXML
    private void evtCancelar(ActionEvent event) {
        if (EOriginal)
            MainFXMLController._pndados.getChildren().clear();
        else
            estadoOriginal();
    }

    @FXML
    private void evtBuscar(Event event) {
        CarregaTabela(txBusca.getText());
    }

    public void EstadoEdicao()
    {
        EOriginal = false;
        setAllErro( false, false, false, false, false, false, false, false);
        setButton(true, false, true, true, false);
        tabela.setDisable(true);
        pndados.setDisable(false);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar)
    {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
    }

    private void CarregaTabela(String filtro)
    {
        CtrlPessoa ctm = CtrlPessoa.create();
        try
        {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro, 2)));
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    private void estadoOriginal()
    {
        EOriginal = true;
        tabela.setDisable(false);

        setAllErro(false, false, false, false, false, false, false, false);
        setButton(false, true, true, true, false);
        pndados.setDisable(true);
        imagem = null;

        clear(pndados.getChildren());
        CarregaTabela("");
    }

    private void setCampos(Object p)
    {
        if (p != null)
        {   
            CtrlPessoa.setCampos(p, txbNome, txbCPF, txbEmail, txbRG, txbTelefone, taObs);
            
            /*txbNome.setText(p.getNome());
            txbCPF.setText(p.getCPF());
            txbEmail.setText(p.getEmail());
            txbRG.setText(p.getRG());
            txbTelefone.setText(p.getTelefone());
            taObs.setText(p.getObs());*/
            /////WritableImage im = Imagem.BufferedImageToImage(((Cliente) p).getImagem());
            /////imgImagemUsuario.setImage(im);
            /////imagem = ((Cliente) p).getImagem();
            TelaEnderecoController.buscaEndereco(CtrlPessoa.getEndereco(p), txbCep, lblErroCep, cbBairro, cbRua, cbCidade, cbEstado, cbPais);
        }
    }

    @FXML
    private void evtConsultaAvancada(MouseEvent event) {
        Parent root = null;
        try
        {
            /*stage.getIcons().add(new Image(getClass().getResourceAsStream("/IMG/logo_atlx.png")));*/
            TelaConsultaUsuarioController.setBtnPainel(true);
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/engenharia/ui/Consulta/TelaConsultaCliente.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("engenharia/ui/estilo/DarkTheme.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.initStyle(StageStyle.DECORATED);
            stage.showAndWait();
            setCampos(TelaConsultaClienteController.getCliente());
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela de Consulta");
        }
    }

    @FXML
    private void evtFichadoUsuario(MouseEvent event) {
        /*String sql = "select * from (select * from usuario where usr_login = '" + (String) (txbLogin.getText() != null ? txbLogin.getText() : txBusca.getText()) + "' ) as usuario inner join endereco on usuario.end_cep = endereco.end_cep inner join bairro on endereco.bai_cod = bairro.bai_cod inner join cidade on bairro.cid_cod = cidade.cid_cod inner join nivel on usuario.nivel_cod = nivel.nivel_cod";
        Relatorio.gerarRelatorio(sql, "Relatorios/MyReports/Rel_FichaUsuario.jasper", "Ficha do Usuario");*/
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1)
        {
            Object p = tabela.getItems().get(lin);

            setCampos(p);

            setButton(true, true, false, false, false);

            flag = 0;
            EOriginal = false;

        }
    }

    public void clear(ObservableList<Node> pn)
    {
        ObservableList<Node> componentes = pn; //”limpa” os componentes
        for (Node n : componentes)
        {
            if (n instanceof Pane)
                clear(((Pane) n).getChildren());
            if (n instanceof TextInputControl) // textfield, textarea e htmleditor

                ((TextInputControl) n).setText("");
            if (n instanceof ComboBox)
                ((ComboBox) n).getItems().clear();
            if (n instanceof ImageView)
                ((ImageView) n).setImage(null);
        }
    }

    public static Cliente getCliente() {
        return cliente;
    }

    @FXML
    private void evtMensagensDoCliente(ActionEvent event) {
        if(flag == 0){
            geraCliente();
            Parent root = null;
            try
            {
                /*stage.getIcons().add(new Image(getClass().getResourceAsStream("/IMG/logo_atlx.png")));*/
                TelaConsultaUsuarioController.setBtnPainel(true);
                Stage stage = new Stage();
                root = FXMLLoader.load(getClass().getResource("/engenharia/ui/Funcoes/TelaMensagemObserver.fxml"));
                Scene scene = new Scene(root);
                scene.getStylesheets().add("engenharia/ui/estilo/DarkTheme.css");
                stage.setScene(scene);
                stage.setMaximized(false);
                stage.initStyle(StageStyle.DECORATED);
                stage.showAndWait();
                setCampos(TelaConsultaClienteController.getCliente());
            } catch (IOException ex)
            {
                Mensagem.ExibirException(ex, "Erro ao Carregar Tela de Consulta");
            }
        }else{
            Mensagem.Exibir("Selecione um Cliente", 1);
        }
    }
    
    
}
