/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engenharia.ui.Cadastro;

import Utils.ControledeAcesso;
import Pessoa.Entidade.Nivel;
import Pessoa.Dao.CtrNivel;
import Utils.Mensagem;
import engenharia.ui.MainFXMLController;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadNivelController implements Initializable
{

    @FXML
    private TextField txbNome;
    @FXML
    private Label lbErroNome;
    @FXML
    private TextField txbPermissao;
    @FXML
    private Label lbErroPermissao;
    @FXML
    private ToggleButton tgbVisivel;
    @FXML
    private Button btnovo;
    @FXML
    private Button btalterar;
    @FXML
    private Button btapagar;
    @FXML
    private Button btconfirmar;
    @FXML
    private Button btcancelar;
    @FXML
    private TextField txBusca;
    @FXML
    private TableView<Nivel> tabela;
    @FXML
    private TableColumn<Nivel, String> tcNome;
    @FXML
    private TableColumn<Nivel, String> tcVisivel;
    @FXML
    private TableColumn<Nivel, String> tcPermissao;
    @FXML
    private TableColumn<Nivel, String> tcCodigo;
    private boolean EOriginal;
    @FXML
    private VBox pndados;
    private Nivel nivel;
    private int flag;
    private String codigo;
    @FXML
    private ComboBox<Object> cbCampos;
    @FXML
    private RadioButton rbAtivar;
    @FXML
    private RadioButton rbDesativar;
    private ArrayList<Object> permissao;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcNome.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcVisivel.setCellValueFactory(new PropertyValueFactory("Visivel"));
        tcPermissao.setCellValueFactory(new PropertyValueFactory("Permissao"));
        tcCodigo.setCellValueFactory(new PropertyValueFactory("Codigo"));
        permissao = new ArrayList();
        estadoOriginal();
        CarregaTabela("");
    }

    private void CarregaTabela(String filtro)
    {
        CtrNivel ctn = new CtrNivel();
        try
        {
            tabela.setItems(FXCollections.observableList(ctn.get(filtro)));
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    private void estadoOriginal()
    {
        EOriginal = true;
        tabela.setDisable(false);
        setAllErro(false, false);
        setButton(false, true, true, true, false);
        pndados.setDisable(true);

        clear(pndados.getChildren());

        cbCampos.getItems().clear();
        cbCampos.getItems().addAll(FXCollections.observableList(ControledeAcesso.getNom()));
        cbCampos.getSelectionModel().selectFirst();
        rbAtivar.selectedProperty().setValue(Boolean.FALSE);
        rbDesativar.selectedProperty().setValue(Boolean.TRUE);
        clearPermissao();

        CarregaTabela("");
    }

    public void EstadoEdicao()
    {
        EOriginal = false;
        setAllErro(false, false);
        setButton(true, false, true, true, false);
        tabela.setDisable(true);
        pndados.setDisable(false);
    }

    public void clear(ObservableList<Node> pn)
    {
        ObservableList<Node> componentes = pn; //”limpa” os componentes
        for (Node n : componentes)
        {
            if (n instanceof Pane)
                clear(((Pane) n).getChildren());
            if (n instanceof TextInputControl) // textfield, textarea e htmleditor

                ((TextInputControl) n).setText("");
            if (n instanceof ComboBox)
                ((ComboBox) n).getItems().clear();
            if (n instanceof ImageView)
                ((ImageView) n).setImage(null);
        }
    }

    private void setAllErro(boolean Enome, boolean EPermissao)
    {
        lbErroPermissao.setVisible(EPermissao);
        lbErroNome.setVisible(Enome);
    }

    private void setErro(Label lb, String Msg)
    {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar)
    {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
    }

    @FXML
    private void evtVisivelClicked(MouseEvent event)
    {
    }

    @FXML
    private void evtNovo(ActionEvent event)
    {
        EstadoEdicao();
        flag = 1;
    }

    @FXML
    private void evtAlterar(ActionEvent event)
    {
        EstadoEdicao();
        flag = 0;
    }

    private Nivel geraNivel()
    {
        CtrNivel cn = new CtrNivel();
        String Permissao = "";
        for (Object aux : permissao)
        {
            Permissao = Permissao + aux;
        }
        return nivel = new Nivel(codigo, txbNome.getText(), Permissao, (tgbVisivel.selectedProperty().get()) ? '1' : '0');
    }

    @FXML
    private void evtApagar(ActionEvent event)
    {
        CtrNivel cn = new CtrNivel();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir"))
            cn.Apagar(geraNivel());
        estadoOriginal();
        CarregaTabela("");
    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        setAllErro(false, false);
        CtrNivel cp = new CtrNivel();
        if (validaCampos())
            if (flag == 0)//alterar

                if (!cp.Alterar(geraNivel()))
                    Mensagem.Exibir(cp.getMsg(), 2);
                else
                {
                    Mensagem.Exibir(cp.getMsg(), 1);
                    estadoOriginal();
                }
            else//cadastrar
            if (!cp.Salvar(geraNivel()))
                Mensagem.Exibir(cp.getMsg(), 2);
            else
            {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginal();
            }
        else
            Mensagem.Exibir("Campos Inválidos!", 2);
        CarregaTabela("");
    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        if (EOriginal)
            MainFXMLController._pndados.getChildren().clear();
        else
            estadoOriginal();
    }

    @FXML
    private void evtBuscar(Event event)
    {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event)
    {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1)
        {
            Nivel p = tabela.getItems().get(lin);

            txbNome.setText(p.getNome());
            txbPermissao.setText(p.getPermissao());
            tgbVisivel.setSelected((p.getVisivel() == '1'));
            codigo = p.getCodigo();

            setButton(true, true, false, false, false);
            clearPermissao();
            for (int i = 0; i < permissao.size() && p.getPermissao().charAt(i) != ' '; i++)
            {
                permissao.set(i, p.getPermissao().charAt(i));
            }
            cbCampos.getSelectionModel().selectFirst();
            evtClickCampos(event);

            flag = 0;
            EOriginal = false;
        }
    }

    private boolean validaCampos()
    {
        boolean fl = true;

        if (txbNome.getText().trim().isEmpty())
        {
            setErro(lbErroNome, "Campo Obrigatório Vazio");
            txbNome.setText("");
            txbNome.requestFocus();
            fl = false;
        }/*
        if (txbPermissao.getText().trim().isEmpty()){
            setErro(lbErroPermissao, "Campo Obrigatório Vazio");
            txbPermissao.setText("");
            txbPermissao.requestFocus();
            fl = false;
        }*/

        return fl;
    }

    @FXML
    private void evtRbAtivar(MouseEvent event)
    {
        rbAtivar.selectedProperty().setValue(Boolean.TRUE);
        rbDesativar.selectedProperty().setValue(Boolean.FALSE);
        permissao.set(cbCampos.getSelectionModel().getSelectedIndex(), '1');
    }

    @FXML
    private void evtRbDesativar(MouseEvent event)
    {
        rbAtivar.selectedProperty().setValue(Boolean.FALSE);
        rbDesativar.selectedProperty().setValue(Boolean.TRUE);
        permissao.set(cbCampos.getSelectionModel().getSelectedIndex(), '0');
    }

    @FXML
    private void evtClickCampos(Event event)
    {
        if (!permissao.isEmpty() && (char) permissao.get(cbCampos.getSelectionModel().getSelectedIndex()) == '0')
        {
            rbAtivar.selectedProperty().setValue(Boolean.FALSE);
            rbDesativar.selectedProperty().setValue(Boolean.TRUE);
        } else
        {
            rbAtivar.selectedProperty().setValue(Boolean.TRUE);
            rbDesativar.selectedProperty().setValue(Boolean.FALSE);
        }
    }

    private void clearPermissao()
    {
        permissao.clear();
        for (int i = cbCampos.getItems().size(); i > 0; i--)
        {
            permissao.add('0');
        }
    }

}
