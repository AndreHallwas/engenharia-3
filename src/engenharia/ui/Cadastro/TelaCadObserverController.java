/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engenharia.ui.Cadastro;

import Produto.Controladora.CtrlProduto;
import Pessoa.Entidade.Cliente;
import Produto.Entidade.Produto;
import Interface.Consulta.TelaConsultaClienteController;
import Interface.Consulta.TelaConsultaUsuarioController;
import Utils.Mensagem;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadObserverController implements Initializable {

    @FXML
    private TextField txbProduto;
    @FXML
    private TextField txbCliente;
    private Produto produto;
    private Cliente cliente;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        produto = TelaCadProdutoController.getProduto();
        txbProduto.setText(produto.getNome());
    }    

    @FXML
    private void evtProduto(Event event) {
        Parent root = null;
        try
        {
            /*stage.getIcons().add(new Image(getClass().getResourceAsStream("/IMG/logo_atlx.png")));*/
            /*Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/engenharia/ui/Consulta/TelaConsultaProduto.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("engenharia/ui/estilo/DarkTheme.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.initStyle(StageStyle.DECORATED);
            stage.showAndWait();
            txbProduto.setText(TelaConsultaProdutoController.getProduto().getNome());
            produto = TelaConsultaProdutoController.getProduto();*/
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela de Consulta");
        }
    }

    @FXML
    private void evtCliente(Event event) {
        Parent root = null;
        try
        {
            /*stage.getIcons().add(new Image(getClass().getResourceAsStream("/IMG/logo_atlx.png")));*/
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/engenharia/ui/Consulta/TelaConsultaCliente.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("engenharia/ui/estilo/DarkTheme.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.initStyle(StageStyle.DECORATED);
            stage.showAndWait();
            txbCliente.setText(TelaConsultaClienteController.getCliente().getCPF());
            cliente = TelaConsultaClienteController.getCliente();
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela de Consulta");
        }
    }

    @FXML
    private void evtCadastrar(Event event) {
        CtrlProduto cp = CtrlProduto.create();
        if(cp.register(cliente, produto)){
            Mensagem.Exibir("Cadastro Efetuado", 1);
        }else{
            Mensagem.Exibir("Cadastro Não Efetuado", 3);
        }
    }

    @FXML
    private void evtCancelar(ActionEvent event) {
        Stage stage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
        stage.close();/////fexa janela
    }

    @FXML
    private void evtRemover(Event event) {
        CtrlProduto cp = CtrlProduto.create();
        if(cp.remove(cliente, produto)){
            Mensagem.Exibir("Exclusão Efetuada", 1);
        }else{
            Mensagem.Exibir("Exclusão Não Efetuada", 3);
        }
    }
    
}
