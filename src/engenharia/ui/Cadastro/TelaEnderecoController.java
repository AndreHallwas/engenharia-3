/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Gerenciamento;

import Endereco.Entidade.Bairro;
import Endereco.Entidade.Cidade;
import Endereco.Entidade.Endereco;
import Endereco.Entidade.Estado;
import Endereco.Dal.CtrEndereco;
import Pessoa.Dao.CtrPessoa;
import Utils.Mensagem;
import java.util.List;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Raizen
 */
public class TelaEnderecoController
{

    public static boolean valida(TextField txbCep, Label lblErroCep) //Validações de banco
    {
        boolean fl = true;
        CtrPessoa cp = new CtrPessoa();

        if (txbCep.getText().length() == 8)
        {
            CtrEndereco ce = new CtrEndereco();
            if (ce.get(txbCep.getText()).isEmpty())
            {
                setErro(lblErroCep, "Cep Não Existe");
                txbCep.setText("");
                txbCep.requestFocus();
                fl = false;
            }
        } else
        {
            setErro(lblErroCep, "Cep Inválido");
            txbCep.setText("");
            txbCep.requestFocus();
            fl = false;
        }

        return fl;
    }

    private static void setErro(Label lb, String Msg)
    {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public static void ConsultaCep(String filtro, TextField txbCep, Label lblErroCep,
            ComboBox cbBairro, ComboBox cbRua, ComboBox cbCidade, ComboBox cbEstado, ComboBox cbPais)
    {
        buscaEndereco(filtro, txbCep, lblErroCep, cbBairro, cbRua, cbCidade, cbEstado, cbPais);
    }

    public static void buscaEndereco(String filtro, TextField txbCep, Label lblErroCep,
            ComboBox cbBairro, ComboBox cbRua, ComboBox cbCidade, ComboBox cbEstado, ComboBox cbPais)
    {
        if (filtro.length() == 8)
        {
            CtrEndereco ce = new CtrEndereco();
            List endereco = ce.get(filtro);
            if (!endereco.isEmpty())
            {
                cbBairro.getItems().clear();
                cbRua.getItems().clear();
                cbBairro.getItems().clear();
                cbCidade.getItems().clear();
                cbEstado.getItems().clear();
                cbPais.getItems().clear();
                Endereco en = (Endereco) endereco.get(0);
                txbCep.setText(en.getCEP());
                cbRua.getItems().add(en);
                cbRua.getSelectionModel().select(en);
                cbBairro.getItems().add(en.getBairro());
                cbBairro.getSelectionModel().select(en.getBairro());
                cbCidade.getItems().add(en.getBairro().getCidade());
                cbCidade.getSelectionModel().select(en.getBairro().getCidade());
                cbEstado.getItems().add(en.getBairro().getCidade().getEstado());
                cbEstado.getSelectionModel().select(en.getBairro().getCidade().getEstado());
                cbPais.getItems().add(en.getBairro().getCidade().getEstado().getPais());
                cbPais.getSelectionModel().select(en.getBairro().getCidade().getEstado().getPais());
            } else
            {
                setErro(lblErroCep, "Cep Não Encontrado");
                txbCep.setText("");
                txbCep.requestFocus();
            }
        } else
        {
            setErro(lblErroCep, "Cep Inválido");
            txbCep.setText("");
            txbCep.requestFocus();
        }
    }

    private void defaultEndereco()
    {
        /* CtrEndereco ce = new CtrEndereco();
        cbBairro.getItems().clear();
        cbRua.getItems().clear();
        cbBairro.getItems().clear();
        cbCidade.getItems().clear();
        cbEstado.getItems().clear();
        cbPais.getItems().clear();
        List estado = ce.getEstado("");
        Estado es = (Estado) estado.get(0);
        cbEstado.getItems().addAll(estado);/*
        List cidade = ce.getCidade(es.getCodigo());
        Cidade ci = (Cidade) cidade.get(0);
        cbCidade.getItems().addAll(cidade);
        List bairro = ce.getBairro(ci.getCodigo());
        Bairro bi = (Bairro) bairro.get(0);
        cbBairro.getItems().addAll(bairro);
        List endereco = ce.getEndereco(bi.getCodigo());
        Endereco en = (Endereco) endereco.get(0);
        cbRua.getItems().addAll(endereco);*/
 /*cbPais.getItems().addAll((List)ce.getPais(""));*/
    }

    public static void SelecionaEstado(ComboBox cbCidade, ComboBox<Estado> cbEstado)
    {
        CtrEndereco ce = new CtrEndereco();
        cbCidade.getItems().clear();
        List cidade = ce.getCidade(cbEstado.getSelectionModel().getSelectedItem().getCodigo());
        Cidade ci = (Cidade) cidade.get(0);
        cbCidade.getItems().addAll(cidade);
    }

    public static void SelecionaCidade(ComboBox cbBairro, ComboBox<Cidade> cbCidade)
    {
        CtrEndereco ce = new CtrEndereco();
        cbBairro.getItems().clear();
        List bairro = ce.getBairro(cbCidade.getSelectionModel().getSelectedItem().getCodigo());
        Bairro bi = (Bairro) bairro.get(0);
        cbBairro.getItems().addAll(bairro);
    }

    public static void SelecionaBairro(ComboBox cbRua, ComboBox<Bairro> cbBairro)
    {
        CtrEndereco ce = new CtrEndereco();
        cbRua.getItems().clear();
        List endereco = ce.get(cbBairro.getSelectionModel().getSelectedItem().getCodigo());
        Endereco en = (Endereco) endereco.get(0);
        cbRua.getItems().addAll(endereco);
    }

    public static void SelecionaRua(TextField txbCep, ComboBox<Endereco> cbRua)
    {
        txbCep.setText(cbRua.getSelectionModel().getSelectedItem().getCEP());
    }

}
