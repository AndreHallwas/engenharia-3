/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engenharia.ui.Funcional;

import Pessoa.Controladora.CtrlPessoa;
import Utils.Mensagem;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import engenharia.ui.Cadastro.TelaCadClienteController;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaMensagemObserverController implements Initializable {

    @FXML
    private TextArea taMensagem;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try{
            CtrlPessoa cp = CtrlPessoa.create();
            String Texto = "";
            ArrayList<String> elems = cp.getMessages(TelaCadClienteController.getCliente());
            for(String elem : elems){
                Texto += elem+"\n\n";
            }
            taMensagem.setText(Texto);
        }catch(Exception ex){
            Mensagem.ExibirException(ex, "Erro na Mensagem");
        }
    }    

    @FXML
    private void evtConfirmar(ActionEvent event) {
        Stage stage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
        stage.close();/////fexa janela
    }
    
}
