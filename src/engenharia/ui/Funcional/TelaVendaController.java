/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engenharia.ui.Funcional;

import Transacao.Controladora.CtrlVenda;
import Transacao.Entidade.ProdutoDaVenda;
import Produto.Controladora.CtrlLote;
import Pessoa.Controladora.CtrlPessoa;
import Produto.Controladora.CtrlProduto;
import Pessoa.Entidade.Cliente;
import Produto.Entidade.Lote;
import Produto.Entidade.Produto;
import Pessoa.Entidade.Usuario;
import Interface.Consulta.TelaConsultaClienteController;
import Interface.Funcional.TelaVenda;
import Interface.Gerenciamento.TelaConsultaProdutoController;
import Utils.Carrinho;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import engenharia.ui.Consulta.TelaConsultaLoteController;
import static engenharia.ui.Consulta.TelaConsultaLoteController.lote;
import engenharia.ui.MainFXMLController;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.swing.JOptionPane;


/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaVendaController implements Initializable {

    @FXML
    private HBox pndados;
    @FXML
    private TextField txbUsuario;
    @FXML
    private TextField txbCliente;
    @FXML
    private TextField txbCPF;
    @FXML
    private TextField txbProduto;
    @FXML
    private TextField txbPreco;
    @FXML
    private VBox pndados2;
    @FXML
    private TextField txbQuantidade;
    @FXML
    private TextField txbValidade;
    @FXML
    private TextArea txbObs;
    @FXML
    private TextField txbValorTotal;
    @FXML
    private TextField txbParcelas;
    @FXML
    private Button btnAdicionarCarrinho;
    @FXML
    private Button btnAlterarCarrinho;
    @FXML
    private Button btnExcluirCarrinho;
    @FXML
    private TableView<TelaVenda> tbLote;
    @FXML
    private TableColumn<TelaVenda, String> tcLote;
    @FXML
    private TableColumn<TelaVenda, String> tcProduto;
    @FXML
    private TableColumn<TelaVenda, String> tcQuantidade;
    @FXML
    private Button btnConfirma;
    @FXML
    private Button btnCancelar;
    @FXML
    private Button btnVenda;
    @FXML
    private TextField txbLote;
    private TelaVenda tv;
    private Usuario usuario;
    private int flag;
    private static Object produto;
    private Carrinho carrinho;
    private Cliente cliente;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        tcLote.setCellValueFactory(new PropertyValueFactory("CodigoLote"));
        tcProduto.setCellValueFactory(new PropertyValueFactory("NomeProduto"));
        tcQuantidade.setCellValueFactory(new PropertyValueFactory("Estoque"));

        MaskFieldUtil.monetaryField(txbPreco);
        MaskFieldUtil.monetaryField(txbValorTotal);
        MaskFieldUtil.numericField(txbParcelas);
        flag = 1;
        txbParcelas.setText("1");

        carrinho = new Carrinho();
        estadoOriginal();
        CarregaTabela("");
    }    
    
    private void CarregaTabela(String filtro)
    {
        CarregaTabelaLotes();
    }

    private void CarregaTabelaLotes()
    {
        CtrlLote ctm = CtrlLote.create();
        try
        {
            tbLote.setItems(FXCollections.observableList(carrinho.geraLotes()));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Erro Ao CarreGar Lotes");
        }
    }
    
    @FXML
    private void evtConsultaCliente(MouseEvent event) {
        Parent root = null;
        try
        {
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/engenharia/ui/Consulta/TelaConsultaCliente.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("farmacia/ui/estilo/tema1.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.showAndWait();
            cliente = TelaConsultaClienteController.getCliente();
            if (cliente != null){
                txbCliente.setText(cliente.getNome());
                txbCPF.setText(cliente.getCPF());
            }
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao consultar fornecedor na compra");
        }
    }

    @FXML
    private void evtConsultaProduto(MouseEvent event) {
        Parent root = null;
        try
        {
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/engenharia/ui/Consulta/TelaConsultaProduto.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("engenharia/ui/estilo/tema1.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.showAndWait();
            produto = TelaConsultaProdutoController.getProduto();
            if (produto != null)
            {
                CtrlProduto.setCampos(produto, txbProduto, txbPreco, txbObs);
            }

        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao consultar produto na compra");
        }
    }

    @FXML
    private void evtAdicionarCarrinho(ActionEvent event) {
        EstadoEdicao();
        flag = 1;
        CarregaTabela("");
    }

    @FXML
    private void evtAlterarCarrinho(ActionEvent event) {
        flag = 0;
        EstadoEdicao();
    }

    @FXML
    private void evtExcluirCarrinho(ActionEvent event) {
        carrinho.remove(tv.getLote());
        CarregaTabela("");
        estadoOriginal();
    }
    

    public void setFornecedor()
    {   /*
            if (fornecedor != null)
            {
                txbFornecedor.setText(fornecedor.getNome());
                txbFornecedor.setDisable(true);
            } else
                txbFornecedor.setDisable(false);
        */
    }

    private void estadoOriginal()
    {

        pndados.setDisable(true);

        setButtomCarrinho(true, true, false);
        setButtom(false, false, true);

        txbValorTotal.setText(Double.toString(carrinho.getValorTotal()));

        clear(pndados.getChildren());

        if (!carrinho.isEmpty())
            btnVenda.setDisable(true);
        else
            btnVenda.setDisable(false);

        txbUsuario.setText(MainFXMLController._txUsuarioInterface.getText());
        txbParcelas.setText(txbParcelas.getText().isEmpty() ? "1" : txbParcelas.getText());

        txbValorTotal.setText(Float.toString(carrinho.getValorTotal()));
        CarregaTabela("");
    }

    public void clear(ObservableList<Node> pn)
    {
        ObservableList<Node> componentes = pn; //”limpa” os componentes
        for (Node n : componentes)
        {
            if (n instanceof Pane)
                clear(((Pane) n).getChildren());
            if (n instanceof TextInputControl) // textfield, textarea e htmleditor
                ((TextInputControl) n).setText("");
            if (n instanceof ComboBox)
                ((ComboBox) n).getItems().clear();
            if (n instanceof ImageView)
                ((ImageView) n).setImage(null);
        }
    }

    public void EstadoEdicao()
    {
        pndados.setDisable(false);
        setButtom(false, true, false);
        setButtomCarrinho(true, false, true);
        /////setFornecedor();
    }

    @FXML
    private void ClicknaTabelaLotes(MouseEvent event) {
        int lin = tbLote.getSelectionModel().getSelectedIndex();
        if (lin > -1)
        {
            tv = tbLote.getItems().get(lin);
            setCampos(tv.getLote(),tv.getProduto(),tv.getEstoque());

            setButtomCarrinho(false, false, true);
            setButtom(false, false, false);

            flag = 0;
        }
    }

    @FXML
    private void evtConfirmar(ActionEvent event) {
        CtrlPessoa cp = CtrlPessoa.create();
        ArrayList<Object> pessoas = cp.Pesquisar(txbUsuario.getText(), 1);
        if (!pessoas.isEmpty())
        {
            usuario = (Usuario) pessoas.get(0);
            pessoas = cp.Pesquisar(txbCPF.getText(), 2);
            
            if (!pessoas.isEmpty())
            {
                cliente = (Cliente) pessoas.get(0);
                CtrlLote cl = CtrlLote.create();
                ArrayList<Object> lote = cl.Pesquisar(txbLote.getText(), 6);
                
                if(!lote.isEmpty()){
                    
                    if (carrinho == null)
                        carrinho = new Carrinho();
                    if (Integer.parseInt(txbQuantidade.getText()) > 0)
                        if(flag == 1){
                            if(!carrinho.add(new ProdutoDaVenda(null, (Lote) lote.get(0), produto, txbQuantidade.getText()),null)){
                                Mensagem.Exibir("Quantidade Inválida", 2);
                            }
                        }
                        else
                            carrinho.set(new ProdutoDaVenda(null, (Lote) lote.get(0), produto, txbQuantidade.getText()),null);
                    estadoOriginal();
                }
            }
        }
    }


    @FXML
    private void evtCancelar(ActionEvent event) {
        estadoOriginal();
    }
    
    private void setCampos(Lote l,Produto p, String Quantidade)
    {
        if (l != null)
        {
            txbQuantidade.setText(Quantidade);
            txbProduto.setText(p.getNome());
            txbPreco.setText(Double.toString(p.getPrecoCom()));
            txbObs.setText(p.getObs());
            txbValorTotal.setText(Float.toString(carrinho.getValorTotal()));
        }
    }
    
     public void setButtomCarrinho(boolean alterarC, boolean excluirC, boolean adicionarC)
    {
        btnAlterarCarrinho.setDisable(alterarC);
        btnExcluirCarrinho.setDisable(excluirC);
        btnAdicionarCarrinho.setDisable(adicionarC);
    }

    public void setButtom(boolean Bcancelar, boolean BVenda, boolean Bconfirma)
    {
        btnCancelar.setDisable(Bcancelar);
        btnVenda.setDisable(BVenda);
        btnConfirma.setDisable(Bconfirma);
    }

    @FXML
    private void evtVenda(ActionEvent event) {
        CtrlVenda cc = CtrlVenda.create();
        if(cc.registar(MainFXMLController._txUsuarioInterface.getText(),cliente.getCPF(),txbParcelas.getText(),carrinho)){
            carrinho.clear();
            txbParcelas.setText("1");
            estadoOriginal();
        }
        CarregaTabela("");
    }

    @FXML
    private void evtConsultaLote(MouseEvent event) {
        
        Parent root = null;
        try
        {
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/engenharia/ui/Consulta/TelaConsultaLote.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("engenharia/ui/estilo/tema1.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.showAndWait();
            lote = TelaConsultaLoteController.lote;  
            if(lote != null){
                CtrlLote.setLote(lote , txbLote, txbValidade);
            }
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao cancelar compra");
        }
    }

    @FXML
    private void evtCancelarCompra(ActionEvent event) {
        
        Parent root = null;
        try
        {
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/engenharia/ui/Consulta/TelaConsultaVenda.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("engenharia/ui/estilo/tema1.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.showAndWait();
            /*compra = TelaConsultaCompraController.compra;  
            if(compra != null){
                CtrlCompra cc = new CtrlCompra();
                cc.cancelarCompra(compra.getCodigo());                
            }*/
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao cancelar compra");
        }
    }

    public static Object getProduto() {
        return produto;
    }
    
}
