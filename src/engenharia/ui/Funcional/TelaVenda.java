/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Funcional;

import Produto.Entidade.Lote;
import Produto.Entidade.Produto;

/**
 *
 * @author Raizen
 */

public class TelaVenda{
        private Lote lote;
        private Produto produto;
        private String Estoque;
        private String CodigoLote;
        private String NomeProduto;

        public TelaVenda(Lote lote, Produto produto, String Estoque) {
            this.lote = lote;
            this.produto = produto;
            this.Estoque = Estoque;
            this.CodigoLote = lote.getCodigo();
            this.NomeProduto = produto.getNome();
        }

        public Lote getLote() {
            return lote;
        }

        public Produto getProduto() {
            return produto;
        }

        public String getEstoque() {
            return Estoque;
        }

        public String getCodigoLote() {
            return CodigoLote;
        }

        public String getNomeProduto() {
            return NomeProduto;
        } 
    }
