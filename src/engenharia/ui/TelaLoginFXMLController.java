/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engenharia.ui;

import Utils.ControledeAcesso;
import Empresa.Dao.CtrEmpresa;
import Utils.Imagem;
import Utils.Mensagem;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaLoginFXMLController implements Initializable
{

    @FXML
    private TextField txbSenha;
    @FXML
    private TextField txbLogin;
    @FXML
    private ImageView imgEmpresa;
    @FXML
    private Label lblErroLogin;
    @FXML
    private Label lblErroSenha;
    private static boolean Retorno;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        // TODO_
        /////CtrlAcesso.Default();
        CtrEmpresa ce = new CtrEmpresa();
        imgEmpresa.setImage(Imagem.BufferedImageToImage(ce.get().getImagem()));
        Retorno = false;
    }

    private void setErro(Label lb, String Msg)
    {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        if (txbLogin.getText().trim().isEmpty())
        {
            setErro(lblErroLogin, "Campo Obrigatório Vazio");
            txbLogin.setText("");
            txbLogin.requestFocus();
            fl = false;
        }
        if (txbSenha.getText().trim().isEmpty())
        {
            setErro(lblErroSenha, "Campo Obrigatório Vazio");
            txbSenha.setText("");
            txbSenha.requestFocus();
            fl = false;
        }
        return fl;
    }

    @FXML
    private void evtLogar(ActionEvent event)
    {
        if (validaCampos())
            if (ControledeAcesso.logar(txbSenha.getText(), txbLogin.getText()))
            {
                evtCancelar(event);
                Retorno = true;
            } else
            {
                txbLogin.requestFocus();
                setErro(lblErroLogin, "Login ou Senha Inválido");
            }
    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        /////MainFXMLController._pndados.getChildren().clear();
        MainFXMLController._login.close();
    }

    public static boolean isRetorno()
    {
        return Retorno;
    }

}
