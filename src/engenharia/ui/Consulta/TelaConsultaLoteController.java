/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engenharia.ui.Consulta;


import Produto.Controladora.CtrlLote;
import Produto.Controladora.CtrlProduto;
import Produto.Entidade.Lote;
import Utils.Mensagem;
import engenharia.ui.Funcional.TelaVendaController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaLoteController implements Initializable
{

    public static Object lote;
    @FXML
    private TextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcCodigo;
    private TableColumn<Object, String> tcProduto;
    @FXML
    private TableColumn<Object, String> tcFornecedor;
    @FXML
    private TableColumn<Object, String> tcEstoqueInicial;
    @FXML
    private TableColumn<Object, String> tcEstoque;
    @FXML
    private TableColumn<Object, String> tcValidade;
    @FXML
    private FlowPane pnBusca;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcCodigo.setCellValueFactory(new PropertyValueFactory("Codigo"));
        tcEstoque.setCellValueFactory(new PropertyValueFactory("Estoque"));
        tcEstoqueInicial.setCellValueFactory(new PropertyValueFactory("EstoqueInicial"));
        tcValidade.setCellValueFactory(new PropertyValueFactory("Validade"));
        if(TelaVendaController.getProduto() != null){
            CarregaTabela(CtrlProduto.getCodigo(TelaVendaController.getProduto()));
            pnBusca.setDisable(true);
        }else{
            CarregaTabela("");
            pnBusca.setDisable(false);
        }
        lote = null;
    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        if (lote != null)
            evtCancelar(event);
    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        Stage stage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
        stage.close();/////fexa janela
    }

    @FXML
    private void evtBuscar()
    {
        CarregaTabela(txBusca.getText());
    }

    private void CarregaTabela(String filtro)
    {
        CtrlLote ctm = CtrlLote.create();
        try
        {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro, 5)));
        }
        catch(Exception ex)
        {
            Mensagem.ExibirException(ex, "Erro na Consulta de Lote!!!!!");
        }
    }

    @FXML
    private void ClicknaTabela(MouseEvent event)
    {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        lote = (Lote) tabela.getItems().get(lin);
    }

    public static Object getLote() {
        return lote;
    }

}
