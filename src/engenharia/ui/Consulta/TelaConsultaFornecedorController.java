/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Consulta;

import Fornecedor.Entidade.Fornecedor;
import Fornecedor.Dao.CtrFornecedor;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Drake
 */
public class TelaConsultaFornecedorController implements Initializable
{

    @FXML
    private TextField txBusca;
    @FXML
    private TableView<Fornecedor> tabela;
    @FXML
    private TableColumn<Fornecedor, String> tcNome;
    @FXML
    private TableColumn<Fornecedor, String> tcCNPJ;
    @FXML
    private TableColumn<Fornecedor, String> tcTelefone;
    @FXML
    private TableColumn<Fornecedor, String> tcEndereco;
    private static Fornecedor fornecedor;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcNome.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcEndereco.setCellValueFactory(new PropertyValueFactory("Endereco"));
        tcCNPJ.setCellValueFactory(new PropertyValueFactory("CNPJ"));
        tcTelefone.setCellValueFactory(new PropertyValueFactory("Telefone"));
        CarregaTabela("");
        fornecedor = null;
    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        if (fornecedor != null)
            evtCancelar(event);
    }

    private void CarregaTabela(String filtro)
    {
        /* CtrFornecedor ctm = new CtrFornecedor();
        try
        {
            tabela.setItems(FXCollections.observableList(ctm.getFornecedor(filtro, 1)));
        }
        catch(Exception ex)
        {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }*/
    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        Stage stage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
        stage.close();/////fexa janela
    }

    @FXML
    private void evtBuscar()
    {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event)
    {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1)
            fornecedor = tabela.getItems().get(lin);
    }

    public static Fornecedor getFornecedor()
    {
        return fornecedor;
    }

}
