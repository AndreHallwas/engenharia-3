/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Consulta;

import Pessoa.Entidade.Pessoa;
import Pessoa.Entidade.Usuario;
import Pessoa.Dao.CtrPessoa;
import Utils.Mensagem;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaUsuarioController implements Initializable
{

    @FXML
    private TextField txBusca;
    @FXML
    private TableView<Pessoa> tabela;
    @FXML
    private TableColumn<Usuario, String> tcNome;
    @FXML
    private TableColumn<Usuario, String> tcRG;
    @FXML
    private TableColumn<Usuario, String> tcCPF;
    @FXML
    private TableColumn<Usuario, String> tcTelefone;
    @FXML
    private TableColumn<Usuario, String> tcEndereco;
    @FXML
    private TableColumn<Usuario, String> tcEmail;
    @FXML
    private TableColumn<Usuario, Double> tcSalario;
    @FXML
    private TableColumn<Usuario, String> tcNivel;
    @FXML
    private TableColumn<Usuario, String> tcObs;
    @FXML
    private TableColumn<Usuario, String> tcLogin;
    private static Usuario usuario;
    private static boolean btnPainel = true;
    @FXML
    private HBox btnGp;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcNome.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcEmail.setCellValueFactory(new PropertyValueFactory("Email"));
        tcEndereco.setCellValueFactory(new PropertyValueFactory("Endereco"));
        tcNivel.setCellValueFactory(new PropertyValueFactory("Nivel"));
        tcCPF.setCellValueFactory(new PropertyValueFactory("CPF"));
        tcObs.setCellValueFactory(new PropertyValueFactory("Obs"));
        tcRG.setCellValueFactory(new PropertyValueFactory("RG"));
        tcSalario.setCellValueFactory(new PropertyValueFactory("Salario"));
        tcTelefone.setCellValueFactory(new PropertyValueFactory("Telefone"));
        tcLogin.setCellValueFactory(new PropertyValueFactory("Login"));
        btnGp.setVisible(btnPainel);
    }

    private void CarregaTabela(String filtro)
    {
        CtrPessoa ctm = new CtrPessoa();
        try
        {
            tabela.setItems(FXCollections.observableList(ctm.get(filtro, 1, 1)));
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtBuscar(Event event)
    {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event)
    {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1)
            usuario = (Usuario) tabela.getItems().get(lin);
    }

    @FXML
    private void evtConfirmar(Event event)
    {
        if (usuario != null)
            evtCancelar(event);
        else
            Mensagem.Exibir("Nenhum Usuario Selecionado", 2);
    }

    @FXML
    private void evtCancelar(Event event)
    {
        Stage stage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
        stage.close();/////fexa janela
    }

    public static void setBtnPainel(boolean btnPainel)
    {
        TelaConsultaUsuarioController.btnPainel = btnPainel;
    }

    public static Usuario getUsuario()
    {
        return usuario;
    }

}
