/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Consulta;

import Pessoa.Entidade.Cliente;
import Pessoa.Entidade.Pessoa;
import Pessoa.Dao.CtrPessoa;
import Utils.Mensagem;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaClienteController implements Initializable
{

    private static Cliente cliente;
    @FXML
    private TextField txBusca;
    @FXML
    private TableView<Pessoa> tabela;
    @FXML
    private TableColumn<Cliente, String> tcNome;
    @FXML
    private TableColumn<Cliente, String> tcRG;
    @FXML
    private TableColumn<Cliente, String> tcCPF;
    @FXML
    private TableColumn<Cliente, String> tcTelefone;
    @FXML
    private TableColumn<Cliente, String> tcEndereco;
    @FXML
    private TableColumn<Cliente, String> tcEmail;
    @FXML
    private TableColumn<Cliente, String> tcObs;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcNome.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcEmail.setCellValueFactory(new PropertyValueFactory("Email"));
        tcEndereco.setCellValueFactory(new PropertyValueFactory("Endereco"));
        tcCPF.setCellValueFactory(new PropertyValueFactory("CPF"));
        tcObs.setCellValueFactory(new PropertyValueFactory("Obs"));
        tcRG.setCellValueFactory(new PropertyValueFactory("RG"));
        tcTelefone.setCellValueFactory(new PropertyValueFactory("Telefone"));
        CarregaTabela("");
        cliente = null;
    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        if (cliente != null)
            evtCancelar(event);
    }

    private void CarregaTabela(String filtro)
    {
        CtrPessoa ctm = new CtrPessoa();
        try
        {
            tabela.setItems(FXCollections.observableList(ctm.get(filtro, 0, 2)));
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex);
        }
    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        Stage stage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
        stage.close();/////fexa janela
    }

    @FXML
    private void evtBuscar()
    {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event)
    {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        cliente = (Cliente) tabela.getItems().get(lin);
    }

    public static Cliente getCliente() {
        return cliente;
    }
    
    

}
