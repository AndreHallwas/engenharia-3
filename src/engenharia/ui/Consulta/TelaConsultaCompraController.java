/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Consulta;

import Transacao.Entidade.Compra;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Drake
 */
public class TelaConsultaCompraController implements Initializable
{

    @FXML
    private TextField txBusca;
    @FXML
    private TableView<Compra> tabela;
    @FXML
    private TableColumn<Compra, String> tcCodigo;
    @FXML
    private TableColumn<Compra, String> tcFornecedor;
    @FXML
    private TableColumn<Compra, String> tcValor;
    @FXML
    private TableColumn<Compra, String> tcQuantidade;
    public static Compra compra;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcCodigo.setCellValueFactory(new PropertyValueFactory("Codigo"));
        tcFornecedor.setCellValueFactory(new PropertyValueFactory("For"));
        tcQuantidade.setCellValueFactory(new PropertyValueFactory("compra_quantLotes"));
        tcValor.setCellValueFactory(new PropertyValueFactory("compra_valor"));
        CarregaTabela("");
        compra = null;
    }

    @FXML
    private void evtBuscar()
    {
        CarregaTabela(txBusca.getText());
    }

    private void CarregaTabela(String filtro)
    {
        /*CtrCompra ctm = new CtrCompra();
        try
        {
            tabela.setItems(ctm.getCompra(filtro, 1));
        }
        catch(Exception ex)
        {
            
        }*/
    }

    @FXML
    private void ClicknaTabela(MouseEvent event)
    {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        compra = tabela.getItems().get(lin);

    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        if (compra != null)
            evtCancelar(event);

    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        Stage stage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
        stage.close();/////fexa janela
    }

}
