/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Relatorio;

import Utils.Mensagem;
import Utils.Relatorio;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Luish
 */
public class TelaRelatFichaUsuarioController implements Initializable
{

    @FXML
    private TextField txbusca;
    @FXML
    private ScrollPane spdado;
    @FXML
    private HBox dado;
    private SwingNode sn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        Default();
    }

    private void Default()
    {
        try
        {
            sn = Relatorio.gerarRelatorio("select * from usuario "
                    + "inner join endereco on usuario.end_cep = endereco.end_cep "
                    + "inner join bairro on endereco.bai_cod = bairro.bai_cod "
                    + "inner join cidade on bairro.cid_cod = cidade.cid_cod "
                    + "inner join nivel on usuario.nivel_cod = nivel.nivel_cod", "Relatorios/MyReports/Rel_FichaUsuario.jasper");
            dado.getChildren().clear();
            dado.getChildren().add(sn);
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Nenhum Registro No Banco");
        }
    }

    @FXML
    private void evtBucscar(ActionEvent event)
    {
        try
        {
            if (!txbusca.getText().isEmpty())
            {
                sn = Relatorio.gerarRelatorio("select * from (select * from usuario where upper(usr_login) like upper('" + txbusca.getText() + "%') ) as usuario "
                        + "inner join endereco on usuario.end_cep = endereco.end_cep "
                        + "inner join bairro on endereco.bai_cod = bairro.bai_cod "
                        + "inner join cidade on bairro.cid_cod = cidade.cid_cod "
                        + "inner join nivel on usuario.nivel_cod = nivel.nivel_cod", "Relatorios/MyReports/Rel_FichaUsuario.jasper");
                dado.getChildren().clear();
                dado.getChildren().add(sn);
            } else
                Default();
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Não Há Dados Eminentes Nesta Consulta");
            txbusca.setText("");
            Default();
        }
    }

    @FXML
    private void btnTelaCheia(MouseEvent event)
    {
        try
        {
            if (!txbusca.getText().isEmpty())
                Relatorio.gerarRelatorio("select * from (select * from usuario where upper(usr_login) "
                        + "like upper('" + txbusca.getText() + "%') ) as usuario inner join endereco "
                        + "on usuario.end_cep = endereco.end_cep inner join bairro "
                        + "on endereco.bai_cod = bairro.bai_cod inner join cidade "
                        + "on bairro.cid_cod = cidade.cid_cod inner join nivel on usuario.nivel_cod = nivel.nivel_cod",
                        "Relatorios/MyReports/Rel_FichaUsuario.jasper", "Ficha do Usuario");
            else
                Relatorio.gerarRelatorio("select * from usuario "
                        + "inner join endereco on usuario.end_cep = endereco.end_cep "
                        + "inner join bairro on endereco.bai_cod = bairro.bai_cod "
                        + "inner join cidade on bairro.cid_cod = cidade.cid_cod "
                        + "inner join nivel on usuario.nivel_cod = nivel.nivel_cod", "Relatorios/MyReports/Rel_FichaUsuario.jasper", "Ficha do Usuario");
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Não Há Dados Eminentes Nesta Consulta");
            txbusca.setText("");
            Default();
        }
    }

}
