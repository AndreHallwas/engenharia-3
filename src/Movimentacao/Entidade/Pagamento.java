package Movimentacao.Entidade;

import Fornecedor.Entidade.Fornecedor;
import Movimentacao.Dal.CtrPagamento;
import Movimentacao.Dal.CtrRecebimento;
import Pessoa.Entidade.Usuario;
import Transacao.Entidade.Compra;

public class Pagamento implements Movimentacao {

    private String Codigo;
    private Compra compra;
    private Fornecedor fornecedor;
    private Usuario usuario;
    private double pag_valor;
    private String Data;

    @Override
    public boolean RealizarMovimentacao() {
        CtrPagamento cp = new CtrPagamento();
        return cp.Salvar(this);
    }
    
    public void geraValorParcela(double ValorTotal, int Parcelas){
        setPag_valor(ValorTotal/Parcelas);
    }

    public Pagamento(double pag_valor, Compra compra) {
        this.pag_valor = pag_valor;
        this.compra = compra;
    }

    public Pagamento(String Codigo, Compra compra, Fornecedor fornecedor, Usuario usuario, double pag_valor, String Data) {
        this.Codigo = Codigo;
        this.compra = compra;
        this.fornecedor = fornecedor;
        this.usuario = usuario;
        this.pag_valor = pag_valor;
        this.Data = Data;
    }

    public Pagamento(Compra compra, Fornecedor fornecedor, Usuario usuario, double pag_valor, String Data) {
        this.compra = compra;
        this.fornecedor = fornecedor;
        this.usuario = usuario;
        this.pag_valor = pag_valor;
        this.Data = Data;
    }

    /**
     * @return the Codigo
     */
    public String getCodigo() {
        return Codigo;
    }

    /**
     * @param Codigo the Codigo to set
     */
    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    /**
     * @return the compra
     */
    public Compra getCompra() {
        return compra;
    }

    /**
     * @param compra the compra to set
     */
    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    /**
     * @return the fornecedor
     */
    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    /**
     * @param fornecedor the fornecedor to set
     */
    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the pag_valor
     */
    public double getPag_valor() {
        return pag_valor;
    }

    /**
     * @param pag_valor the pag_valor to set
     */
    public void setPag_valor(double pag_valor) {
        this.pag_valor = pag_valor;
    }

    /**
     * @return the Data
     */
    public String getData() {
        return Data;
    }

    /**
     * @param Data the Data to set
     */
    public void setData(String Data) {
        this.Data = Data;
    }
    
}
