package Fornecedor.Dao;

import Fornecedor.Entidade.Fornecedor;
import Fornecedor.Entidade.Fornecedor;
import Pessoa.Entidade.Usuario;
import Padroes.Dao.ControledeEntidade;
import Utils.Banco;
import Utils.Imagem;
import Utils.Mensagem;
import java.io.ByteArrayInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CtrFornecedor extends ControledeEntidade
{
    
    public CtrFornecedor()
    {
        flag = false;
        pstmt = null;
    }

    public boolean Salvar(Fornecedor n)
    {
        pstmt = Banco.con.geraStatement(
                "insert into Fornecedor(for_cnpj, end_cep, for_nome, for_telefone, for_email, for_obs, for_foto) values(?,?,?,?,?,?,?)");

        setParametros(n);
        setMsg(Banco.con.manipular(pstmt), "Cadastro");

        return flag;
    }

    public boolean Alterar(Fornecedor n)
    {
        pstmt = Banco.con.geraStatement("update Fornecedor set for_cnpj = ?,"
                + " end_cep = ?,for_nome = ?,for_telefone = ?, for_email = ?, for_obs = ?, for_foto = ?  where for_cnpj = '" + n.getCNPJ() + "'");

        setParametros(n);
        setMsg(Banco.con.manipular(pstmt), "Alteração");

        return flag;
    }

    public boolean Apagar(Fornecedor n)
    {
        pstmt = Banco.con.geraStatement("delete from Fornecedor where for_cnpj = '" + n.getCNPJ() + "'");

        setMsg(Banco.con.manipular(pstmt), "Exclusão");

        return flag;
    }
    
    private void setParametros(Fornecedor n)
    {
        int count = 0;
        try
        {
            pstmt.setString(++count, n.getCNPJ());
            pstmt.setString(++count, n.getCep());
            pstmt.setString(++count, n.getNome());
            pstmt.setString(++count, n.getTelefone());
            pstmt.setString(++count, n.getEmail());
            pstmt.setString(++count, n.getObs());
            /////if(((Usuario)p).getImagem() != null){
            byte[] a =
            {
                1
            };
            pstmt.setBinaryStream(++count, (((Fornecedor) n).getImagem() != null) ? Imagem.BufferedImageToInputStream(((Fornecedor) n).getImagem()) : new ByteArrayInputStream(a/*(1)*/));
            /////}
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }
    
    public ArrayList<Fornecedor> get(String filtro)
    {
        String sql = null;
        ArrayList<Fornecedor> n = new ArrayList<>();
        ResultSet rs;
        sql = "select * from Fornecedor where upper(for_nome) like upper('" + filtro + "%')"
                + " or upper(for_cnpj) like upper('" + filtro + "%') or upper(end_cep) like upper('" + filtro + "%')"
                + " or upper(for_telefone) like upper('" + filtro + "%')";
        sql += getCodigo("nivel_cod", filtro);

        rs = Banco.con.consultar(sql);
        try
        {
            while (rs.next())
            {
                n.add(new Fornecedor(rs.getString("for_nome"), rs.getString("for_cnpj"), rs.getString("end_cep"), rs.getString("for_telefone"), rs.getString("for_email"), rs.getString("for_obs"), Imagem.ByteArrayToBufferedImage(rs.getBytes("for_foto"))));
            }
        } catch (Exception ex)
        {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }

        return n;
    }
    
    
}
