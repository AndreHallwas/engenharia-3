/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fornecedor.Controladora;

import Fornecedor.Dao.CtrFornecedor;
import Fornecedor.Entidade.Fornecedor;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class CtrlFornecedor
{

    public static CtrlFornecedor create() {
        return new CtrlFornecedor();
    }

    private CtrlFornecedor() {
    }

    public static void setCampos(Object fornecedor, TextField txbNome, TextField txbCNPJ, TextField txbEmail, TextField txbTelefone, TextArea taObs) {
        Fornecedor f = (Fornecedor) fornecedor;
        txbNome.setText(f.getNome());
        txbCNPJ.setText(f.getCNPJ());
        txbEmail.setText(f.getEmail());
        txbTelefone.setText(f.getTelefone());
        taObs.setText(f.getObs());
    }

    public static BufferedImage getImagem(Object fornecedor) {
        return ((Fornecedor)fornecedor).getImagem();
    }

    public static String getEndereco(Object fornecedor) {
        return ((Fornecedor)fornecedor).getCep();
    }

    private CtrFornecedor cf;

    public ArrayList<Object> Pesquisar(String Filtro)
    {
        cf = new CtrFornecedor();
        ArrayList<Object> Fornecedor = new ArrayList();
        Fornecedor.addAll(cf.get(Filtro));
        return Fornecedor;
    }

    public boolean Salvar(String Nome, String CNPJ, String Cep, String Telefone, String Email, String Obs, BufferedImage Imagem)
    {
        cf = new CtrFornecedor();
        return cf.Salvar(new Fornecedor(Nome, CNPJ, Cep, Telefone, Email, Obs, Imagem));
    }

    public boolean Alterar(String Nome, String CNPJ, String Cep, String Telefone, String Email, String Obs, BufferedImage Imagem)
    {
        cf = new CtrFornecedor();
        return cf.Alterar(new Fornecedor(Nome, CNPJ, Cep, Telefone, Email, Obs, Imagem));
    }

    public boolean Remover(String CNPJ)
    {
        cf = new CtrFornecedor();
        return cf.Apagar(new Fornecedor(CNPJ));
    }

    public String getMsg()
    {
        return cf.getMsg();
    }

}
