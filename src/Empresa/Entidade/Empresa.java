/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa.Entidade;

import java.awt.image.BufferedImage;

/**
 *
 * @author Raizen
 */
public class Empresa
{

    String Nome;
    String Endereco;
    String Fone;
    String RazaoSocial;
    String CNPJ;
    BufferedImage Imagem;

    public Empresa(String Nome, String CNPJ, String Endereco, String Fone, String RazaoSocial, BufferedImage Imagem)
    {
        this.Nome = Nome;
        this.Endereco = Endereco;
        this.Fone = Fone;
        this.RazaoSocial = RazaoSocial;
        this.CNPJ = CNPJ;
        this.Imagem = Imagem;
    }

    public Empresa()
    {
    }

    public Empresa(String Nome, String CNPJ, String Endereco, String Fone, String RazaoSocial)
    {
        this.Nome = Nome;
        this.Endereco = Endereco;
        this.Fone = Fone;
        this.RazaoSocial = RazaoSocial;
        this.CNPJ = CNPJ;
    }

    public String getNome()
    {
        return Nome;
    }

    public String getEndereco()
    {
        return Endereco;
    }

    public String getTelefone()
    {
        return Fone;
    }

    public String getRazaoSocial()
    {
        return RazaoSocial;
    }

    public String getCNPJ()
    {
        return CNPJ;
    }

    public BufferedImage getImagem()
    {
        return Imagem;
    }

    public void setNome(String Nome)
    {
        this.Nome = Nome;
    }

    public void setEndereco(String Endereco)
    {
        this.Endereco = Endereco;
    }

    public void setFone(String Fone)
    {
        this.Fone = Fone;
    }

    public void setRazaoSocial(String RazaoSocial)
    {
        this.RazaoSocial = RazaoSocial;
    }

    public void setCNPJ(String CNPJ)
    {
        this.CNPJ = CNPJ;
    }

    public void setImagem(BufferedImage Imagem)
    {
        this.Imagem = Imagem;
    }
}
