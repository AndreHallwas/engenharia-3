/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa.Dao;

import Empresa.Entidade.Empresa;
import Padroes.Dao.ControledeEntidade;
import Utils.Banco;
import Utils.Imagem;
import Utils.Mensagem;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Raizen
 */
public class CtrEmpresa extends ControledeEntidade
{
    
    public CtrEmpresa()
    {
        flag = false;
        pstmt = null;
    }

    public boolean Salvar(Empresa f)
    {
        pstmt = Banco.con.geraStatement(
                "insert into Empresa(emp_cnpj, emp_endereco, emp_nome, emp_telefone,"
                + " emp_razaosocial,emp_logo) values(?,?,?,?,?,?)");

        setParametros(f);
        setMsg(Banco.con.manipular(pstmt), "Cadastro");

        return flag;
    }

    public boolean Alterar(Empresa f)
    {
        pstmt = Banco.con.geraStatement("update Empresa set emp_cnpj = ?,"
                + " emp_endereco = ?, emp_nome = ?, emp_telefone = ?, emp_razaosocial = ?, emp_logo = ?");

        setParametros(f);
        setMsg(Banco.con.manipular(pstmt), "Alteração");

        return flag;
    }

    public boolean Apagar(Empresa f)
    {
        pstmt = Banco.con.geraStatement("delete from Empresa where emp_cnpj = '" + f.getCNPJ() + "'");

        setMsg(Banco.con.manipular(pstmt), "Exclusão");

        return flag;
    }
    
    private void setParametros(Empresa f)
    {
        try
        {
            pstmt.setString(1, f.getCNPJ());
            pstmt.setString(2, f.getEndereco());
            pstmt.setString(3, f.getNome());
            pstmt.setString(4, f.getTelefone());
            pstmt.setString(5, f.getRazaoSocial());
            pstmt.setBinaryStream(6, Imagem.BufferedImageToInputStream(f.getImagem()));
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }

    public Empresa get()
    {
        String sql = "select * from Empresa";
        Empresa f = null;
        ResultSet rs = Banco.con.consultar(sql);

        try
        {
            if (rs.next())
                f = new Empresa(rs.getString("emp_nome"), rs.getString("emp_cnpj"), rs.getString("emp_endereco"),
                        rs.getString("emp_telefone"), rs.getString("emp_razaosocial"),
                        Imagem.ByteArrayToBufferedImage(rs.getBytes("emp_logo")));
        } catch (SQLException ex)
        {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }

        return f;
    }
}
