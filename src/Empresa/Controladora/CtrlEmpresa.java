/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa.Controladora;

import Empresa.Dao.CtrEmpresa;
import Empresa.Entidade.Empresa;
import java.awt.image.BufferedImage;
import javafx.scene.control.TextField;

/**
 *
 * @author Raizen
 */
public class CtrlEmpresa {

    public static BufferedImage getImagem(Object empresa) {
        return ((Empresa)empresa).getImagem();
    }

    public static void setCampos(Object empresa, TextField txbNome, TextField txbCNPJ, TextField txbEndereco, TextField txbFone, TextField txbRazao2) {
        Empresa p = (Empresa) empresa;
        txbNome.setText(p.getNome());
        txbCNPJ.setText(p.getCNPJ());
        txbEndereco.setText(p.getEndereco());
        txbFone.setText(p.getTelefone());
        txbRazao2.setText(p.getRazaoSocial());
    }

    public static CtrlEmpresa create() {
        return new CtrlEmpresa();
    }

    private CtrEmpresa ce;

    private CtrlEmpresa() {
    }

    public Object Pesquisar() {
        ce = new CtrEmpresa();
        return ce.get();
    }

    public boolean Salvar(String Nome, String CNPJ, String Endereco, String Fone, String RazaoSocial, BufferedImage Imagem) {
        ce = new CtrEmpresa();
        return ce.Salvar(new Empresa(Nome, CNPJ, Endereco, Fone, RazaoSocial, Imagem));
    }

    public boolean Alterar(String Nome, String CNPJ, String Endereco, String Fone, String RazaoSocial, BufferedImage Imagem) {
        ce = new CtrEmpresa();
        return ce.Alterar(new Empresa(Nome, CNPJ, Endereco, Fone, RazaoSocial, Imagem));
    }

    public boolean Remover(String Nome, String CNPJ, String Endereco, String Fone, String RazaoSocial, BufferedImage Imagem) {
        ce = new CtrEmpresa();
        return ce.Apagar(new Empresa(Nome, CNPJ, Endereco, Fone, RazaoSocial, Imagem));
    }
}
