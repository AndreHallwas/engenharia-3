/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Dal;

import Endereco.Entidade.Bairro;
import Endereco.Entidade.Cidade;
import Endereco.Entidade.Endereco;
import Endereco.Entidade.Estado;
import Endereco.Entidade.Pais;
import Padroes.Dao.ControledeEntidade;
import Utils.Banco;
import Utils.Mensagem;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * ii
 *
 * @author Aluno
 */
public class CtrEndereco extends ControledeEntidade
{

    private boolean flag;
    private PreparedStatement pstmt;
    private String Msg;

    public CtrEndereco()
    {
        flag = false;
        pstmt = null;
    }

    private void setParametrosBairro(Bairro b)
    {
        try
        {
            pstmt.setString(1, b.getCodigo());
            pstmt.setString(2, b.getNome());
            pstmt.setString(3, b.getCidade().getCodigo());
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }

    private void setParametrosCidade(Cidade c)
    {
        try
        {
            pstmt.setString(1, c.getCodigo());
            pstmt.setString(2, c.getNome());
            pstmt.setString(3, c.getEstado().getCodigo());
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }

    private void setParametrosEstado(Estado e)
    {
        try
        {
            pstmt.setString(1, e.getCodigo());
            pstmt.setString(2, e.getNome());
            pstmt.setString(3, e.getPais().getCodigo());
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }

    private void setParametrosPais(Pais p)
    {
        try
        {
            pstmt.setString(1, p.getCodigo());
            pstmt.setString(2, p.getNome());
            pstmt.setString(3, p.getSGL());
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }

    private void setParametrosEndereco(Endereco e)
    {
        try
        {
            pstmt.setString(1, e.getCEP());
            pstmt.setString(2, e.getEndereco());
            pstmt.setString(3, e.getBairro().getCodigo());
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }
    
    public ArrayList<Object> getPais(String codigo)
    {
        ArrayList<Object> lista = new ArrayList<>();
        String sql = "select * from pais where upper(pais_nome) like upper('%" + codigo + "%')"
                + " or upper(pais_sgl) like upper('%" + codigo + "%')";
        sql += getCodigo("pais_cod", codigo);
        ResultSet rs = Banco.con.consultar(Banco.con.geraStatement(sql));

        try
        {
            while (rs.next())
            {
                lista.add(new Pais(rs.getString("pais_cod"), rs.getString("pais_nome"), rs.getString("pais_sgl")));
            }
            setMsg(true, "Consulta");
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex, "Erro Na Consulta");
        }

        return lista;
    }

    public ArrayList<Object> getEstado(String codigo)
    {
        ArrayList<Object> lista = new ArrayList<>();
        String sql = "select * from estado where upper(est_nome) like upper('%" + codigo + "%')"
                + " or upper(est_uf) like upper('%" + codigo + "%')";
        sql += getCodigo("est_cod", codigo);
        sql += getCodigo("pais_cod", codigo);
        ResultSet rs = Banco.con.consultar(Banco.con.geraStatement(sql));

        try
        {
            while (rs.next())
            {
                Pais p = (Pais) getPais(rs.getString("pais_cod")).get(0);
                lista.add(new Estado(rs.getString("est_cod"), rs.getString("est_uf"), rs.getString("est_nome"), p));
            }
            setMsg(true, "Consulta");
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex, "Erro Na Consulta");
        }

        return lista;
    }

    public ArrayList<Object> getCidade(String codigo)
    {
        ArrayList<Object> lista = new ArrayList<>();
        String sql = "select * from cidade where upper(cid_nome) like upper('%" + codigo + "%')";
        sql += getCodigo("cid_cod", codigo);
        sql += getCodigo("est_cod", codigo);
        ResultSet rs = Banco.con.consultar(Banco.con.geraStatement(sql));

        try
        {
            while (rs.next())
            {
                Estado e = (Estado) getEstado(rs.getString("est_cod")).get(0);
                lista.add(new Cidade(rs.getString("cid_cod"), rs.getString("cid_nome"), e));
            }
            setMsg(true, "Consulta");
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex, "Erro Na Consulta");
        }

        return lista;
    }

    public ArrayList<Object> getBairro(String codigo)
    {
        ArrayList<Object> lista = new ArrayList<>();
        String sql = "select * from bairro where upper(bai_nome) like upper('%" + codigo + "%')";
        sql += getCodigo("bai_cod", codigo);
        sql += getCodigo("cid_cod", codigo);
        ResultSet rs = Banco.con.consultar(Banco.con.geraStatement(sql));

        try
        {
            while (rs.next())
            {
                Cidade c = (Cidade) getCidade(rs.getString("cid_cod")).get(0);
                lista.add(new Bairro(rs.getString("bai_cod"), rs.getString("bai_nome"), c));
            }
            setMsg(true, "Consulta");
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex, "Erro Na Consulta");
        }

        return lista;
    }

    public ArrayList<Object> get(String codigo)
    {
        String sql = null;
        ArrayList<Object> lista = new ArrayList<>();
        ResultSet rs;
        try
        {
            sql = "select * from Endereco where end_cep = '" + codigo + "'"
                    + " or upper(end_local) like upper('" + codigo + "%')";
            sql += getCodigo("bai_cod", codigo);
            pstmt = Banco.con.geraStatement(sql);
            rs = Banco.con.consultar(pstmt);
            Bairro b;
            while (rs.next())
            {
                b = (Bairro) getBairro(rs.getString("bai_cod")).get(0);
                lista.add(new Endereco(rs.getString("end_cep"), rs.getString("end_local"), b));
            }
            setMsg(true, "Consulta");
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex, "Erro Na Consulta");
        }
        return lista;
    }
    
}
