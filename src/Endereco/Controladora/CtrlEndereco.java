/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Controladora;

import Endereco.Dal.CtrEndereco;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class CtrlEndereco
{

    public static CtrlEndereco create() {
        return new CtrlEndereco();
    }

    private CtrEndereco ce;

    private CtrlEndereco() {
    }

    public ArrayList<Object> PesquisarEndereco(String Filtro)
    {
        ce = new CtrEndereco();
        return ce.get(Filtro);
    }

    public ArrayList<Object> PesquisarBairro(String Filtro)
    {
        ce = new CtrEndereco();
        return ce.getBairro(Filtro);
    }

    public ArrayList<Object> PesquisarCidade(String Filtro)
    {
        ce = new CtrEndereco();
        return ce.getCidade(Filtro);
    }

    public ArrayList<Object> PesquisarEstado(String Filtro)
    {
        ce = new CtrEndereco();
        return ce.getEstado(Filtro);
    }
}
