/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Entidade;

/**
 *
 * @author Aluno
 */
public class Cidade
{

    private String Codigo;
    private String Nome;
    private Estado estado;

    public Cidade()
    {
    }

    public Cidade(String Codigo, String Nome, Estado estado)
    {
        this.Codigo = Codigo;
        this.Nome = Nome;
        this.estado = estado;
    }

    public String getCodigo()
    {
        return Codigo;
    }

    public String getNome()
    {
        return Nome;
    }

    public void setCodigo(String Codigo)
    {
        this.Codigo = Codigo;
    }

    public void setNome(String Nome)
    {
        this.Nome = Nome;
    }

    public Estado getEstado()
    {
        return estado;
    }

    public void setEstado(Estado estado)
    {
        this.estado = estado;
    }

    @Override
    public String toString()
    {
        return Nome;
    }

}
