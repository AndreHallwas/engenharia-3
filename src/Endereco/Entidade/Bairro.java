/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Entidade;

/**
 *
 * @author Aluno
 */
public class Bairro
{

    private String Codigo;
    private String Nome;
    private Cidade cidade;

    public Bairro()
    {
    }

    public Bairro(String Codigo, String Nome, Cidade cidade)
    {
        this.Codigo = Codigo;
        this.Nome = Nome;
        this.cidade = cidade;
    }

    public String getCodigo()
    {
        return Codigo;
    }

    public String getNome()
    {
        return Nome;
    }

    public Cidade getCidade()
    {
        return cidade;
    }

    public void setCodigo(String Codigo)
    {
        this.Codigo = Codigo;
    }

    public void setNome(String Nome)
    {
        this.Nome = Nome;
    }

    public void setCidade(Cidade cidade)
    {
        this.cidade = cidade;
    }

    @Override
    public String toString()
    {
        return Nome;
    }

}
