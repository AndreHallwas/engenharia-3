/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Entidade;

/**
 *
 * @author Raizen
 */
public class Pais
{

    private String Codigo;
    private String Nome;
    private String SGL;

    public Pais()
    {
    }

    public Pais(String Codigo, String Nome, String SGL)
    {
        this.Codigo = Codigo;
        this.Nome = Nome;
        this.SGL = SGL;
    }

    public String getCodigo()
    {
        return Codigo;
    }

    public String getNome()
    {
        return Nome;
    }

    public String getSGL()
    {
        return SGL;
    }

    public void setCodigo(String Codigo)
    {
        this.Codigo = Codigo;
    }

    public void setNome(String Nome)
    {
        this.Nome = Nome;
    }

    public void setSGL(String SGL)
    {
        this.SGL = SGL;
    }

    @Override
    public String toString()
    {
        return Nome;
    }
}
