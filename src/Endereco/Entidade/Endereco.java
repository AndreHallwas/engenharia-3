/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Entidade;

import Endereco.Entidade.Bairro;

/**
 *
 * @author Aluno
 */
public class Endereco
{

    private String CEP;
    private String Endereco;
    private Bairro bairro;

    public Endereco()
    {
    }

    public Endereco(String CEP, String Endereco, Bairro bairro)
    {
        this.CEP = CEP;
        this.Endereco = Endereco;
        this.bairro = bairro;
    }

    public String getCEP()
    {
        return CEP;
    }

    public String getEndereco()
    {
        return Endereco;
    }

    public Bairro getBairro()
    {
        return bairro;
    }

    public void setCEP(String CEP)
    {
        this.CEP = CEP;
    }

    public void setEndereco(String Endereco)
    {
        this.Endereco = Endereco;
    }

    public void setBairro(Bairro bairro)
    {
        this.bairro = bairro;
    }

    @Override
    public String toString()
    {
        return Endereco;
    }
}
