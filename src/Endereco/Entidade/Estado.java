package Endereco.Entidade;

public class Estado
{

    private String Codigo;
    private String UF;
    private String Nome;
    private Pais Pais;

    public Estado()
    {
    }

    public Estado(String Codigo, String UF, String Nome, Pais Pais)
    {
        this.Codigo = Codigo;
        this.UF = UF;
        this.Nome = Nome;
        this.Pais = Pais;
    }

    public String getCodigo()
    {
        return Codigo;
    }

    public String getUF()
    {
        return UF;
    }

    public String getNome()
    {
        return Nome;
    }

    public Pais getPais()
    {
        return Pais;
    }

    public void setCodigo(String Codigo)
    {
        this.Codigo = Codigo;
    }

    public void setUF(String UF)
    {
        this.UF = UF;
    }

    public void setNome(String Nome)
    {
        this.Nome = Nome;
    }

    public void setPais(Pais Pais)
    {
        this.Pais = Pais;
    }

    @Override
    public String toString()
    {
        return Nome;
    }

}
