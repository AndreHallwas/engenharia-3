/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Entidade;

/**
 *
 * @author Aluno
 */
public class Tipo {
    private String Codigo;
    private String Tipo;

    public Tipo(String Codigo, String Tipo) {
        this.Codigo = Codigo;
        this.Tipo = Tipo;
    }

    public String getCodigo() {
        return Codigo;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }
    
}
