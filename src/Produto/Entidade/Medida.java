/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Entidade;

/**
 *
 * @author Aluno
 */
public class Medida {
     private String Medida_Prod;
     private String Codigo;

    public Medida(String Medida_Prod, String Codigo) {
        this.Medida_Prod = Medida_Prod;
        this.Codigo = Codigo;
    }

    public String getMedida_Prod() {
        return Medida_Prod;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setMedida_Prod(String Medida_Prod) {
        this.Medida_Prod = Medida_Prod;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }
     
    
}
