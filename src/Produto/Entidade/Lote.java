/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Entidade;

import Produto.Entidade.Produto;

/**
 *
 * @author Raizen
 */
public class Lote {
    private String Codigo;
    private int EstoqueInicial;
    private int Estoque;
    private String Validade;
    // private Produto 

    public Lote() {
    }

    public Lote(String Codigo, int EstoqueInicial, int Estoque, String Validade) {
        this.Codigo = Codigo;
        this.EstoqueInicial = EstoqueInicial;
        this.Estoque = Estoque;
        this.Validade = Validade;
    }

    public Lote(String Codigo) {
        this.Codigo = Codigo;
    }

    public Lote(int EstoqueInicial, int Estoque, String Validade) {
        this.EstoqueInicial = EstoqueInicial;
        this.Estoque = Estoque;
        this.Validade = Validade;
    }

    public String getCodigo() {
        return Codigo;
    }

    public int getEstoqueInicial() {
        return EstoqueInicial;
    }

    public int getEstoque() {
        return Estoque;
    }

    public String getValidade() {
        return Validade;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public void setEstoqueInicial(int EstoqueInicial) {
        this.EstoqueInicial = EstoqueInicial;
    }

    public void setEstoque(int Estoque) {
        this.Estoque = Estoque;
    }

    public void setValidade(String Validade) {
        this.Validade = Validade;
    }
    
}
