/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Entidade;

/**
 *
 * @author Aluno
 */
public class Local {
    private String Local_Desc;
    private String Local_Num;
    private String Codigo;

    public Local(String Local_Desc, String Local_Num, String Codigo) {
        this.Local_Desc = Local_Desc;
        this.Local_Num = Local_Num;
        this.Codigo = Codigo;
    }

    public String getLocal_Desc() {
        return Local_Desc;
    }

    public String getLocal_Num() {
        return Local_Num;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setLocal_Desc(String Local_Desc) {
        this.Local_Desc = Local_Desc;
    }

    public void setLocal_Num(String Local_Num) {
        this.Local_Num = Local_Num;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }
    
    
}
