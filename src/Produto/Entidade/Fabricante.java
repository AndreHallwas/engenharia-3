/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Entidade;

import java.awt.image.BufferedImage;

/**
 *
 * @author Raizen
 */
public class Fabricante
{

    private String Nome;
    private String CNPJ;
    private String Cep;
    private String Telefone;
    private String Email;
    private String Obs;
    private BufferedImage Imagem;

    public Fabricante()
    {
    }

    public Fabricante(String CNPJ) {
        this.CNPJ = CNPJ;
    }

    public Fabricante(String Nome, String CNPJ, String Cep, String Telefone, String Email, String Obs, BufferedImage Imagem)
    {
        this.Nome = Nome;
        this.CNPJ = CNPJ;
        this.Cep = Cep;
        this.Telefone = Telefone;
        this.Email = Email;
        this.Obs = Obs;
        this.Imagem = Imagem;
    }

    /**
     * @return the Nome
     */
    public String getNome() {
        return Nome;
    }

    /**
     * @param Nome the Nome to set
     */
    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    /**
     * @return the CNPJ
     */
    public String getCNPJ() {
        return CNPJ;
    }

    /**
     * @param CNPJ the CNPJ to set
     */
    public void setCNPJ(String CNPJ) {
        this.CNPJ = CNPJ;
    }

    /**
     * @return the Cep
     */
    public String getCep() {
        return Cep;
    }

    /**
     * @param Cep the Cep to set
     */
    public void setCep(String Cep) {
        this.Cep = Cep;
    }

    /**
     * @return the Telefone
     */
    public String getTelefone() {
        return Telefone;
    }

    /**
     * @param Telefone the Telefone to set
     */
    public void setTelefone(String Telefone) {
        this.Telefone = Telefone;
    }

    /**
     * @return the Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     * @param Email the Email to set
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     * @return the Obs
     */
    public String getObs() {
        return Obs;
    }

    /**
     * @param Obs the Obs to set
     */
    public void setObs(String Obs) {
        this.Obs = Obs;
    }

    /**
     * @return the Imagem
     */
    public BufferedImage getImagem() {
        return Imagem;
    }

    /**
     * @param Imagem the Imagem to set
     */
    public void setImagem(BufferedImage Imagem) {
        this.Imagem = Imagem;
    }


}
