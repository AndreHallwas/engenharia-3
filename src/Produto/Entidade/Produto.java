package Produto.Entidade;

import java.awt.image.BufferedImage;



public class Produto //implements 
{
    private String Codigo;
    private String Nome;
    private String Obs;
    private String NomeF;
    private double PrecoFab;
    private double PrecoCom;
    private Fabricante fabricante;
    private Tipo tipo;
    private Local local;
    private Medida medida;
    private BufferedImage Foto;
    private Lote lote; // lista de lotes

    public Produto(String Codigo) {
        this.Codigo = Codigo;
    }
    

    public Produto(String Codigo, String Nome, String Obs, String NomeF, double PrecoFab, double PrecoCom, Fabricante fabricante, String Tipo, String Local_Desc, String Local_Num, String Medida_Prod, BufferedImage Foto, Lote lote) {
        this.Codigo = Codigo;
        this.Nome = Nome;
        this.Obs = Obs;
        this.NomeF = NomeF;
        this.PrecoFab = PrecoFab;
        this.PrecoCom = PrecoCom;
        this.fabricante = fabricante;
        this.tipo = new Tipo(null, Tipo);
        local = new Local(Local_Desc, Local_Num, null);
        this.medida = new Medida(Medida_Prod, null);
        this.Foto = Foto;
        this.lote = lote;
    }

    public Produto(String Codigo, String Nome, String Obs, String NomeF, double PrecoFab, double PrecoCom, Fabricante fabricante, String Tipo, String Local_Desc, String Local_Num, String Medida_Prod, BufferedImage Foto) {
        this.Codigo = Codigo;
        this.Nome = Nome;
        this.Obs = Obs;
        this.NomeF = NomeF;
        this.PrecoFab = PrecoFab;
        this.PrecoCom = PrecoCom;
        this.fabricante = fabricante;
        this.tipo = new Tipo(null, Tipo);
        local = new Local(Local_Desc, Local_Num, null);
        this.medida = new Medida(Medida_Prod, null);
        this.Foto = Foto;
    }

    public Produto(String Codigo, String Nome, String Obs, String NomeF, double PrecoFab, double PrecoCom, Fabricante fabricante, String Tipo, String Local_Desc, String Local_Num, String Medida_Prod) {
        this.Codigo = Codigo;
        this.Nome = Nome;
        this.Obs = Obs;
        this.NomeF = NomeF;
        this.PrecoFab = PrecoFab;
        this.PrecoCom = PrecoCom;
        this.fabricante = fabricante;
        this.tipo = new Tipo(null, Tipo);
        local = new Local(Local_Desc, Local_Num, null);
        this.medida = new Medida(Medida_Prod, null);
    }
    
    public Produto()
    {
    }

    public Produto(String Codigo, String Nome, String Marca, String Obs)
    {
        this.Codigo = Codigo;
        this.Nome = Nome;
        this.Obs = Obs;
    }

    public Produto(String Nome, String Obs)
    {
        this.Nome = Nome;
        this.Obs = Obs;
    }

    public void setCodigo(String Codigo)
    {
        this.Codigo = Codigo;
    }

    public String getNomeF()
    {
        return NomeF;
    }

    public double getPrecoFab()
    {
        return PrecoFab;
    }

    public double getPrecoCom()
    {
        return PrecoCom;
    }

    public void setNome(String Nome)
    {
        this.Nome = Nome;
    }

    public void setObs(String Obs)
    {
        this.Obs = Obs;
    }

    public String getCodigo()
    {
        return Codigo;
    }

    public String getNome()
    {
        return Nome;
    }

    public void setNomeF(String NomeF)
    {
        this.NomeF = NomeF;
    }

    public void setPrecoFab(double PrecoFab)
    {
        this.PrecoFab = PrecoFab;
    }

    public void setPrecoCom(double PrecoCom)
    {
        this.PrecoCom = PrecoCom;
    }
    
    public String getObs()
    {
        return Obs;
    }

    public String getTipo() {
        return tipo.getTipo();
    }

    public String getLocal_Desc() {
        return local.getLocal_Desc();
    }

    public String getLocal_Num() {
        return local.getLocal_Num();
    }

    public String getMedida_Prod() {
        return medida.getMedida_Prod();
    }

    public void setTipo(String Tipo) {
        this.tipo = new Tipo(null, Tipo);
    }

    public void setLocal_Desc(String Local_Desc) {
        local.setLocal_Desc(Local_Desc);
    }

    public void setLocal_Num(String Local_Num) {
        local.setLocal_Num(Local_Num);
    }

    public void setMedida_Prod(String Medida_Prod) {
        this.medida = new Medida(Medida_Prod, null);
    }

    public Fabricante getFabricante() {
        return fabricante;
    }

    public void setFabricante(Fabricante fabricante) {
        this.fabricante = fabricante;
    }

    public BufferedImage getFoto() {
        return Foto;
    }

    public void setFoto(BufferedImage Foto) {
        this.Foto = Foto;
    }
    
}
