package Produto.Controladora;

import Produto.Entidade.Lote;
import Produto.Entidade.Produto;
import Produto.Dao.CtrLote;
import java.util.ArrayList;
import javafx.scene.control.TextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Drake
 */
public class CtrlLote {

    public static CtrlLote create() {
        return new CtrlLote();
    }

    private CtrlLote() {
    }

    public static String getLoteCodigo(Object lote) {
        return ((Lote) lote).getCodigo();
    }

    public static String getProdutoCodigo(Object produto) {
        return ((Produto) produto).getCodigo();
    }

    public static void setProduto(Object produto, TextField txbProduto) {
        txbProduto.setText(((Produto) produto).getNome());
    }

    public static void setLote(Object lote, TextField txbLote, TextField txbValidade) {
        txbValidade.setText(((Lote)lote).getValidade());
        txbLote.setText(((Lote)lote).getCodigo());
    }

    private CtrLote cl;

    public ArrayList<Object> Pesquisar(String Filtro, int opc) {
        cl = new CtrLote();
        ArrayList<Object> Lotes = new ArrayList();
        Lotes.addAll(cl.get(Filtro, opc));
        return Lotes;
    }

    public boolean Salvar(String Codigo, int EstoqueInicial, int Estoque, String Validade, String Produto) {
        CtrlProduto cp = CtrlProduto.create();
        Produto p = (Produto) cp.Pesquisar(Produto).get(0);
        cp.Notify(p);
        cl = new CtrLote();
        return cl.Salvar(new Lote(Codigo, EstoqueInicial, Estoque, Validade), p);
    }

    public boolean Alterar(String Codigo, int EstoqueInicial, int Estoque, String Validade, String Produto) {
        CtrlProduto cp = CtrlProduto.create();
        Produto p = (Produto) cp.Pesquisar(Produto).get(0);
        cl = new CtrLote();
        return cl.Alterar(new Lote(Codigo, EstoqueInicial, Estoque, Validade), p);
    }

    public boolean Remover(String Lote, String Produto) {
        cl = new CtrLote();
        return cl.Apagar(new Lote(Lote), new Produto(Produto));
    }

    public String getMsg() {
        return cl.getMsg();
    }

}
