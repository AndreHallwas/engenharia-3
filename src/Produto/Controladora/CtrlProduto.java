/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Controladora;

import Produto.Entidade.Fabricante;
import Produto.Entidade.Produto;
import Produto.Dao.CtrProduto;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import Padroes.Subject;

/**
 *
 * @author Raizen
 */
public class CtrlProduto implements Subject {

    public static CtrlProduto create() {
        return new CtrlProduto();
    }

    private CtrlProduto() {
    }

    public static void setCampos(Object produto, TextField txbNome, TextField txbPreco, TextField txbPrecoCon, TextField txbObsMedida, TextField txbNomeFarmaceutico, TextField txbLocalNum, TextField txbLocalDesc, TextField txbObsMedida0, TextField txbTipo, TextField txbCNPJ, TextArea taObs) {
        Produto f = (Produto) produto;
        txbPrecoCon.setText(f.getPrecoCom() + "");
        txbObsMedida.setText(f.getObs());
        txbNomeFarmaceutico.setText(f.getNomeF());
        txbLocalNum.setText(f.getLocal_Num());
        txbLocalDesc.setText(f.getLocal_Desc());
        txbObsMedida.setText(f.getMedida_Prod());
        txbTipo.setText(f.getTipo());
        txbCNPJ.setText(f.getFabricante().getCNPJ());
        setCampos(produto, txbNome, txbPreco, taObs);
    }

    public static String getCodigo(Object f) {
        return ((Produto) f).getCodigo();
    }

    public static void setCampos(Object produto, TextField txbNome, TextField txbPreco, TextArea taObs) {
        Produto f = (Produto) produto;
        txbNome.setText(f.getNome());
        taObs.setText(f.getObs());
    }

    private CtrProduto cp;

    public ArrayList<Object> Pesquisar(String Filtro) {
        cp = new CtrProduto();
        ArrayList<Object> Produtos = new ArrayList();
        Produtos.addAll(cp.get(Filtro, 0));
        return Produtos;
    }

    public boolean Salvar(String Codigo, String Nome, String Obs, String NomeF, double PrecoFab, double PrecoCom,
            String fabricante, String Tipo, String Local_Desc, String Local_Num, String Medida_Prod, BufferedImage Foto) {
        CtrlFabricante cf = CtrlFabricante.create();
        cp = new CtrProduto();
        return cp.Salvar(new Produto(Codigo, Nome, Obs, NomeF, PrecoFab, PrecoCom, (Fabricante) cf.Pesquisar(fabricante).get(0),
                Tipo, Local_Desc, Local_Num, Medida_Prod, Foto));
    }

    public boolean Alterar(String Codigo, String Nome, String Obs, String NomeF, double PrecoFab, double PrecoCom,
            String fabricante, String Tipo, String Local_Desc, String Local_Num, String Medida_Prod, BufferedImage Foto) {
        CtrlFabricante cf = CtrlFabricante.create();
        cp = new CtrProduto();
        return cp.Alterar(new Produto(Codigo, Nome, Obs, NomeF, PrecoFab, PrecoCom, (Fabricante) cf.Pesquisar(fabricante).get(0),
                Tipo, Local_Desc, Local_Num, Medida_Prod, Foto));
    }

    public boolean Remover(String Codigo) {
        cp = new CtrProduto();
        return cp.Apagar(new Produto(Codigo));
    }

    public String getMsg() {
        return cp.getMsg();
    }

    @Override
    public boolean register(Object Individuo, Object Objeto) {
        cp = new CtrProduto();
        return cp.register(Individuo, Objeto);
    }

    @Override
    public boolean remove(Object Individuo, Object Objeto) {
        cp = new CtrProduto();
        return cp.remove(Individuo, Objeto);
    }

    @Override
    public void Notify(Object Objeto) {
        cp = new CtrProduto();
        cp.Notify(Objeto);
    }

}
