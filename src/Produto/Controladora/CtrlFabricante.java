/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Controladora;

import Produto.Entidade.Fabricante;
import Produto.Dao.CtrFabricante;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class CtrlFabricante
{

    public static CtrlFabricante create() {
        return new CtrlFabricante();
    }

    private CtrFabricante cf;

    private CtrlFabricante() {
    }

    public ArrayList<Object> Pesquisar(String Filtro)
    {
        cf = new CtrFabricante();
        return cf.get(Filtro);
    }

    public boolean Salvar(String Nome, String CNPJ, String Cep, String Telefone, String Email, String Obs, BufferedImage Imagem)
    {
        cf = new CtrFabricante();
        return cf.Salvar(new Fabricante(Nome, CNPJ, Cep, Telefone, Email, Obs, Imagem));
    }

    public boolean Alterar(String Nome, String CNPJ, String Cep, String Telefone, String Email, String Obs, BufferedImage Imagem)
    {
        cf = new CtrFabricante();
        return cf.Alterar(new Fabricante(Nome, CNPJ, Cep, Telefone, Email, Obs, Imagem));
    }

    public boolean Remover(String CNPJ)
    {
        cf = new CtrFabricante();
        return cf.Apagar(new Fabricante(CNPJ));
    }

    public String getMsg()
    {
        return cf.getMsg();
    }
}
