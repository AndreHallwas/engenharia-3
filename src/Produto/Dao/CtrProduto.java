/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Dao;

import Produto.Controladora.CtrlFabricante;
import Pessoa.Controladora.CtrlPessoa;
import Pessoa.Entidade.Cliente;
import Pessoa.Entidade.Pessoa;
import Produto.Entidade.Produto;
import Utils.Banco;
import Utils.Mensagem;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Padroes.Subject;
import Padroes.Observer;
import Padroes.Dao.ControledeEntidade;
import Padroes.Dao.CtrObserver;

/**
 *
 * @author Raizen
 */
public class CtrProduto extends ControledeEntidade implements Subject{

    public CtrProduto() {
        flag = false;
        pstmt = null;
    }
    
    public boolean Salvar(Produto l)
    {
        pstmt = Banco.con.geraStatement(
                "insert into produto(prod_nome,prod_obs,prod_nome_farmacologico,prod_preco_fabricacao,prod_preco_comercial,"
                        + "fab_cnpj,medida_prod,local_num,local_desc,tipo_desc) values(?,?,?,?,?,?,?,?,?,?)");
        setParametrosProduto(l);
        setMsg(Banco.con.manipular(pstmt), "Cadastro");

        return flag;
    }
    
    public boolean Alterar(Produto l)
    {
      
        pstmt = Banco.con.geraStatement("update Produto set prod_nome = ?, prod_obs = ?, prod_nome_farmacologico = ?,"
                + " prod_preco_fabricacao = ?, prod_preco_comercial = ?, fab_cnpj = ?, medida_prod = ?, local_num = ?,"
                + " local_desc = ?,tipo_desc = ? where prod_cod = '" + ((Produto)l).getCodigo() + "'");
        setParametrosProduto(l);
        
        setMsg(Banco.con.manipular(pstmt), "Alteração");

        return flag;
    }
    
    public boolean Apagar(Produto l)
    {
        pstmt = Banco.con.geraStatement("delete from Produto where prod_cod = '" + l.getCodigo() + "'");
        setMsg(Banco.con.manipular(pstmt), "Exclusão");

        return flag;
    }
    
    private void setParametrosProduto(Produto p)
    {
        int count = 0;
        try
        {
            pstmt.setString(++count, p.getNome());
            pstmt.setString(++count, p.getObs());
            pstmt.setString(++count, p.getNomeF());
            pstmt.setDouble(++count, p.getPrecoFab());
            pstmt.setDouble(++count, p.getPrecoCom());
            pstmt.setString(++count, p.getFabricante().getCNPJ());
            pstmt.setString(++count, p.getMedida_Prod());
            pstmt.setString(++count, p.getLocal_Num());
            pstmt.setString(++count, p.getLocal_Desc());
            pstmt.setString(++count, p.getTipo());
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }
    
    public ArrayList<Produto> get(String filtro, int op)
    {
        String sql = null;
        ArrayList<Produto> lista = new ArrayList();
        ResultSet rs;
        try
        {
            sql = "select * from Produto where upper(prod_nome) like upper('" + filtro + "%')";
            getCodigo("prod_cod", filtro);

            pstmt = Banco.con.geraStatement(sql);
            rs = Banco.con.consultar(pstmt);

            while(rs.next()){
                /*CtrlFabricante cf = new CtrlFabricante();*/
                lista.add(new Produto(rs.getString("prod_cod"), rs.getString("prod_nome"), rs.getString("prod_obs"), 
                        rs.getString("prod_nome_farmacologico"), rs.getDouble("prod_preco_fabricacao"), rs.getDouble("prod_preco_comercial"), 
                        /*cf.Pesquisar(rs.getString("fab_cnpj")).get(0)*/null,rs.getString("tipo_desc"),rs.getString("local_desc"),rs.getString("local_num"),
                        rs.getString("medida_prod")));
            }
            setMsg(true, "Consulta");
                
        } catch (Exception ex)
        {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }
        return lista;
    }

    @Override
    public boolean register(Object Individuo,Object Objeto) {
        if(Individuo instanceof Cliente){
           CtrObserver co = new CtrObserver();
           if(co.get(((Cliente)Individuo).getCPF(),((Produto)Objeto).getCodigo(), 2).isEmpty()){
               return co.Salvar(Individuo, Objeto);
           }
        }
        return false;
    }

    @Override
    public boolean remove(Object Individuo,Object Objeto) {
        if(Individuo instanceof Cliente){
           CtrObserver co = new CtrObserver();
           if(co.get(((Cliente)Individuo).getCPF(),((Produto)Objeto).getCodigo(), 2).isEmpty()){
               return co.Apagar(Individuo, Objeto);
           }
        }
        return false;
    }

    @Override
    public void Notify(Object Objeto) {
        if(Objeto instanceof Produto){
            CtrObserver co = new CtrObserver();
            CtrlPessoa cp = CtrlPessoa.create();
            ArrayList<Object> cliente;
            ArrayList<String> clientes = co.get(((Produto)Objeto).getCodigo(), Msg, 1);
            for(String Individuo : clientes){
                cliente = cp.Pesquisar(Individuo,0);
                if(!cliente.isEmpty()){
                    cp.update(cliente.get(0), Objeto, "Chegou o Produto "+((Produto)Objeto).getNome());
                }
            }
        }
    }
    
}
