package Produto.Dao;


import Produto.Entidade.Produto;
import Produto.Entidade.Lote;
import Padroes.Dao.ControledeEntidade;
import Utils.Banco;
import Utils.Mensagem;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Raizen
 */
public class CtrLote extends ControledeEntidade
{
    
    public CtrLote() {
        flag = false;
        pstmt = null;
    }
    
    public boolean Salvar(Lote l,Produto p)
    {
        pstmt = Banco.con.geraStatement(
                "insert into lote(prod_cod, lote_estoqueinicial, lote_estoque, lote_validade) values(?,?,?,?)");
        setParametrosLote(l,p);
        setMsg(Banco.con.manipular(pstmt), "Cadastro");

        return flag;
    }
    
    public boolean Alterar(Lote l,Produto p)
    {
        pstmt = Banco.con.geraStatement("update lote set prod_cod = ?, lote_estoqueinicial = ?, lote_estoque = ?, "
                + "lote_validade = ? where lot_cod = '" + ((Lote)l).getCodigo() + "'");
        setParametrosLote(l,p);
        
        setMsg(Banco.con.manipular(pstmt), "Alteração");

        return flag;
    }
    
    public boolean Apagar(Lote l,Produto p)
    {
        pstmt = Banco.con.geraStatement("delete from lote where lot_cod = '" + ((Lote)l).getCodigo() + "'");
        setMsg(Banco.con.manipular(pstmt), "Exclusão");

        return flag;
    }
    
    private void setParametrosLote(Lote l,Produto p)
    {
        int count = 0;
        try
        {
            pstmt.setString(++count, p.getCodigo());
            pstmt.setInt(++count, l.getEstoqueInicial());
            pstmt.setInt(++count, l.getEstoque());
            pstmt.setString(++count, l.getValidade());
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }
    
    public ArrayList<Lote> get(String filtro, int op)
    {
        String sql = null;
        ArrayList<Lote> lista = new ArrayList();
        ResultSet rs;
        try
        {   
            sql = "select * from lote";
            if (!filtro.isEmpty()){
                if (op == 1)
                    sql += " where prod_cod in (select prod_cod from produto where upper(prod_nome) like upper('" + filtro + "%')";
                else if (op == 2)
                    sql += " inner join produtos_compra on produtos_compra.lot_cod = lote.lot_cod inner join compra on "
                            + "produtos_compra.com_cod = compra.com_cod where compra.com_cod = '" + filtro + "'";///consulta ruim
                else if(op == 5){
                    sql+= " where prod_cod = '"+ filtro +"'";
                }
                else if(op == 6){
                    sql += " where lot_cod = '" + filtro + "'";
                }
            }

            pstmt = Banco.con.geraStatement(sql);
            rs = Banco.con.consultar(pstmt);

            while (rs.next())
            {
                lista.add(new Lote(rs.getString("lot_cod"), rs.getInt("lote_estoqueinicial"), rs.getInt("lote_estoque"), rs.getString("lote_validade")));
            }
            setMsg(true, "Consulta");
            
        } catch (Exception ex)
        {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }
        return lista;
    }
    
}
