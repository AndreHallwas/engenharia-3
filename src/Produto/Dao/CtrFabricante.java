/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Dao;

import Produto.Entidade.Fabricante;
import Padroes.Dao.ControledeEntidade;
import Utils.Banco;
import Utils.Imagem;
import Utils.Mensagem;
import java.io.ByteArrayInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class CtrFabricante extends ControledeEntidade
{
    
    public CtrFabricante()
    {
        flag = false;
        pstmt = null;
    }

    public boolean Salvar(Fabricante n)
    {
        pstmt = Banco.con.geraStatement(
                "insert into Fabricante(fab_cnpj, end_cep, fab_nome, fab_telefone, fab_email, fab_obs, fab_foto) values(?,?,?,?,?,?,?)");

        setParametros(n);
        setMsg(Banco.con.manipular(pstmt), "Cadastro");

        return flag;
    }

    public boolean Alterar(Fabricante n)
    {
        pstmt = Banco.con.geraStatement("update Fabricante set fab_cnpj = ?,"
                + " end_cep = ?,fab_nome = ?,fab_telefone = ?, fab_email = ?, fab_obs = ?, fab_foto = ?  where fab_cnpj = '" + n.getCNPJ() + "'");

        setParametros(n);
        setMsg(Banco.con.manipular(pstmt), "Alteração");

        return flag;
    }

    public boolean Apagar(Fabricante n)
    {
        pstmt = Banco.con.geraStatement("delete from Fabricante where fab_cnpj = '" + n.getCNPJ() + "'");

        setMsg(Banco.con.manipular(pstmt), "Exclusão");

        return flag;
    }
    
    private void setParametros(Fabricante n)
    {
        int count = 0;
        try
        {
            pstmt.setString(++count, n.getCNPJ());
            pstmt.setString(++count, n.getCep());
            pstmt.setString(++count, n.getNome());
            pstmt.setString(++count, n.getTelefone());
            pstmt.setString(++count, n.getEmail());
            pstmt.setString(++count, n.getObs());
            /////if(((Usuario)p).getImagem() != null){
            byte[] a =
            {
                1
            };
            pstmt.setBinaryStream(++count, (((Fabricante) n).getImagem() != null) ? Imagem.BufferedImageToInputStream(((Fabricante) n).getImagem()) : new ByteArrayInputStream(a/*(1)*/));
            /////}
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }
    
    public ArrayList<Object> get(String filtro)
    {
        String sql = null;
        ArrayList<Object> n = new ArrayList<>();
        ResultSet rs;
        sql = "select * from Fabricante where upper(fab_nome) like upper('" + filtro + "%')"
                + " or upper(fab_cnpj) like upper('" + filtro + "%') or upper(end_cep) like upper('" + filtro + "%')"
                + " or upper(fab_telefone) like upper('" + filtro + "%')";
        sql += getCodigo("nivel_cod", filtro);

        rs = Banco.con.consultar(sql);
        try
        {
            while (rs.next())
            {
                n.add(new Fabricante(rs.getString("fab_nome"), rs.getString("fab_cnpj"), rs.getString("end_cep"), rs.getString("fab_telefone"), rs.getString("fab_email"), rs.getString("fab_obs"), Imagem.ByteArrayToBufferedImage(rs.getBytes("fab_foto"))));
            }
        } catch (Exception ex)
        {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }

        return n;
    }
    
    
}
