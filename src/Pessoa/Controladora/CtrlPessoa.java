/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Controladora;

import Pessoa.Entidade.Cliente;
import Pessoa.Entidade.Nivel;
import Pessoa.Entidade.Pessoa;
import Pessoa.Entidade.Usuario;
import Padroes.Dao.CtrMensagem;
import Pessoa.Dao.CtrPessoa;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import Padroes.Observer;

/**
 *
 * @author Raizen
 */
public class CtrlPessoa implements Observer {

    public static CtrlPessoa create() {
        return new CtrlPessoa();
    }

    public CtrlPessoa() {
    }

    public static void setCampos(Object Pessoa, TextField txbNome, TextField txbCPF, TextField txbEmail, TextField txbRG, TextField txbTelefone, TextArea taObs) {
        Pessoa p = (Pessoa) Pessoa;
        txbNome.setText(p.getNome());
        txbCPF.setText(p.getCPF());
        txbEmail.setText(p.getEmail());
        txbRG.setText(p.getRG());
        txbTelefone.setText(p.getTelefone());
        taObs.setText(p.getObs());
    }

    public static String getEndereco(Object p) {
        return ((Pessoa) p).getEndereco();
    }

    public static BufferedImage getImagem(Object p) {
        return ((Usuario)p).getImagem();
    }

    public static void setCampos(Object pessoa, TextField txbNome, TextField txbCPF, TextField txbEmail, TextField txbRG, TextField txbTelefone, TextArea taObs, TextField txbSalario, PasswordField txbSenha, TextField txbLogin, ComboBox<String> cbNivel) {
        setCampos(pessoa, txbNome, txbCPF, txbEmail, txbRG, txbTelefone, taObs);
        Usuario p = (Usuario) pessoa;
        txbSalario.setText(Double.toString(p.getSalario()));
        txbSenha.setText(Integer.toString(p.getSenha()));
        txbLogin.setText(p.getLogin());
        cbNivel.getSelectionModel().select(p.getNivel().getNome());
    }

    private CtrPessoa cp;

    public ArrayList<Object> Pesquisar(String Filtro, int Tipo) {
        cp = new CtrPessoa();
        ArrayList<Object> Pessoas = new ArrayList();
        Pessoas.addAll(cp.get(Filtro, 2, Tipo));
        return Pessoas;
    }

    public boolean Salvar(String Nome, String CPF, String RG, String Telefone, String Endereco, String Email, String Obs,
            double Salario, Nivel nivel, int Senha, String Login, BufferedImage Imagem) {
        cp = new CtrPessoa();
        if (Login != null && !Login.isEmpty()) {
            return cp.Salvar(new Usuario(Salario, nivel, Senha, Nome, CPF, RG, Telefone, Endereco, Email, Obs, Login, Imagem));
        } else {
            return cp.Salvar(new Cliente(Nome, CPF, RG, Telefone, Endereco, Email, Obs));
        }
    }

    public boolean Alterar(String Nome, String CPF, String RG, String Telefone, String Endereco, String Email, String Obs,
            double Salario, Nivel nivel, int Senha, String Login, BufferedImage Imagem) {
        cp = new CtrPessoa();
        if (Login != null && !Login.isEmpty()) {
            return cp.Alterar(new Usuario(Salario, nivel, Senha, Nome, CPF, RG, Telefone, Endereco, Email, Obs, Login, Imagem));
        } else {
            return cp.Alterar(new Cliente(Nome, CPF, RG, Telefone, Endereco, Email, Obs));
        }
    }

    public boolean Remover(String Pessoa, String Tipo) {
        cp = new CtrPessoa();
        if (Tipo.equalsIgnoreCase("Usuario")) {
            return cp.Apagar(new Usuario(Pessoa));
        } else {
            return cp.Apagar(new Cliente(Pessoa));
        }
    }

    public String getMsg() {
        return cp.getMsg();
    }

    @Override
    public void update(Object Individuo, Object Objeto, Object msg) {
        cp = new CtrPessoa();
        cp.update(Individuo, Objeto, msg);
    }

    public ArrayList<String> getMessages(Object Individuo) {
        CtrMensagem cm = new CtrMensagem();
        if (Individuo instanceof Cliente) {
            return cm.get(((Cliente) Individuo).getCPF());
        }
        return null;
    }
}
