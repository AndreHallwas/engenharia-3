/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Entidade;

/**
 *
 * @author Raizen
 */
abstract public class Pessoa
{

    private String Nome;
    private String CPF;
    private String RG;
    private String Telefone;
    private String Endereco;
    private String Email;
    private String Obs;

    public Pessoa()
    {
    }

    public Pessoa(String Nome, String CPF, String RG, String Telefone, String Endereco, String Email, String Obs)
    {
        this.Nome = Nome;
        this.CPF = CPF;
        this.RG = RG;
        this.Telefone = Telefone;
        this.Endereco = Endereco;
        this.Email = Email;
        this.Obs = Obs;
    }

    public String getNome()
    {
        return Nome;
    }

    public String getCPF()
    {
        return CPF;
    }

    public String getRG()
    {
        return RG;
    }

    public String getTelefone()
    {
        return Telefone;
    }

    public String getEndereco()
    {
        return Endereco;
    }

    public String getEmail()
    {
        return Email;
    }

    public String getObs()
    {
        return Obs;
    }

    public void setNome(String Nome)
    {
        this.Nome = Nome;
    }

    public void setCPF(String CPF)
    {
        this.CPF = CPF;
    }

    public void setRG(String RG)
    {
        this.RG = RG;
    }

    public void setTelefone(String Telefone)
    {
        this.Telefone = Telefone;
    }

    public void setEndereco(String Endereco)
    {
        this.Endereco = Endereco;
    }

    public void setEmail(String Email)
    {
        this.Email = Email;
    }

    public void setObs(String Obs)
    {
        this.Obs = Obs;
    }
}
