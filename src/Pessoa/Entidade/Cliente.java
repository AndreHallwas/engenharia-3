/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Entidade;

/**
 *
 * @author Raizen
 */
public class Cliente extends Pessoa
{

    public Cliente(String CPF) {
        super.setCPF(CPF);
    }

    public Cliente(String Nome, String CPF, String RG, String Telefone, String Endereco, String Email, String Obs)
    {
        super(Nome, CPF, RG, Telefone, Endereco, Email, Obs);
    }
    
}
