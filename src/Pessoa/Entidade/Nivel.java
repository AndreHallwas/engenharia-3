/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Entidade;

/**
 *
 * @author Raizen
 */
public class Nivel
{

    private String Codigo;
    private String Nome;
    private String Permissao;
    private char Visivel;

    public Nivel()
    {
    }

    public Nivel(String Codigo, String Nome, String Permissao, char Visivel)
    {
        this.Codigo = Codigo;
        this.Nome = Nome;
        this.Permissao = Permissao;
        this.Visivel = Visivel;
    }

    public void setVisivel(char Visivel)
    {
        this.Visivel = Visivel;
    }

    public char getVisivel()
    {
        return Visivel;
    }

    public String getCodigo()
    {
        return Codigo;
    }

    public String getNome()
    {
        return Nome;
    }

    public String getPermissao()
    {
        return Permissao;
    }

    public void setCodigo(String Codigo)
    {
        this.Codigo = Codigo;
    }

    public void setNome(String Nome)
    {
        this.Nome = Nome;
    }

    public void setPermissao(String Permissao)
    {
        this.Permissao = Permissao;
    }

    @Override
    public String toString()
    {
        return Nome;
    }

}
