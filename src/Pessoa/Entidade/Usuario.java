/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Entidade;

import java.awt.image.BufferedImage;

/**
 *
 * @author Raizen
 */
public class Usuario extends Pessoa
{

    private int cod;
    private double Salario;
    private Nivel nivel;
    private int Senha;
    private String Login;
    private BufferedImage Imagem;

    public Usuario()
    {
    }

    public Usuario(String Login) {
        this.Login = Login;
    }

    public Usuario(double Salario, Nivel nivel, int Senha, String Nome, String CPF, String RG, String Telefone, String Endereco, String Email, String Obs, String Login, BufferedImage Imagem)
    {
        super(Nome, CPF, RG, Telefone, Endereco, Email, Obs);
        this.Salario = Salario;
        this.nivel = nivel;
        this.Senha = Senha;
        this.Login = Login;
        this.Imagem = Imagem;
    }

    public void setLogin(String Login)
    {
        this.Login = Login;
    }

    public String getLogin()
    {
        return Login;
    }

    public int getSenha()
    {
        return Senha;
    }

    public void setSenha(int Senha)
    {
        this.Senha = Senha;
    }

    public double getSalario()
    {
        return Salario;
    }

    public void setSalario(double Salario)
    {
        this.Salario = Salario;
    }

    public int getCod()
    {
        return cod;
    }

    public void setCod(int cod)
    {
        this.cod = cod;
    }

    public Nivel getNivel()
    {
        return nivel;
    }

    public void setNivel(Nivel nivel)
    {
        this.nivel = nivel;
    }

    public BufferedImage getImagem()
    {
        return Imagem;
    }

    public void setImagem(BufferedImage Imagem)
    {
        this.Imagem = Imagem;
    }

}
