/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Dao;

import Pessoa.Entidade.Cliente;
import Pessoa.Entidade.Pessoa;
import Produto.Entidade.Produto;
import Pessoa.Entidade.Usuario;
import Utils.Banco;
import Utils.Imagem;
import Utils.Mensagem;
import java.io.ByteArrayInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Padroes.Subject;
import Padroes.Observer;
import Padroes.Dao.ControledeEntidade;
import Padroes.Dao.CtrMensagem;

/**
 *
 * @author Raizen
 */
public class CtrPessoa extends ControledeEntidade implements Observer
{
    
    public CtrPessoa()
    {
        flag = false;
        pstmt = null;
    }

    public boolean Salvar(Pessoa p)
    {
        if (p instanceof Usuario)
        {
            //if(((Usuario)p).getImagem() != null){
            pstmt = Banco.con.geraStatement("insert into Usuario(usr_email,usr_datacadastro, usr_RG, usr_CPF, usr_nome, nivel_cod, usr_Obs, usr_telefone, end_cep, usr_salario, usr_senha,usr_foto ,usr_login)"
                    + " values(?,CURRENT_TIMESTAMP, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            /*}else{
                pstmt =  Banco.con.geraStatement("insert into Usuario(usr_email, usr_RG, usr_CPF, usr_nome, nivel_cod, usr_Obs, usr_telefone, end_cep, usr_salario, usr_senha ,usr_login)"
                        +" values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            }*/
            setParametrosUsuario(p);
        } else if (p instanceof Cliente)
        {
            pstmt = Banco.con.geraStatement("insert into Cliente(cli_email, cli_RG, cli_CPF, cli_nome, cli_Obs, cli_telefone, end_cep)"
                    + " values(?, ?, ?, ?, ?, ?, ?)");
            setParametrosCliente(p);
        }

        setMsg(Banco.con.manipular(pstmt), "Cadastro");

        return flag;
    }

    private void setParametrosUsuario(Pessoa p)
    {
        int count = 0;
        try
        {
            pstmt.setString(++count, p.getEmail());
            pstmt.setString(++count, p.getRG());
            pstmt.setString(++count, p.getCPF());
            pstmt.setString(++count, p.getNome());
            pstmt.setInt(++count, Integer.parseInt(((Usuario) p).getNivel().getCodigo()));
            pstmt.setString(++count, p.getObs());
            pstmt.setString(++count, p.getTelefone());
            pstmt.setString(++count, p.getEndereco());
            pstmt.setDouble(++count, ((Usuario) p).getSalario());
            pstmt.setString(++count, Integer.toString(((Usuario) p).getSenha()));
            /////if(((Usuario)p).getImagem() != null){
            byte[] a = {1};
            pstmt.setBinaryStream(++count, (((Usuario) p).getImagem() != null) ? Imagem.BufferedImageToInputStream(((Usuario) p).getImagem()) : new ByteArrayInputStream(a/*(1)*/));
            /////}
            pstmt.setString(++count, ((Usuario) p).getLogin());

        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex);
        }
    }

    private void setParametrosCliente(Pessoa p)
    {
        try
        {
            pstmt.setString(1, p.getEmail());
            pstmt.setString(2, p.getRG());
            pstmt.setString(3, p.getCPF());
            pstmt.setString(4, p.getNome());
            pstmt.setString(5, p.getObs());
            pstmt.setString(6, p.getTelefone());
            pstmt.setString(7, p.getEndereco());
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }
    
    public boolean Alterar(Pessoa p)
    {
        if (p instanceof Usuario)
        {
            pstmt = Banco.con.geraStatement("update Usuario set usr_email = ?, usr_RG = ?, usr_CPF = ?, usr_nome = ?, nivel_cod = ?, usr_Obs = ?, usr_telefone = ?, end_cep = ?, usr_salario = ?, usr_senha = ?, usr_foto = ? where usr_login = '" + ((Usuario) p).getLogin() + "'");
            setParametrosUsuario(p);
        } else if (p instanceof Cliente)
        {
            pstmt = Banco.con.geraStatement("update Cliente set cli_email = ?, cli_RG = ?, cli_CPF = ?, cli_nome = ?, cli_Obs = ?, cli_telefone = ?, end_cep = ? where cli_rg = '" + p.getRG() + "'");
            setParametrosCliente(p);
        }

        setMsg(Banco.con.manipular(pstmt), "Alteração");

        return flag;
    }

    public boolean Apagar(Pessoa p)
    {
        if (p instanceof Usuario)
            pstmt = Banco.con.geraStatement("delete from Usuario where usr_login = '" + ((Usuario) p).getLogin() + "'");
        else
            pstmt = Banco.con.geraStatement("delete from Cliente where cli_rg = '" + p.getRG() + "'");
        setMsg(Banco.con.manipular(pstmt), "Exclusão");

        return flag;
    }

    public ArrayList<Pessoa> get(String filtro, int op, int tipo)
    {
        String sql = null;
        ArrayList<Pessoa> lista = new ArrayList<>();
        ResultSet rs;
        try
        {
            if (tipo == 1)
            {
                sql = "select * from Usuario";
                if (!filtro.isEmpty())
                    sql += " where upper(usr_nome) like upper('%" + filtro + "%') or upper(usr_login) like upper('%" + filtro + "%')";

                pstmt = Banco.con.geraStatement(sql);
                rs = Banco.con.consultar(pstmt);

                while (rs.next())
                {
                    CtrNivel cn = new CtrNivel();
                    lista.add(new Usuario(rs.getDouble("usr_salario"), cn.get(rs.getString("nivel_cod")).get(0), rs.getInt("usr_senha"), rs.getString("usr_nome"), rs.getString("usr_CPF"), rs.getString("usr_RG"), rs.getString("usr_telefone"), rs.getString("end_cep"), rs.getString("usr_email"), rs.getString("usr_Obs"), rs.getString("usr_login"), Imagem.ByteArrayToBufferedImage(rs.getBytes("usr_foto"))));
                }

                setMsg(true, "Consulta");
            } else
            {
                sql = "select * from Cliente";
                if (!filtro.isEmpty())
                    sql += " where upper(cli_nome) like upper('" + filtro + "%') or upper(cli_cpf) like upper('%" + filtro + "%')";

                pstmt = Banco.con.geraStatement(sql);
                rs = Banco.con.consultar(pstmt);

                while (rs.next())
                {
                    lista.add(new Cliente(rs.getString("cli_nome"), rs.getString("cli_CPF"), rs.getString("cli_RG"), rs.getString("cli_telefone"), rs.getString("end_cep"), rs.getString("cli_email"), rs.getString("cli_Obs")));
                }

                setMsg(true, "Consulta");
            }
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex, "Erro Na Consulta");
        }
        return lista;
    }

    @Override
    public void update(Object Individuo,Object Objeto,Object msg) {
        if(msg instanceof String){
            CtrMensagem cm = new CtrMensagem();
            cm.Salvar(Individuo, Objeto, (String) msg);
        }
    }

}
