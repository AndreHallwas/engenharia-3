/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Dao;

import Pessoa.Entidade.Nivel;
import Padroes.Dao.ControledeEntidade;
import Utils.Banco;
import Utils.Mensagem;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class CtrNivel extends ControledeEntidade
{
    
    public CtrNivel()
    {
        flag = false;
        pstmt = null;
    }

    public boolean Salvar(Nivel n)
    {
        pstmt = Banco.con.geraStatement(
                "insert into nivel(nivel_nome, nivel_permissao,nivel_visivel) values(?,?,?)");

        setParametros(n);
        setMsg(Banco.con.manipular(pstmt), "Cadastro");

        return flag;
    }

    public boolean Alterar(Nivel n)
    {
        pstmt = Banco.con.geraStatement("update nivel set nivel_nome = ?,"
                + " nivel_permissao = ?,nivel_visivel = ? where nivel_cod = '" + n.getCodigo() + "'");

        setParametros(n);
        setMsg(Banco.con.manipular(pstmt), "Alteração");

        return flag;
    }

    public boolean Apagar(Nivel n)
    {
        pstmt = Banco.con.geraStatement("delete from nivel where nivel_cod = '" + n.getCodigo() + "'");

        setMsg(Banco.con.manipular(pstmt), "Exclusão");

        return flag;
    }
    
    private void setParametros(Nivel n)
    {
        try
        {
            pstmt.setString(1, n.getNome());
            pstmt.setString(2, n.getPermissao());
            pstmt.setObject(3, n.getVisivel());
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }
    
    public ArrayList<Nivel> get(String filtro)
    {
        String sql = null;
        ArrayList<Nivel> n = new ArrayList<>();
        ResultSet rs;
        sql = "select * from nivel where upper(nivel_nome) like upper('" + filtro + "%')"
                + " or nivel_permissao = '" + filtro + "' or nivel_visivel = '" + filtro + "'";
        sql += getCodigo("nivel_cod", filtro);

        rs = Banco.con.consultar(sql);
        try
        {
            while (rs.next())
            {
                n.add(new Nivel(rs.getString("nivel_cod"), rs.getString("nivel_nome"), rs.getString("nivel_permissao"), ((String) rs.getObject("nivel_visivel")).charAt(0)));
            }
        } catch (Exception ex)
        {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }

        return n;
    }
    
}
