/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Transacao.Dal.CtrCompra;
import farmacia.entidades.Compra;
import farmacia.entidades.Pagamento;
import farmacia.entidades.Fornecedor;
import farmacia.entidades.Usuario;
import farmacia.util.Banco;
import java.sql.ResultSet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;

/**
 *
 * @author Drake
 */
public class CtrPagamento
{

    public boolean Salvar(Pagamento p)
    {
        Alert a;
        boolean flag;
        String sql = null;

        sql = "insert into Pagamento(com_cod, for_cnpj, usr_login, pag_valor, pag_data)"
                + " values('$1', '$2', '$3', '$4', '$5')";
        sql = sql.replace("$1", p.getCompra().getCodigo());
        sql = sql.replace("$2", p.getFornecedor().getCNPJ());
        sql = sql.replace("$3", p.getUsuario().getLogin());
        sql = sql.replace("$4", Double.toString(p.getPag_valor()));
        sql = sql.replace("$5", p.getData() == null ? "" : p.getData());

        if (Banco.con.manipular(sql))
            flag = true;
        else
            flag = false;
        return flag;
    }

    public boolean Alterar(Pagamento p)
    {
        String sql = null;

        sql = "update Pagamento set com_cod = '$1', for_cnpj = '$2', usr_login = '$3', pag_valor = '$4', pag_data = '$5' where pag_cod = '" + p.getCodigo() + "'";
        sql = sql.replace("$1", p.getCompra().getCodigo());
        sql = sql.replace("$2", p.getFornecedor().getCNPJ());
        sql = sql.replace("$3", p.getUsuario().getLogin());
        sql = sql.replace("$4", Double.toString(p.getPag_valor()));
        sql = sql.replace("$5", p.getData());

        return Banco.con.manipular(sql);
    }

    public void Apagar(Pagamento p)
    {
        String sql = "delete from Pagamento where pag_cod = '" + p.getCodigo() + "'";

        Alert a;
        if (Banco.con.manipular(sql))
        {
            a = new Alert(Alert.AlertType.INFORMATION);
            a.setContentText("Operação Efetuada com Sucesso");
        } else
        {
            a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Operação Não Efetuada");
        }
        a.showAndWait();
    }

    public ObservableList<Pagamento> getPagamento(String filtro, int op)
    {
        String sql = null;
        ObservableList<Pagamento> lista = FXCollections.observableArrayList();
        ResultSet rs;
        sql = "select * from Pagamento";
        if (!filtro.isEmpty())
        {
            Banco.conectar();
            if (op == 1)
                sql += " where com_cod = '" + filtro + "' and pag_data is null or pag_data = ''";
            else if (op == 2)
                sql += " where pag_cod = '" + filtro + "' and pag_data is null or pag_data = ''";
            else if (op == 3)
                sql += " where com_cod = '" + filtro + "' and pag_data is not null and pag_data <> ''";
            else if (op == 4)
                sql += " where com_cod = '" + Integer.parseInt(filtro) + "' or pag_cod = '" + Integer.parseInt(filtro) + "' or pag_data like '%" + filtro + "%'";
            try
            {
                rs = Banco.con.consultar(sql);
                while (rs.next())
                {
                    CtrFornecedor cf = new CtrFornecedor();
                    CtrPessoa cp = new CtrPessoa();
                    CtrCompra cc = new CtrCompra();
                    Fornecedor fornecedor = cf.BuscaFornecedorCodigo(rs.getString("for_cnpj"));
                    Usuario usuario = cp.BuscaUsuarioCodigo(rs.getString("usr_login"));
                    Compra compra = cc.BuscaCompraCodigo(rs.getString("com_cod"));
                    lista.add(new Pagamento(rs.getString("pag_cod"), compra, fornecedor, usuario, rs.getDouble("pag_valor"), rs.getString("pag_data")));
                }
            } catch (Exception ex)
            {
                System.out.println("Erro: " + ex.getMessage());
            }
            return lista;
        }
        return null;
    }

    public Pagamento BuscaPagamentoCodigo(String Codigo)
    {
        String sql = null;
        Pagamento p = null;
        ResultSet rs;
        sql = "select * from Pagamento";
        if (!Codigo.isEmpty())
            sql += " where pag_cod = '" + Codigo + "'";
        Banco.conectar();
        rs = Banco.con.consultar(sql);
        try
        {
            if (rs.next())
            {
                CtrFornecedor cf = new CtrFornecedor();
                CtrPessoa cp = new CtrPessoa();
                CtrCompra cc = new CtrCompra();
                Fornecedor fornecedor = cf.BuscaFornecedorCodigo(rs.getString("for_cnpj"));
                Usuario usuario = cp.BuscaUsuarioCodigo(rs.getString("usr_login"));
                Compra compra = cc.BuscaCompraCodigo(rs.getString("com_cod"));
                p = new Pagamento(rs.getString("pag_cod"), compra, fornecedor, usuario, rs.getDouble("pag_valor"), rs.getString("pag_data"));
            }
        } catch (Exception ex)
        {
            System.out.println("Erro: " + ex.getMessage());
        }
        return p;
    }
}
