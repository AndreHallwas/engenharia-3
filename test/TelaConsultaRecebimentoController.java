/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import OldTransacao.Recebimento;
import OldTransacao.CtrRecebimento;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaConsultaRecebimentoController implements Initializable
{

    @FXML
    private TextField txBusca;
    @FXML
    private TableView<Recebimento> tabela;
    @FXML
    private TableColumn<Recebimento, String> tcCodigo;
    @FXML
    private TableColumn<Recebimento, String> tcVenda;
    @FXML
    private TableColumn<Recebimento, String> tcCliente;
    @FXML
    private TableColumn<Recebimento, String> tcValor;
    @FXML
    private TableColumn<Recebimento, String> tcData;
    public static Recebimento recebimento;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcCliente.setCellValueFactory(new PropertyValueFactory("clienteC"));
        tcCodigo.setCellValueFactory(new PropertyValueFactory("Codigo"));
        tcData.setCellValueFactory(new PropertyValueFactory("Data"));
        tcValor.setCellValueFactory(new PropertyValueFactory("Valor"));
        tcVenda.setCellValueFactory(new PropertyValueFactory("VendaC"));
        CarregaTabela("");
        produto = null;
    }

    @FXML
    private void evtBuscar()
    {
        CarregaTabela(txBusca.getText());
    }

    private void CarregaTabela(String filtro)
    {
        CtrRecebimento ctm = new CtrRecebimento();
        try
        {
            tabela.setItems(ctm.getRecebimento(filtro, 1));
        } catch (Exception ex)
        {

        }
    }

    @FXML
    private void ClicknaTabela(MouseEvent event)
    {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        recebimento = tabela.getItems().get(lin);

    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        if (recebimento != null)
            evtCancelar(event);

    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        Stage stage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
        stage.close();/////fexa janela
    }

}
