/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transacao.Dal;

import Padroes.Dao.ControledeEntidade;
import Transacao.Entidade.Transacao;
import Transacao.Entidade.ProdutoDaVenda;
import Produto.Controladora.CtrlLote;
import Produto.Controladora.CtrlProduto;
import OldTransacao.CtrlVenda;
import Produto.Entidade.Lote;
import Produto.Entidade.Produto;
import OldTransacao.Venda;
import Utils.Banco;
import Utils.Mensagem;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class CtrProdutosDaVenda extends ControledeEntidade{
    public boolean Salvar(ProdutoDaVenda l)
    {
        pstmt = Banco.con.geraStatement(
                "insert into produtos_venda(venda_cod,lot_cod,prod_cod,produtos_quant)"
                        + " values(?,?,?,?)");
        setParametrosProdutoDaVenda(l);
        setMsg(Banco.con.manipular(pstmt), "Cadastro");

        return flag;
    }
    
    public boolean Alterar(ProdutoDaVenda l)
    {
      
        pstmt = Banco.con.geraStatement("update Produtos_Venda set venda_cod = ?, lot_cod = ?, "
                + "prod_cod = ?, produtos_quant = ? where venda_cod = '" + ((ProdutoDaVenda)l).getVenda().getCodigo() + "'");
        setParametrosProdutoDaVenda(l);
        
        setMsg(Banco.con.manipular(pstmt), "Alteração");

        return flag;
    }
    
    public boolean Apagar(ProdutoDaVenda l)
    {
        pstmt = Banco.con.geraStatement("delete from Produtos_Venda where venda_cod = '" + l.getQuantidade() + "'");
        setMsg(Banco.con.manipular(pstmt), "Exclusão");

        return flag;
    }
    
    private void setParametrosProdutoDaVenda(ProdutoDaVenda p)
    {
        int count = 0;
        try
        {
            pstmt.setInt(++count, Integer.parseInt(p.getVenda().getCodigo()));
            pstmt.setInt(++count, Integer.parseInt(((Lote)p.getLote()).getCodigo()));
            pstmt.setInt(++count, Integer.parseInt(((Produto)p.getProduto()).getCodigo()));
            pstmt.setInt(++count, Integer.parseInt(p.getQuantidade()));
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }
    
    public ArrayList<ProdutoDaVenda> get(String filtro, int op)
    {
        String sql = null;
        ArrayList<ProdutoDaVenda> lista = new ArrayList();
        ResultSet rs;
        try
        {
            sql = "select * from Produtos_Venda where prod_cod = '"+filtro+"' ";
            getCodigo("venda_cod", filtro);
            getCodigo("lot_cod", filtro);

            pstmt = Banco.con.geraStatement(sql);
            rs = Banco.con.consultar(pstmt);

            while(rs.next()){
                CtrlVenda cv = CtrlVenda.create();
                CtrlLote cl = CtrlLote.create();
                CtrlProduto cp = CtrlProduto.create();
                lista.add(new ProdutoDaVenda((Transacao) cv.Pesquisar(rs.getString("venda_cod")).get(0), 
                        (Lote) cl.Pesquisar(rs.getString("lot_nome"), 3).get(0), 
                        (Produto) cp.Pesquisar(rs.getString("prod_cod")).get(0), 
                        rs.getString("produtos_quant")));
            }
            setMsg(true, "Consulta");
                
        } catch (Exception ex)
        {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }
        return lista;
    }
    
}
