/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transacao.Controladora;

import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public abstract class CtrlMovimentacao {
    
    public abstract boolean Gerar(Object... Params);
    public abstract boolean Concretizar(Object... Params);
    public abstract ArrayList<Object> Pesquisar(String filtro);
}
