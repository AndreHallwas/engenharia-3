/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transacao.Controladora;

import Transacao.Entidade.Transacao;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public abstract class CtrlTransacao {

    protected Transacao transacao;
    
    public abstract boolean cancelar(String Vendacod);
    public abstract ArrayList<Object> Pesquisar(String filtro);
    public abstract boolean registar(Object... Params);
}
