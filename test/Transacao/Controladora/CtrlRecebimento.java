package Transacao.Controladora;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Movimentacao.Controladora.CtrlMovimentacao;
import Movimentacao.Controladora.CtrlRecebimento;
import Movimentacao.Dal.CtrRecebimento;
import Movimentacao.Entidade.Recebimento;
import OldTransacao.Venda;
import java.util.ArrayList;

/**
 *
 * @author Drake
 */
public class CtrlRecebimento extends CtrlMovimentacao {

    public static CtrlRecebimento create() {
        return new CtrlRecebimento();
    }

    private CtrRecebimento cr;

    private CtrlRecebimento() {
    }

    @Override
    public ArrayList<Object> Pesquisar(String filtro) {
        cr = new CtrRecebimento();
        ArrayList<Object> Recebimentos = new ArrayList();
        Recebimentos.addAll(cr.get(filtro, 4));
        return Recebimentos;
    }
    
    /*
    *@Params Object venda, double Valor;
    */
    @Override
    public boolean Gerar(Object... Params) {
        Double Valor = 0.00;
        Object Venda = null;
        if (Params != null) {
            if (Params[0] != null) {
                Valor = (Double) Params[0];
            }
            if (Params[1] != null) {
                Venda = Params[1];
            }
        }
        Recebimento recebimento = new Recebimento(Valor, Venda);
        return recebimento.RealizarMovimentacao();
    }
    
    /*
    *@Params String Codigo, String venda, double Valor, String Data;
    */
    @Override
    public boolean Concretizar(Object... Params) {
        String Codigo = "";
        String venda = "";
        double Valor = 0.00;
        String Data = "";

        if (Params != null) {
            if (Params[0] != null) {
                Codigo = (String) Params[0];
            }
            if (Params[1] != null) {
                venda = (String) Params[1];
            }
            if (Params[2] != null) {
                Valor = (double) Params[2];
            }
            if (Params[3] != null) {
                Data = (String) Params[3];
            }
        }

        CtrlVenda cv = CtrlVenda.create();
        Venda ven = (Venda) cv.Pesquisar(venda).get(0);
        cr = new CtrRecebimento();
        return cr.Alterar(new Recebimento(Codigo, ven, Valor, Data));
    }
}
