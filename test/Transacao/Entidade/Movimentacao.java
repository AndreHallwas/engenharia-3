package Transacao.Entidade;

/*
* Padrões Utilizados.
* Strategy
* Bridge(Transacao, Movimentacao)
*/
public interface Movimentacao{

    public abstract boolean RealizarMovimentacao();

}
