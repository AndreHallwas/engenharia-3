package Transacao.Entidade;

import Movimentacao.Entidade.Recebimento;
import Movimentacao.Entidade.Movimentacao;
import Transacao.Dal.CtrProdutosDaVenda;
import Transacao.Dal.CtrVenda;
import Produto.Controladora.CtrlLote;
import Pessoa.Controladora.CtrlPessoa;
import Pessoa.Entidade.Cliente;
import Produto.Entidade.Lote;
import Produto.Entidade.Produto;
import Pessoa.Entidade.Usuario;
import Interface.Funcional.TelaVenda;
import Utils.Banco;
import Utils.Carrinho;
import java.util.ArrayList;

public class Venda extends Transacao {

    private double Valor;
    private Usuario usuario = null;
    private Cliente cliente = null;
    private Carrinho carrinho;
    private int Parcelas;

    public Venda(String Codigo) {
        this.Codigo = Codigo;
    }

    public Venda(String Codigo, Cliente cliente, Usuario usuario, double Valor) {
        this.Codigo = Codigo;
        this.usuario = usuario;
        this.cliente = cliente;
        this.Valor = Valor;
    }

    public Venda() {
        
    }

    public Venda(String usuario, String cliente, String Parcelas, Carrinho carrinho) {
        this.Parcelas = Integer.parseInt(Parcelas);
        this.carrinho = carrinho;
        IniciaTransacao(usuario, cliente);
    }

    @Override
    public boolean OperacaoDaMovimentacao(Movimentacao movimentacao) {
        mergeResultado(movimentacao.RealizarMovimentacao());
        return isResultado();
    }

    @Override
    public boolean OperacaoItensDaTransacao(ProdutoDaTransacao produtoDaTransacao) {
        CtrlLote CLote = CtrlLote.create();
        Lote lote = (Lote) ((ProdutoDaVenda) produtoDaTransacao).getLote();
        Produto produto = (Produto) ((ProdutoDaVenda) produtoDaTransacao).getProduto();
        CtrProdutosDaVenda CProdutosdaVenda = new CtrProdutosDaVenda();
        mergeResultado(CProdutosdaVenda.Salvar((ProdutoDaVenda) produtoDaTransacao));
        lote.setEstoque(lote.getEstoque() - Integer.parseInt(((ProdutoDaVenda) produtoDaTransacao).getQuantidade()));
        mergeResultado(CLote.Alterar(lote.getCodigo(), lote.getEstoqueInicial(), lote.getEstoque(),
                lote.getValidade(), produto.getCodigo()));
        return isResultado();
    }

    @Override
    protected boolean RegistraTransacao() {
        CtrVenda cv = new CtrVenda();
        setValor(getCarrinho().getValorTotal());
        mergeResultado(cv.Salvar(this));
        setCodigo(Banco.con.getMaxPK("venda", "venda_cod") + "");
        return isResultado();
    }

    @Override
    protected boolean IniciaTransacao(Object... Params) {
        CtrlPessoa cp = CtrlPessoa.create();
        if (Params != null) {
            if (Params[0] != null) {
                String Usuario = (String) Params[0];
                setUsuario((Usuario) cp.Pesquisar(Usuario, 1).get(0));
                mergeResultado(true);
            }
            if (Params[1] != null) {
                String Cliente = (String) Params[1];
                setCliente((Cliente) cp.Pesquisar(Cliente, 1).get(0));
                mergeResultado(true);
            }
        } else {
            mergeResultado(false);
        }
        return isResultado();
    }

    @Override
    protected boolean GeraMovimentacao() {
        movimentacao = new Movimentacao[getParcelas()];
        for (int i = 0; i < getParcelas(); i++) {
            movimentacao[i] = new Recebimento(getValor(), this);
        }
        return isResultado();
    }

    @Override
    protected boolean GeraProdutodaTransacao() {
        ArrayList<TelaVenda> tv = getCarrinho().geraLotes();
        produtosTransacao = new ProdutoDaTransacao[tv.size()];
        for (int i = 0; i < tv.size(); i++) {
            produtosTransacao[i] = new ProdutoDaVenda(this, tv.get(i).getLote(), tv.get(i).getProduto(), tv.get(i).getEstoque());
        }
        return true;
    }

    @Override
    protected boolean OperacaoDeCancelamento() {
        CtrVenda cv = new CtrVenda();
        return cv.Apagar(this);
    }

    /**
     * @return the Valor
     */
    public double getValor() {
        return Valor;
    }

    /**
     * @param Valor the Valor to set
     */
    public void setValor(double Valor) {
        this.Valor = Valor;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the carrinho
     */
    public Carrinho getCarrinho() {
        return carrinho;
    }

    /**
     * @param carrinho the carrinho to set
     */
    public void setCarrinho(Carrinho carrinho) {
        this.carrinho = carrinho;
    }

    /**
     * @return the Parcelas
     */
    public int getParcelas() {
        return Parcelas;
    }

    /**
     * @param Parcelas the Parcelas to set
     */
    public void setParcelas(int Parcelas) {
        this.Parcelas = Parcelas;
    }

}
