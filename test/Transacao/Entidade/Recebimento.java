package Transacao.Entidade;

import Movimentacao.Entidade.Movimentacao;
import Movimentacao.Dal.CtrRecebimento;
import java.time.LocalDate;

public class Recebimento implements Movimentacao {
    
    private String Codigo;
    private double Valor;
    private Object venda;
    private String Data;
    
    @Override
    public boolean RealizarMovimentacao() {
        CtrRecebimento cr = new CtrRecebimento();
        return cr.Salvar(this);
    }
    
    public void geraValorParcela(double ValorTotal, int Parcelas){
        setValor(ValorTotal/Parcelas);
    }

    public Recebimento() {
        
    }

    public Recebimento(String Codigo, Object venda, double Valor, String Data) {
        this.Codigo = Codigo;
        this.Valor = Valor;
        this.venda = venda;
        this.Data = Data;
    }

    public Recebimento(double Valor, Object venda) {
        this.Valor = Valor;
        this.venda = venda;
        this.Data = LocalDate.now().toString();
    }

    /**
     * @return the Codigo
     */
    public String getCodigo() {
        return Codigo;
    }

    /**
     * @param Codigo the Codigo to set
     */
    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    /**
     * @return the Valor
     */
    public double getValor() {
        return Valor;
    }

    /**
     * @param Valor the Valor to set
     */
    public void setValor(double Valor) {
        this.Valor = Valor;
    }

    /**
     * @return the venda
     */
    public Object getVenda() {
        return venda;
    }

    /**
     * @param venda the venda to set
     */
    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    /**
     * @return the Data
     */
    public String getData() {
        return Data;
    }

    /**
     * @param Data the Data to set
     */
    public void setData(String Data) {
        this.Data = Data;
    }

}
