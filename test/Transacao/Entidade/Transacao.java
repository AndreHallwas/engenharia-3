package Transacao.Entidade;

import Movimentacao.Entidade.Movimentacao;
import Transacao.Entidade.ProdutoDaTransacao;
import java.util.ArrayList;

/*
* Padrões Utilizados.
* Template
* Bridge(Transacao, Movimentacao)
 */
public abstract class Transacao {

    protected Movimentacao[] movimentacao;
    protected ProdutoDaTransacao[] produtosTransacao;
    private boolean Resultado = true;
    protected String Codigo;

    public final void RealizarTransacao() {
        RegistraTransacao();
        GeraMovimentacao();
        GeraProdutodaTransacao();
        for (ProdutoDaTransacao produtoDaTransacao : produtosTransacao) {
            OperacaoItensDaTransacao(produtoDaTransacao);
        }
        for (Movimentacao mov : movimentacao) {
            OperacaoDaMovimentacao(mov);
        }

    }

    public final void CancelarTransacao(String Codigo) {
        setCodigo(Codigo);
        OperacaoDeCancelamento();
    }

    protected abstract boolean OperacaoDaMovimentacao(Movimentacao movimentacao);

    protected abstract boolean OperacaoItensDaTransacao(ProdutoDaTransacao produtoDaTransacao);

    protected abstract boolean GeraMovimentacao();

    protected abstract boolean GeraProdutodaTransacao();

    protected abstract boolean RegistraTransacao();

    protected abstract boolean IniciaTransacao(Object... Params);

    protected abstract boolean OperacaoDeCancelamento();

    protected void mergeResultado(boolean Resultado) {
        this.setResultado(this.isResultado() && Resultado);
    }

    /**
     * @return the Codigo
     */
    public String getCodigo() {
        return Codigo;
    }

    /**
     * @param Codigo the Codigo to set
     */
    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    /**
     * @return the Resultado
     */
    public boolean isResultado() {
        return Resultado;
    }

    /**
     * @param Resultado the Resultado to set
     */
    public void setResultado(boolean Resultado) {
        this.Resultado = Resultado;
    }

}
