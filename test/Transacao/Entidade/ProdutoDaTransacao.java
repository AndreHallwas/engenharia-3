/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transacao.Entidade;

import Produto.Entidade.Produto;

/**
 *
 * @author Raizen
 */
public abstract class ProdutoDaTransacao {
    protected Object produto;
    protected Transacao transacao;

    public ProdutoDaTransacao() {
    }

    public ProdutoDaTransacao(Object produto, Transacao transacao) {
        this.produto = produto;
        this.transacao = transacao;
    }

    /**
     * @return the produto
     */
    public Object getProduto() {
        return produto;
    }

    /**
     * @param produto the produto to set
     */
    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    /**
     * @return the transacao
     */
    public Transacao getTransacao() {
        return transacao;
    }

    /**
     * @param transacao the transacao to set
     */
    public void setTransacao(Transacao transacao) {
        this.transacao = transacao;
    }
    
}
