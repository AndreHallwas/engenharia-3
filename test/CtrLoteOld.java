package engenharia.entidades.controle.Revisar;


import engenharia.entidades.Revisar.Lote;
import Produto.Entidade.Produto;
import Produto.Controladora.CtrlFabricante;
import Produto.Controladora.CtrlFabricante;
import Utils.Banco;
import Utils.Mensagem;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Raizen
 */
public class CtrLote
{
    private boolean flag;
    private PreparedStatement pstmt;
    private String Msg;
    
    public CtrLote() {
        flag = false;
        pstmt = null;
    }
    
    public boolean Salvar(Object l)
    {
        if(l instanceof Lote){
            pstmt = Banco.con.geraStatement(
                    "insert into lote(prod_cod, lote_estoqueinicial, lote_estoque, lote_validade) values(?,?,?,?)");
            setParametrosLote((Lote) l);
        }else if(l instanceof Produto){
            pstmt = Banco.con.geraStatement(
                    "insert into produto(prod_nome,prod_obs,prod_nome_farmacologico,prod_preco_fab,prod_preco_comercial,"
                            + "fab_cnpj,medida_prod,local_num,local_desc,tipo_desc) values(?,?,?,?,?,?,?,?,?,?)");
            setParametrosProduto((Produto) l);
        }
        setMsg(Banco.con.manipular(pstmt), "Cadastro");

        return flag;
    }
    
    public boolean Alterar(Object l)
    {
        if(l instanceof Lote){
            pstmt = Banco.con.geraStatement("update lote set prod_cod = ?, lote_estoqueinicial = ?, lote_estoque = ?, "
                    + "lote_validade = ? where lot_cod = '" + ((Lote)l).getCodigo() + "'");
            setParametrosLote((Lote) l);
        }else if(l instanceof Produto){
            pstmt = Banco.con.geraStatement("update Produto set prod_nome = ?, prod_obs = ?, prod_nome_farmacologico = ?,"
                    + " prod_preco_fab = ?, prod_preco_comercial = ?, fab_cnpj = ?, medida_prod = ?, local_num = ?,"
                    + " local_desc = ?,tipo_desc = ? where prod_cod = '" + ((Produto)l).getCodigo() + "'");
            setParametrosProduto((Produto) l);
        }
        
        setMsg(Banco.con.manipular(pstmt), "Alteração");

        return flag;
    }
    
    public boolean Apagar(Object l)
    {
        if(l instanceof Lote)
            pstmt = Banco.con.geraStatement("delete from lote where lot_cod = '" + ((Lote)l).getCodigo() + "'");
        else if(l instanceof Produto)
            pstmt = Banco.con.geraStatement("delete from Produto where prod_cod = '" + ((Produto)l).getCodigo() + "'");
        setMsg(Banco.con.manipular(pstmt), "Exclusão");

        return flag;
    }
    
    private void setMsg(boolean Condition, String Op)
    {
        if (Condition)
        {
            Msg = Op + " Efetuado";
            flag = true;
        } else
            Msg = Op + " Não Efetuado Erro: " + Banco.con.getMensagemErro();
        Mensagem.ExibirLog(Msg);
    }
    
    private void setParametrosLote(Lote l)
    {
        int count = 0;
        try
        {
            pstmt.setString(++count, l.getProduto().getCodigo());
            pstmt.setInt(++count, l.getEstoqueInicial());
            pstmt.setInt(++count, l.getEstoque());
            pstmt.setString(++count, l.getValidade());
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }
    private void setParametrosProduto(Produto p)
    {
        int count = 0;
        try
        {
            pstmt.setString(++count, p.getNome());
            pstmt.setString(++count, p.getObs());
            pstmt.setString(++count, p.getNomeF());
            pstmt.setDouble(++count, p.getPrecoFab());
            pstmt.setDouble(++count, p.getPrecoCom());
            pstmt.setString(++count, p.getFabricante().getCNPJ());
            pstmt.setString(++count, p.getMedida_Prod());
            pstmt.setString(++count, p.getLocal_Num());
            pstmt.setString(++count, p.getLocal_Desc());
            pstmt.setString(++count, p.getTipo());
        } catch (SQLException ex)
        {
            Mensagem.ExibirException(ex);
        }
    }

    private String getCodigo(String Campo, String filtro)
    {
        String sql = "";
        if (!filtro.isEmpty())
            try
            {
                int aux = Integer.parseInt(0 + filtro);
                sql = sql + " or " + Campo + " = '" + aux + "'";
                return sql;
            } catch (NumberFormatException ex)
            {
                Mensagem.ExibirException(ex, "Erro no Nivel Cod");
            }
        return "";
    }

    public ObservableList<Object> get(String filtro, int op, int tipo)
    {
        String sql = null;
        ObservableList<Object> lista = FXCollections.observableArrayList();
        ResultSet rs;
        try
        {
            if(tipo == 1){
                sql = "select * from lote";
                if (!filtro.isEmpty()){
                    if (op == 1)
                        sql += " where prod_cod in (select prod_cod from produto where upper(prod_nome) like upper('" + filtro + "%')";
                    else if (op == 2)
                        sql += " inner join produtos_compra on produtos_compra.lot_cod = lote.lot_cod inner join compra on "
                                + "produtos_compra.com_cod = compra.com_cod where compra.com_cod = '" + filtro + "'";///consulta ruim
                    else{
                        sql += " where lot_cod = '" + filtro + "'";
                    }
                }

                pstmt = Banco.con.geraStatement(sql);
                rs = Banco.con.consultar(pstmt);

                while (rs.next())
                {
                    Produto produto = (Produto) get(rs.getString("prod_cod"),0,2).get(0);
                    lista.add(new Lote(rs.getString("lot_cod"), produto, rs.getInt("lote_estoqueinicial"), rs.getInt("lote_estoque"), rs.getString("lote_validade")));
                }
                setMsg(true, "Consulta");
            }else if(tipo == 2){
                sql = "select * from Produto where upper(prod_nome) like upper('" + filtro + "%')";
                getCodigo("prod_cod", filtro);
                
                pstmt = Banco.con.geraStatement(sql);
                rs = Banco.con.consultar(pstmt);
                
                while(rs.next()){
                    CtrlFabricante cf = CtrlFabricante.create();
                    lista.add(new Produto(rs.getString("prod_cod"), rs.getString("prod_nome"), rs.getString("prod_obs"), 
                            rs.getString("prod_nome_farmacologico"), rs.getDouble("prod_preco_fab"), rs.getDouble("prod_preco_comercial"), 
                            cf.Pesquisar(rs.getString("fab_cnpj")).get(0),rs.getString("tipo_desc"),rs.getString("local_desc"),rs.getString("local_num"),
                            rs.getString("medida_prod")));
                }
                setMsg(true, "Consulta");
            }
        } catch (Exception ex)
        {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }
        return lista;
    }
}
