/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Revisado;

import Pessoa.Controladora.CtrlPessoa;
import Fornecedor.Controladora.CtrlFornecedor;
import Produto.Controladora.CtrlLote;
import Pessoa.Entidade.Pessoa;
import OldTransacao.Compra;
import Fornecedor.Entidade.Fornecedor;
import Produto.Entidade.Lote;
import Pessoa.Entidade.Usuario;
import Produto.Entidade.Produto;
import Interface.Consulta.TelaConsultaFornecedorController;
import Interface.Gerenciamento.TelaConsultaProdutoController;
import Utils.Carrinho;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCompraController implements Initializable
{

    private TextField txbusca;
    @FXML
    private TableView<Lote> tbLote;
    @FXML
    private Button btnCompra;
    @FXML
    private Button btnCancelar;
    @FXML
    private TextField txbUsuario;
    @FXML
    private TextField txbFornecedor;
    @FXML
    private TextField txbProduto;
    @FXML
    private TextField txbLote;
    @FXML
    private TextField txbQuantidade;
    @FXML
    private HBox pndados;
    @FXML
    private Button btnConfirma;
    @FXML
    private TableColumn<Lote, String> tcLote;
    @FXML
    private TableColumn<Lote, String> tcProduto;
    @FXML
    private TableColumn<Lote, String> tcQuantidade;
    @FXML
    private Button btnAdicionarCarrinho;
    @FXML
    private Button btnAlterarCarrinho;
    @FXML
    private Button btnExcluirCarrinho;
    private int linLote;
    @FXML
    private TextField txbPreco;
    @FXML
    private TextArea txbObs;
    @FXML
    private TextField txbValorTotal;
    @FXML
    private VBox pndados2;
    private Compra compra;
    @FXML
    private TextField txbParcelas;
    private int flag;
    private String fornec;
    private Fornecedor fornecedor;
    private Produto produto;
    private Usuario usuario;
    private Carrinho carrinho;
    @FXML
    private TextField txbValidade;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcLote.setCellValueFactory(new PropertyValueFactory("Codigo"));
        tcProduto.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcQuantidade.setCellValueFactory(new PropertyValueFactory("Estoque"));

        MaskFieldUtil.monetaryField(txbPreco);
        MaskFieldUtil.monetaryField(txbValorTotal);
        MaskFieldUtil.numericField(txbParcelas);
        flag = 1;
        txbParcelas.setText("1");

        estadoOriginal();
        CarregaTabela("");
    }

    private void evtBuscar(ActionEvent event)
    {
        CarregaTabela(txbusca.getText());
    }

    private void CarregaTabela(String filtro)
    {
        CarregaTabelaLotes();
    }

    private void CarregaTabelaLotes()
    {
        CtrlLote ctm = CtrlLote.create();
        try
        {
            tbLote.setItems(FXCollections.observableList(carrinho.geraLotes()));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Erro Ao CarreGar Lotes");
        }
    }

    @FXML
    private void evtAdicionarCarrinho(ActionEvent event)
    {
        EstadoEdicao();
        flag = 1;
        txbLote.setText(Integer.toString(carrinho.getLength()));
        CarregaTabela("");
    }

    @FXML
    private void evtAlterarCarrinho(ActionEvent event)
    {
        flag = 0;
        EstadoEdicao();
    }

    @FXML
    private void evtComprar(ActionEvent event)
    {
        /*CtrlCompra cc = new CtrlCompra();
        if(cc.registrarCompra(txbUsuario.getText(), fornecedor, carrinho.geraLotes(), txbParcelas.getText())){
            carrinho.clear();
            txbParcelas.setText("1");
            estadoOriginal();
        }*/
        CarregaTabela("");
    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        estadoOriginal();
    }

    private void setCampos(Lote l)
    {
        if (l != null)
        {
            txbQuantidade.setText(Integer.toString(l.getEstoque()));
            txbProduto.setText(l.getProduto().getNome());
            txbLote.setText(l.getCodigo());
            txbPreco.setText(Double.toString(l.getProduto().getPrecoCom()));
            txbObs.setText(l.getProduto().getObs());
            txbValorTotal.setText(Double.toString(carrinho.getValorTotal()));
        }
    }

    public void setButtomCarrinho(boolean alterarC, boolean excluirC, boolean adicionarC)
    {
        btnAlterarCarrinho.setDisable(alterarC);
        btnExcluirCarrinho.setDisable(excluirC);
        btnAdicionarCarrinho.setDisable(adicionarC);
    }

    public void setButtom(boolean Bcancelar, boolean Bcompra, boolean Bconfirma)
    {
        btnCancelar.setDisable(Bcancelar);
        btnCompra.setDisable(Bcompra);
        btnConfirma.setDisable(Bconfirma);
    }

    @FXML
    private void ClicknaTabelaLotes(MouseEvent event)
    {
        int lin = tbLote.getSelectionModel().getSelectedIndex();
        if (lin > -1)
        {
            Lote l = tbLote.getItems().get(lin);
            setCampos(l);

            setButtomCarrinho(false, false, true);
            setButtom(false, false, false);

            flag = 0;
        }
    }

    @FXML
    private void evtExcluirCarrinho(ActionEvent event)
    {
        carrinho.remove(fornec);

        CarregaTabela("");
        estadoOriginal();
    }

    public void setFornecedor()
    {
        if (fornecedor != null)
        {
            txbFornecedor.setText(fornecedor.getNome());
            txbFornecedor.setDisable(true);
        } else
            txbFornecedor.setDisable(false);
    }

    private void estadoOriginal()
    {

        pndados.setDisable(true);

        setButtomCarrinho(true, true, false);
        setButtom(false, false, true);

        txbValorTotal.setText(Double.toString(carrinho.getValorTotal()));

        setFornecedor();

        clear(pndados.getChildren());

        if (!carrinho.isEmpty())
            btnCompra.setDisable(true);
        else
            btnCompra.setDisable(false);

        txbLote.setText(Integer.toString(carrinho.getLength()));
        /////txbUsuario.setText(CtrlAcesso.getControle().getTxUsuarioInterface().getText());
        txbParcelas.setText(txbParcelas.getText().isEmpty() ? "1" : txbParcelas.getText());

        txbValorTotal.setText(Float.toString(carrinho.getValorTotal()));
        CarregaTabela("");
    }

    public void clear(ObservableList<Node> pn)
    {
        ObservableList<Node> componentes = pn; //”limpa” os componentes
        for (Node n : componentes)
        {
            if (n instanceof Pane)
                clear(((Pane) n).getChildren());
            if (n instanceof TextInputControl) // textfield, textarea e htmleditor
                ((TextInputControl) n).setText("");
            if (n instanceof ComboBox)
                ((ComboBox) n).getItems().clear();
            if (n instanceof ImageView)
                ((ImageView) n).setImage(null);
        }
    }

    public void EstadoEdicao()
    {
        pndados.setDisable(false);
        setButtom(false, true, false);
        setButtomCarrinho(true, false, true);
        txbLote.setDisable(true);
        setFornecedor();
    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        CtrlFornecedor cf = CtrlFornecedor.create();
        ArrayList<Fornecedor> fornecedores = cf.Pesquisar(fornecedor.getCNPJ());
        if (!fornecedores.isEmpty())
        {
            fornecedor = fornecedores.get(0);

            CtrlPessoa cp = CtrlPessoa.create();
            ArrayList<Pessoa> pessoas = cp.Pesquisar(txbUsuario.getText(), 1);

            if (!pessoas.isEmpty())
            {
                usuario = (Usuario) pessoas.get(0);

                if (carrinho == null)
                    carrinho = new Carrinho();
                if (Integer.parseInt(txbQuantidade.getText()) > 0)
                    carrinho.add(new Lote(null, produto, Integer.parseInt(txbQuantidade.getText()), Integer.parseInt(txbQuantidade.getText()), txbValidade.getText()));
                estadoOriginal();
            }
        }
    }

    @FXML
    private void evtConsultaProduto()
    {
        Parent root = null;
        try
        {
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/farmacia/ui/Consulta/TelaConsultaProduto.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("farmacia/ui/estilo/tema1.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.showAndWait();
            produto = TelaConsultaProdutoController.getProduto();
            if (produto != null)
            {
                txbProduto.setText(produto.getNome());
                txbPreco.setText(Double.toString(produto.getPrecoCom()));
                txbObs.setText(produto.getObs());
            }

        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao consultar produto na compra");
        }
    }

    @FXML
    private void evtConsultaFornecedor()
    {
        Parent root = null;
        try
        {
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/farmacia/ui/Consulta/TelaConsultaFornecedor.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("farmacia/ui/estilo/tema1.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.showAndWait();
            fornecedor = TelaConsultaFornecedorController.getFornecedor();
            if (fornecedor != null)
                txbFornecedor.setText(fornecedor.getNome());
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao consultar fornecedor na compra");
        }
    }

    @FXML
    private void evtCancelarCompra(ActionEvent event)
    {
        Parent root = null;
        try
        {
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/farmacia/ui/Consulta/TelaConsultaCompra.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("farmacia/ui/estilo/tema1.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.showAndWait();
            /*compra = TelaConsultaCompraController.compra;  
            if(compra != null){
                CtrlCompra cc = new CtrlCompra();
                cc.cancelarCompra(compra.getCodigo());                
            }*/
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao cancelar compra");
        }
    }

}
