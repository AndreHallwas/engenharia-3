/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import OldTransacao.CtrVenda;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Drake
 */
public class TelaConsultaVendaController implements Initializable
{

    @FXML
    private TextField txBusca;
    @FXML
    private TableView<Venda> tabela;
    @FXML
    private TableColumn<Venda, Venda> tcCodigo;
    @FXML
    private TableColumn<Venda, Venda> tcCliente;
    @FXML
    private TableColumn<Venda, Venda> tcUsuario;
    @FXML
    private TableColumn<Venda, Venda> tcValor;
    public static Venda venda;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcCliente.setCellValueFactory(new PropertyValueFactory("clienteC"));
        tcCodigo.setCellValueFactory(new PropertyValueFactory("Codigo"));
        tcUsuario.setCellValueFactory(new PropertyValueFactory("usuarioC"));
        tcValor.setCellValueFactory(new PropertyValueFactory("Valor"));
        CarregaTabela("");
        venda = null;
    }

    @FXML
    private void evtBuscar()
    {
        CarregaTabela(txBusca.getText());
    }

    private void CarregaTabela(String filtro)
    {
        CtrVenda ctm = new CtrVenda();
        try
        {
            tabela.setItems(ctm.getVenda(filtro, 1));
        } catch (Exception ex)
        {

        }
    }

    @FXML
    private void ClicknaTabela(MouseEvent event)
    {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        venda = tabela.getItems().get(lin);

    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        if (venda != null)
            evtCancelar(event);

    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        Stage stage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
        stage.close();/////fexa janela
    }

}
