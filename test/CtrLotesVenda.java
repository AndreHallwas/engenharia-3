/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Transacao.Dal.CtrCompra;
import Produto.Dao.CtrLote;
import OldTransacao.CtrVenda;
import farmacia.entidades.Cliente;
import farmacia.entidades.Compra;
import farmacia.entidades.Fornecedor;
import farmacia.entidades.Lote;
import farmacia.entidades.LotesVenda;
import farmacia.entidades.Produto;
import farmacia.entidades.Usuario;
import farmacia.entidades.Venda;
import farmacia.util.Banco;
import java.sql.ResultSet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;

/**
 *
 * @author Raizen
 */
public class CtrLotesVenda
{

    public boolean Salvar(LotesVenda l)
    {
        Alert a;
        boolean flag;
        String sql = null;

        sql = "insert into lotes_venda(lot_cod, com_cod, prod_cod, venda_cod,cli_rg,for_cnpj,usr_login,usr_obs,lotes_quant)"
                + " values('$1', '$2', '$3', '$4', '$5', '$6', '$7', '$8','$9')";
        sql = sql.replace("$1", l.getLote().getCodigo());
        sql = sql.replace("$2", l.getCompra().getCodigo());
        sql = sql.replace("$3", l.getProduto().getCodigo());
        sql = sql.replace("$4", l.getVenda().getCodigo());
        sql = sql.replace("$5", l.getCliente().getRG());
        sql = sql.replace("$6", l.getFornecedor().getCNPJ());
        sql = sql.replace("$7", l.getUsuario().getLogin());
        sql = sql.replace("$8", l.getUsuario().getObs());
        sql = sql.replace("$9", Integer.toString(l.getQuantidade()));

        if (Banco.con.manipular(sql))
        {
            a = new Alert(Alert.AlertType.INFORMATION);
            System.out.println("Cadastro Efetuado");
            flag = true;
        } else
        {
            a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Cadastro Não Concluido Erro: " + Banco.con.getMensagemErro());
            flag = false;
        }
        a.showAndWait();
        return flag;
    }

    public boolean Alterar(LotesVenda l)
    {
        String sql = null;

        sql = "update lotes_venda set lot_cod = '$1', com_cod = '$2', prod_cod = '$3', venda_cod = '$4'"
                + ", cli_rg = '$5', for_cnpj = '$6', usr_login = '$7', usr_obs = '$8', lotes_quant = '$9' where lotes_cod = '" + l.getCodigo() + "'";
        sql = sql.replace("$1", l.getLote().getCodigo());
        sql = sql.replace("$2", l.getCompra().getCodigo());
        sql = sql.replace("$3", l.getProduto().getCodigo());
        sql = sql.replace("$4", l.getVenda().getCodigo());
        sql = sql.replace("$5", l.getCliente().getRG());
        sql = sql.replace("$6", l.getFornecedor().getCNPJ());
        sql = sql.replace("$7", l.getUsuario().getLogin());
        sql = sql.replace("$8", l.getUsuario().getObs());
        sql = sql.replace("$9", Integer.toString(l.getQuantidade()));

        return Banco.con.manipular(sql);
    }

    public void Apagar(LotesVenda l)
    {
        String sql = "delete from lotes_venda where lotes_cod = '" + l.getCodigo() + "'";

        Alert a;
        if (Banco.con.manipular(sql))
        {
            a = new Alert(Alert.AlertType.INFORMATION);
            a.setContentText("Operação Efetuada com Sucesso");
        } else
        {
            a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Operação Não Efetuada");
        }
        a.showAndWait();
    }

    public ObservableList<LotesVenda> getLotesVenda(String filtro, int op)
    {
        String sql = null;
        ObservableList<LotesVenda> lista = FXCollections.observableArrayList();
        ResultSet rs;
        sql = "select * from lotes_venda";
        if (!filtro.isEmpty())
        {
            if (op == 1)
                sql += " where lot_cod = '" + filtro + "'";
            rs = Banco.con.consultar(sql);
            try
            {
                CtrLote cl = new CtrLote();
                CtrCompra cc = new CtrCompra();
                CtrVenda cv = new CtrVenda();
                CtrPessoa cp = new CtrPessoa();
                CtrFornecedor cf = new CtrFornecedor();

                Lote lote;
                Compra compra;
                Produto produto;
                Venda venda;
                Usuario usuario;
                Fornecedor fornecedor;
                Cliente cliente;

                while (rs.next())
                {
                    lote = cl.BuscaLoteCodigo(rs.getString("lot_cod"));
                    compra = cc.BuscaCompraCodigo(rs.getString("com_cod"));
                    produto = cl.BuscaProdutoCodigo(rs.getString("prod_cod"));
                    venda = cv.BuscaVendaCodigo(rs.getString("venda_cod"));
                    usuario = cp.BuscaUsuarioCodigo(rs.getString("usr_login"));
                    fornecedor = cf.BuscaFornecedorCodigo(rs.getString("for_cnpj"));
                    cliente = cp.BuscaClienteCodigo(rs.getString("cli_rg"));

                    lista.add(new LotesVenda(rs.getString("lotes_cod"), lote, compra, produto, venda, usuario, fornecedor, cliente, rs.getInt("lotes_quant")));
                }
            } catch (Exception ex)
            {
                System.out.println("Erro: " + ex.getMessage());
            }
            return lista;
        }
        return null;
    }

    public LotesVenda BuscaLotesVendaCodigo(String Codigo)
    {
        String sql = null;
        LotesVenda l = null;
        ResultSet rs;
        sql = "select * from lotes_venda";
        if (!Codigo.isEmpty())
            sql += " where lotes_cod = '" + Codigo + "'";
        rs = Banco.con.consultar(sql);
        try
        {
            CtrLote cl = new CtrLote();
            CtrCompra cc = new CtrCompra();
            CtrVenda cv = new CtrVenda();
            CtrPessoa cp = new CtrPessoa();
            CtrFornecedor cf = new CtrFornecedor();

            Lote lote;
            Compra compra;
            Produto produto;
            Venda venda;
            Usuario usuario;
            Fornecedor fornecedor;
            Cliente cliente;
            if (rs.next())
            {
                lote = cl.BuscaLoteCodigo(rs.getString("lot_cod"));
                compra = cc.BuscaCompraCodigo(rs.getString("com_cod"));
                produto = cl.BuscaProdutoCodigo(rs.getString("prod_cod"));
                venda = cv.BuscaVendaCodigo(rs.getString("venda_cod"));
                usuario = cp.BuscaUsuarioCodigo(rs.getString("usr_login"));
                fornecedor = cf.BuscaFornecedorCodigo(rs.getString("for_cnpj"));
                cliente = cp.BuscaClienteCodigo(rs.getString("cli_rg"));

                l = new LotesVenda(rs.getString("lotes_cod"), lote, compra, produto, venda, usuario, fornecedor, cliente, rs.getInt("lotes_quant"));
            }
        } catch (Exception ex)
        {
            System.out.println("Erro: " + ex.getMessage());
        }
        return l;
    }
}
