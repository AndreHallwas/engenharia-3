/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import farmacia.entidades.Pagamento;
import farmacia.entidades.controle.CtrPagamento;
import java.util.ArrayList;

/**
 *
 * @author Drake
 */
public class CtrlPagamento
{

    public ArrayList<Pagamento> Listar(String filtro)
    {
        CtrPagamento cr = new CtrPagamento();
        return (ArrayList<Pagamento>) cr.getPagamento(filtro, 1);
    }

    public void Pagar(Pagamento pagamento)
    {
        CtrPagamento cr = new CtrPagamento();
        cr.Alterar(pagamento);
    }
}
