/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import farmacia.entidades.Cliente;
import farmacia.entidades.Lote;
import farmacia.entidades.LotesVenda;
import farmacia.entidades.Recebimento;
import farmacia.entidades.Usuario;
import farmacia.entidades.Venda;
import farmacia.entidades.controle.CtrLote;
import farmacia.entidades.controle.CtrLotesVenda;
import farmacia.entidades.controle.CtrPessoa;
import farmacia.entidades.controle.CtrRecebimento;
import farmacia.entidades.controle.CtrVenda;
import farmacia.util.Banco;
import java.util.ArrayList;
import javafx.collections.ObservableList;

/**
 *
 * @author Drake
 */
public class CtrlVenda
{

    public boolean registarVenda(String usuarioCod, String clienteCod, ArrayList<LotesVenda> lv, int parcelas)
    {
        CtrVenda cv = new CtrVenda();
        CtrPessoa cp = new CtrPessoa();
        CtrRecebimento cr = new CtrRecebimento();
        double Valor = 0;
        boolean var = false;

        try
        {
            Usuario u = cp.BuscaUsuarioCodigo(usuarioCod);
            Cliente c = cp.BuscaClienteCodigo(clienteCod);
            for (int i = 0; i < lv.size(); i++)
            {
                Valor += lv.get(i).getProduto().getPrecoCom() * lv.get(i).getQuantidade();
            }
            Venda v = new Venda(null, c, u, Valor);
            cv.Salvar(v);
            String VenCod = Banco.con.getMaxPK("venda", "venda_cod") + "";
            v.setCodigo(VenCod);

            registarItensVenda(lv);

            Valor = Valor / parcelas;
            for (int i = 0; i < lv.size(); i++)
            {
                cr.Salvar(new Recebimento(null, v, c, u, Valor, null));
            }
            var = true;
        } catch (Exception ex)
        {
            System.out.println(ex);
        }
        return false;
    }

    public void registarItensVenda(ArrayList<LotesVenda> lv)
    {
        CtrLotesVenda cn = new CtrLotesVenda();
        CtrLote cl = new CtrLote();
        Lote l;

        for (int i = 0; i < lv.size(); i++)
        {
            l = cl.BuscaLoteCodigo(lv.get(i).getCodigo());
            l.setEstoque(l.getEstoque() - lv.get(i).getQuantidade());
            cl.AlterarLote(l);
            cn.Salvar(lv.get(i));
        }
    }

    public boolean cancelarVenda(String Vendacod)
    {
        CtrVenda cv = new CtrVenda();
        CtrLotesVenda cn = new CtrLotesVenda();
        CtrLote cl = new CtrLote();
        CtrRecebimento cr = new CtrRecebimento();
        Venda v;
        boolean var = false;

        try
        {
            v = cv.BuscaVendaCodigo(Vendacod);
            ObservableList<LotesVenda> lv = cn.getLotesVenda(Vendacod, 0);
            Lote l;
            for (int i = 0; i < lv.size(); i++)
            {
                l = cl.BuscaLoteCodigo(lv.get(i).getCodigo());
                l.setEstoque(l.getEstoque() + lv.get(i).getQuantidade());
                cl.AlterarLote(l);
                cn.Alterar(lv.get(i));
            }
            cr.Apagar(new Recebimento(null, v, null, null));
            cv.Apagar(v);
            var = true;
        } catch (Exception ex)
        {
            System.out.println(ex);
        }
        return var;
    }

    public void RealizarPagamento()
    {

    }

    public Venda pesquisaVenda(String filtro)
    {
        CtrVenda cv = new CtrVenda();
        return cv.getVenda(filtro, 1).get(0);
    }
}
