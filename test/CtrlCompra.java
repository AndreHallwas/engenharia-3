/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Transacao.Dal.CtrCompra;
import Produto.Dao.CtrLote;
import engenharia.entidades.Revisar.Lote;
import farmacia.entidades.*;
import farmacia.entidades.controle.*;
import farmacia.util.Banco;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javax.swing.JOptionPane;

/**
 *
 * @author Drake
 */
public class CtrlCompra
{

    public Compra pesquisaCompra(String filtro)
    {
        CtrCompra ctc = new CtrCompra();
        return ctc.getCompra(filtro, 1).get(0);

    }

    public boolean registrarCompra(String User, String For, ArrayList<Lote> LotesNovos, String Parcelas)
    {
        CtrCompra cc = new CtrCompra();
        CtrFornecedor cf = new CtrFornecedor();
        CtrPessoa cp = new CtrPessoa();
        CtrLote cl = new CtrLote();
        CtrPagamento cg = new CtrPagamento();
        double Valor = 0;
        boolean var = false;

        try
        {
            Lote l;
            Usuario u = cp.BuscaUsuario(User);
            Fornecedor f = cf.BuscaFornecedorCodigo(For);
            Compra c = new Compra(f, u, 0, LotesNovos.size());

            cc.Salvar(c);
            String ComCod = Banco.con.getMaxPK("compra", "com_cod") + "";
            c.setCodigo(ComCod);
            int i;
            for (i = 0; i < LotesNovos.size(); i++)
            {
                l = LotesNovos.get(i);
                l.setCompra(c);
                cl.SalvarLote(l);
                Valor += l.getProduto().getPrecoFab() * l.getEstoque();
            }
            Valor = Valor / Integer.parseInt(Parcelas);
            Pagamento pag;
            for (int j = 0; j < Integer.parseInt(Parcelas); j++)
            {
                pag = new Pagamento(ComCod, c, f, u, Valor);
                cg.Salvar(pag);
            }
            var = true;
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Erro ao Registrar a Compra");
        }

        return var;
    }

    public boolean cancelarCompra(String codigoCompra)
    {
        CtrCompra cc = new CtrCompra();
        CtrLote cl = new CtrLote();
        CtrPagamento cg = new CtrPagamento();
        boolean var = false;
        try
        {

            ObservableList<Lote> al = cl.getLote(codigoCompra, 2);
            ObservableList<Pagamento> ac = cg.getPagamento(codigoCompra, 1);
            int i;
            for (i = 0; i < ac.size() && ac.get(i).getData().equals(""); i++)
            {
            }
            if (!(i < ac.size()))
            {
                for (i = 0; i < ac.size(); i++)
                {
                    cg.Apagar(ac.get(i));
                }
                for (i = 0; i < al.size(); i++)
                {
                    cl.ApagarLote(al.get(i));
                }

                Compra c = cc.BuscaCompraCodigo(codigoCompra);
                cc.Apagar(c);
            } else
                JOptionPane.showMessageDialog(null, "Compra Com Parcelas Pagas");
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Erro ao Cancelar a Compra");
        }

        return var;
    }
}
