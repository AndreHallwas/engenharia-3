/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import farmacia.ui.Consulta.TelaConsultaProdutoController;
import farmacia.ui.Consulta.TelaConsultaFornecedorController;
import farmacia.controladoras.CtrlAcesso;
import farmacia.controladoras.CtrlCompra;
import farmacia.entidades.controle.CtrCompra;
import farmacia.entidades.controle.CtrFornecedor;
import farmacia.entidades.controle.CtrLote;
import farmacia.entidades.controle.CtrPagamento;
import farmacia.entidades.controle.CtrPessoa;
import farmacia.entidades.Compra;
import farmacia.entidades.Fornecedor;
import farmacia.entidades.Lote;
import farmacia.entidades.Pagamento;
import farmacia.entidades.Pessoa;
import farmacia.entidades.Produto;
import farmacia.entidades.Usuario;
import farmacia.ui.Consulta.TelaConsultaCompraController;
import farmacia.ui.Consulta.TelaConsultaVendaController;
import farmacia.ui.MainFXMLController;
import static farmacia.ui.MainFXMLController._login;
import farmacia.util.Banco;
import farmacia.util.MaskFieldUtil;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCompraController implements Initializable
{

    private TextField txbusca;
    @FXML
    private TableView<Lote> tbLote;
    @FXML
    private Button btnCompra;
    @FXML
    private Button btnCancelar;
    @FXML
    private TextField txbUsuario;
    @FXML
    private TextField txbFornecedor;
    @FXML
    private TextField txbProduto;
    @FXML
    private TextField txbLote;
    @FXML
    private TextField txbQuantidade;
    private int Count;
    @FXML
    private AnchorPane pndados;
    @FXML
    private Button btnConfirma;
    @FXML
    private TableColumn<Lote, String> tcLote;
    @FXML
    private TableColumn<Lote, String> tcProduto;
    @FXML
    private TableColumn<Lote, String> tcQuantidade;
    private ArrayList<Lote> LotesNovos;
    private int Quant;
    private Lote index;
    private int flag;
    private String fornec;
    private int fornecf;
    private Fornecedor fornecedor;
    private Produto produto;
    @FXML
    private Button btnAdicionarCarrinho;
    @FXML
    private Button btnAlterarCarrinho;
    @FXML
    private Button btnExcluirCarrinho;
    private int linLote;
    @FXML
    private TextField txbPreco;
    @FXML
    private TextArea txbObs;
    @FXML
    private TextField txbValorTotal;
    private double ValorTotal;
    @FXML
    private VBox pndados1;
    @FXML
    private VBox pndados2;
    private Compra compra;
    @FXML
    private TextField txbParcelas;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcLote.setCellValueFactory(new PropertyValueFactory("Codigo"));
        tcProduto.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcQuantidade.setCellValueFactory(new PropertyValueFactory("Estoque"));
        MaskFieldUtil.monetaryField(txbPreco);
        MaskFieldUtil.monetaryField(txbValorTotal);
        MaskFieldUtil.numericField(txbParcelas);
        LotesNovos = new ArrayList<Lote>();
        Quant = 0;
        Count = 0;
        flag = 1;
        fornecf = 0;
        linLote = -1;
        ValorTotal = 0;
        txbParcelas.setText("1");
        estadoOriginal();
        CarregaTabela("");
    }

    private void evtBuscar(ActionEvent event)
    {
        CarregaTabela(txbusca.getText());
    }

    private void CarregaTabela(String filtro)
    {
        CarregaTabelaLotes();
    }

    private void CarregaTabelaLotes()
    {
        CtrLote ctm = new CtrLote();
        try
        {
            tbLote.setItems(FXCollections.observableList(LotesNovos));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Erro Ao CarreGar Lotes");
        }
    }

    @FXML
    private void evtAdicionarCarrinho(ActionEvent event)
    {
        EstadoEdicao();
        flag = 1;
        txbLote.setText(Integer.toString(Count));
        CarregaTabela("");
    }

    @FXML
    private void evtAlterarCarrinho(ActionEvent event)
    {
        flag = 0;
        EstadoEdicao();
    }

    @FXML
    private void evtComprar(ActionEvent event)
    {
        CtrlCompra cc = new CtrlCompra();
        if (cc.registrarCompra(txbUsuario.getText(), fornec, LotesNovos, txbParcelas.getText()))
        {
            LotesNovos = new ArrayList<Lote>();
            fornecf = 0;
            fornec = "";
            txbParcelas.setText("1");
            estadoOriginal();
        }
        CarregaTabela("");
    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        estadoOriginal();
    }

    @FXML
    private void ClicknaTabelaLotes(MouseEvent event)
    {
        int lin = tbLote.getSelectionModel().getSelectedIndex();
        Lote l;
        linLote = lin;
        l = tbLote.getItems().get(lin);
        index = l;
        txbQuantidade.setText(Integer.toString(index.getEstoque()));
        txbProduto.setText(index.getNome());
        txbLote.setText(index.getCodigo());
        txbPreco.setText(Double.toString(index.getProduto().getPrecoCom()));
        txbObs.setText(index.getProduto().getObs());
        txbValorTotal.setText(Double.toString(ValorTotal));
        btnAlterarCarrinho.setDisable(false);
        btnExcluirCarrinho.setDisable(false);
        btnAdicionarCarrinho.setDisable(true);
        btnCancelar.setDisable(false);
        flag = 0;
    }

    @FXML
    private void evtExcluirCarrinho(ActionEvent event)
    {
        int i;
        for (i = 0; i < LotesNovos.size() && !LotesNovos.get(i).getCodigo().equals(txbLote.getText()); i++)
        {
        };
        if (LotesNovos.get(i).getCodigo().equals(txbLote.getText()))
            LotesNovos.remove(i);
        if (LotesNovos.isEmpty())
        {
            fornecf = 0;
            fornec = "";
        }
        CarregaTabela("");
        estadoOriginal();
    }

    private void estadoOriginal()
    {
        pndados.setDisable(true);
        btnAlterarCarrinho.setDisable(true);
        btnExcluirCarrinho.setDisable(true);
        btnCancelar.setDisable(false);
        btnCompra.setDisable(false);
        btnConfirma.setDisable(true);
        btnAdicionarCarrinho.setDisable(false);
        txbValorTotal.setText(Double.toString(ValorTotal));
        if (fornecf == 1)
        {
            txbFornecedor.setText(fornecedor.getNome());
            txbFornecedor.setDisable(true);
        } else
            txbFornecedor.setDisable(false);

        clear(pndados.getChildren());
        clear(pndados1.getChildren());
        clear(pndados2.getChildren());

        if (Count < 1)
            btnCompra.setDisable(true);
        else
            btnCompra.setDisable(false);
        txbLote.setText(Integer.toString(Count));
        txbUsuario.setText(CtrlAcesso.getControle().getTxUsuarioInterface().getText());
        txbParcelas.setText(txbParcelas.getText().isEmpty() ? "1" : txbParcelas.getText());
        geraValorTotal();

        CarregaTabela("");
    }

    public void clear(ObservableList<Node> pn)
    {
        ObservableList<Node> componentes = pn; //”limpa” os componentes
        for (Node n : componentes)
        {
            if (n instanceof TextInputControl) // textfield, textarea e htmleditor

                ((TextInputControl) n).setText("");
            if (n instanceof ComboBox)
                ((ComboBox) n).getItems().clear();
        }
    }

    public void EstadoEdicao()
    {
        pndados.setDisable(false);
        txbLote.setDisable(true);
        btnAdicionarCarrinho.setDisable(true);
        btnAlterarCarrinho.setDisable(true);
        btnCompra.setDisable(true);
        btnCancelar.setDisable(false);
        btnConfirma.setDisable(false);
        if (fornecf == 1)
        {
            txbFornecedor.setText(fornecedor.getNome());
            txbFornecedor.setDisable(true);
        } else
            txbFornecedor.setDisable(false);
    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        CtrFornecedor cf = new CtrFornecedor();
        CtrPessoa cp = new CtrPessoa();
        Fornecedor fornecedorA = cf.BuscaFornecedorCodigo(fornecedor.getCNPJ());
        Usuario usuario = cp.BuscaUsuario(txbUsuario.getText());
        int Posl = buscaCarrinho(produto.getCodigo());
        if (Integer.parseInt(txbQuantidade.getText()) > 0)
        {
            if (Posl != -1 || flag == 0)
            {
                Lote aux;
                if (flag == 0)
                {
                    if (linLote != -1)
                    {
                        aux = LotesNovos.get(linLote);
                        LotesNovos.remove(linLote);
                        LotesNovos.add(new Lote("" + aux.getCodigo(), null, produto, fornecedorA, usuario, Integer.parseInt(txbQuantidade.getText()), Integer.parseInt(txbQuantidade.getText()), null));
                    }
                } else
                {
                    aux = LotesNovos.get(Posl);
                    LotesNovos.remove(Posl);
                    LotesNovos.add(new Lote("" + aux.getCodigo(), null, produto, fornecedorA, usuario, Integer.parseInt(txbQuantidade.getText()) + aux.getEstoque(), Integer.parseInt(txbQuantidade.getText()) + aux.getEstoque(), null));
                }
            } else if (flag == 1)
            {
                LotesNovos.add(new Lote("" + Count++, null, produto, fornecedorA, usuario, Integer.parseInt(txbQuantidade.getText()), Integer.parseInt(txbQuantidade.getText()), null));
                fornec = fornecedorA.getCNPJ();
                fornecf = 1;
            }
            estadoOriginal();
        }
    }

    @FXML
    private void evtConsultaProduto()
    {
        Parent root = null;
        try
        {
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/farmacia/ui/Consulta/TelaConsultaProduto.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("farmacia/ui/estilo/tema1.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.showAndWait();
            produto = TelaConsultaProdutoController.produto;
            if (produto != null)
            {
                txbProduto.setText(produto.getNome());
                txbPreco.setText(Double.toString(produto.getPrecoCom()));
                txbObs.setText(produto.getObs());
            }

        } catch (IOException ex)
        {
            Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void evtConsultaFornecedor()
    {
        Parent root = null;
        try
        {
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/farmacia/ui/Consulta/TelaConsultaFornecedor.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("farmacia/ui/estilo/tema1.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.showAndWait();
            fornecedor = TelaConsultaFornecedorController.fornecedor;
            if (fornecedor != null)
                txbFornecedor.setText(fornecedor.getNome());
        } catch (IOException ex)
        {
            Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private int buscaCarrinho(String text)
    {
        int i = 0;
        while (i < LotesNovos.size() && !LotesNovos.get(i).getProduto().getCodigo().equals(text))
        {
            i++;
        }
        if (i < LotesNovos.size())
            return i;
        return -1;
    }

    private void geraValorTotal()
    {
        ValorTotal = 0;
        for (int i = 0; i < LotesNovos.size(); i++)
        {
            ValorTotal += LotesNovos.get(i).getProduto().getPrecoFab() * LotesNovos.get(i).getEstoque();
        }
        txbValorTotal.setText(Double.toString(ValorTotal));
    }

    @FXML
    private void evtCancelarCompra(ActionEvent event)
    {
        Parent root = null;
        try
        {
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/farmacia/ui/Consulta/TelaConsultaCompra.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("farmacia/ui/estilo/tema1.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.showAndWait();
            compra = TelaConsultaCompraController.compra;
            if (compra != null)
            {
                CtrlCompra cc = new CtrlCompra();
                cc.cancelarCompra(compra.getCodigo());
            }
        } catch (IOException ex)
        {
            Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
