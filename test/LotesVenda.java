
import engenharia.entidades.Revisar.Lote;
import Produto.Entidade.Produto;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Raizen
 */
public class LotesVenda
{

    private String Codigo;
    private Lote lote;
    private Compra compra;
    private Produto produto;
    private Venda venda;
    private Usuario usuario;
    private Fornecedor fornecedor;
    private Cliente cliente;
    private int Quantidade;
    private String prodN;
    private String lotN;

    public LotesVenda()
    {
    }

    public LotesVenda(String Codigo, Lote lote, Compra compra, Produto produto,
            Venda venda, Usuario usuario, Fornecedor fornecedor, Cliente cliente, int Quantidade)
    {
        this.Codigo = Codigo;
        this.lote = lote;
        this.compra = compra;
        this.produto = produto;
        this.venda = venda;
        this.usuario = usuario;
        this.fornecedor = fornecedor;
        this.cliente = cliente;
        this.Quantidade = Quantidade;
        if (produto != null)
            prodN = produto.getNome();
        else
            prodN = "Não tem";
        if (lote != null)
        {
            lotN = lote.getCodigo();
            lotN = "Não tem";
        }
    }

    public void setProdN(String prodN)
    {
        this.prodN = prodN;
    }

    public void setLotN(String lotN)
    {
        this.lotN = lotN;
    }

    public String getProdN()
    {
        return prodN;
    }

    public String getLotN()
    {
        return lotN;
    }

    public void setQuantidade(int Quantidade)
    {
        this.Quantidade = Quantidade;
    }

    public int getQuantidade()
    {
        return Quantidade;
    }

    public String getCodigo()
    {
        return Codigo;
    }

    public Lote getLote()
    {
        return lote;
    }

    public Compra getCompra()
    {
        return compra;
    }

    public Produto getProduto()
    {
        return produto;
    }

    public Venda getVenda()
    {
        return venda;
    }

    public Usuario getUsuario()
    {
        return usuario;
    }

    public Fornecedor getFornecedor()
    {
        return fornecedor;
    }

    public Cliente getCliente()
    {
        return cliente;
    }

    public void setCodigo(String Codigo)
    {
        this.Codigo = Codigo;
    }

    public void setLote(Lote lote)
    {
        this.lote = lote;
        if (lote != null)
        {
            lotN = lote.getCodigo();
            lotN = "Não tem";
        }
    }

    public void setCompra(Compra compra)
    {
        this.compra = compra;
    }

    public void setProduto(Produto produto)
    {
        this.produto = produto;
        if (produto != null)
            prodN = produto.getNome();
        else
            prodN = "Não tem";
    }

    public void setVenda(Venda venda)
    {
        this.venda = venda;
    }

    public void setUsuario(Usuario usuario)
    {
        this.usuario = usuario;
    }

    public void setFornecedor(Fornecedor fornecedor)
    {
        this.fornecedor = fornecedor;
    }

    public void setCliente(Cliente cliente)
    {
        this.cliente = cliente;
    }

}
