/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import farmacia.controladoras.CtrlRecebimento;
import farmacia.ui.Consulta.TelaConsultaVendaController;
import farmacia.entidades.Produto;
import farmacia.entidades.Recebimento;
import farmacia.entidades.Venda;
import farmacia.entidades.controle.CtrLote;
import farmacia.entidades.controle.CtrRecebimento;
import farmacia.ui.MainFXMLController;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Drake
 */
public class TelaRecebimentoController implements Initializable
{

    @FXML
    private Button btnNovo;
    @FXML
    private Button btconfirmar;
    @FXML
    private Button btcancelar;
    @FXML
    private TextField txBusca;
    @FXML
    private TableView<Recebimento> tbRecebimento;
    @FXML
    private TableColumn<Recebimento, String> tcCodigo;
    @FXML
    private TableColumn<Recebimento, String> tcVenda;
    @FXML
    private TableColumn<Recebimento, String> tcCliente;
    @FXML
    private TableColumn<Recebimento, String> tcValor;
    @FXML
    private TextField txbVenda;
    @FXML
    private VBox pndados;
    private Recebimento recebimento;
    private Venda venda;
    @FXML
    private TextField txbRecebimento;
    private int flag;
    @FXML
    private Button btcancelarRecebimento;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcVenda.setCellValueFactory(new PropertyValueFactory("VendaC"));
        tcCodigo.setCellValueFactory(new PropertyValueFactory("Codigo"));
        tcValor.setCellValueFactory(new PropertyValueFactory("Valor"));
        tcCliente.setCellValueFactory(new PropertyValueFactory("clienteC"));
        venda = null;
        recebimento = null;
        flag = 0;
        estadoOriginal();
        /////CarregaTabela("",2);
    }

    public void EstadoEdicao()
    {
        btnNovo.setDisable(true);
        btconfirmar.setDisable(false);
        pndados.setDisable(false);
        btcancelar.setDisable(false);
        btcancelarRecebimento.setDisable(true);
    }

    private void CarregaTabela(String filtro, int fl)
    {
        CtrRecebimento ctm = new CtrRecebimento();
        try
        {
            tbRecebimento.setItems(ctm.getRecebimento(filtro, fl));
        } catch (Exception ex)
        {

        }
    }

    private void estadoOriginal()
    {
        btcancelar.setDisable(true);
        btconfirmar.setDisable(true);
        btnNovo.setDisable(false);
        pndados.setDisable(true);
        btcancelarRecebimento.setDisable(false);
        ObservableList<Node> componentes = pndados.getChildren(); //”limpa” os componentes
        for (Node n : componentes)
        {
            if (n instanceof TextInputControl) // textfield, textarea e htmleditor

                ((TextInputControl) n).setText("");
            if (n instanceof ComboBox)
                ((ComboBox) n).getItems().clear();
        }
        /////CarregaTabela("",2);
        tbRecebimento.setItems(null);
        flag = 1;
    }

    @FXML
    private void evtNovo(ActionEvent event)
    {
        EstadoEdicao();
        flag = 1;
    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        CtrlRecebimento cr = new CtrlRecebimento();
        if (flag == 1)
            recebimento.setData(LocalDate.now().toString());
        else
            recebimento.setData("");

        cr.receber(recebimento);
        /////estadoOriginal();
        /////A Rever
        if (venda != null)
        {
            txbVenda.setText(venda.getCodigo());
            if (flag == 1)
                CarregaTabela(venda.getCodigo(), 1);
            else
                CarregaTabela(venda.getCodigo(), 3);
        }
    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        estadoOriginal();
    }

    @FXML
    private void evtBuscar()
    {
        CarregaTabela(txBusca.getText(), 2);
    }

    @FXML
    private void ClicknaTabela(MouseEvent event)
    {
        int lin = tbRecebimento.getSelectionModel().getSelectedIndex();
        recebimento = tbRecebimento.getItems().get(lin);
        txbRecebimento.setText(recebimento.getCodigo());
        btnNovo.setDisable(true);
        btcancelar.setDisable(false);
    }

    @FXML
    private void evtConsultaVenda(MouseEvent event)
    {
        Parent root = null;
        try
        {
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/farmacia/ui/Consulta/TelaConsultaVenda.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("farmacia/ui/estilo/tema1.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.showAndWait();
            venda = TelaConsultaVendaController.venda;
            if (venda != null)
            {
                txbVenda.setText(venda.getCodigo());
                if (flag == 1)
                    CarregaTabela(venda.getCodigo(), 1);
                else
                    CarregaTabela(venda.getCodigo(), 3);
            }
        } catch (IOException ex)
        {
            Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void evtCancelarRecebimento(ActionEvent event)
    {
        EstadoEdicao();
        flag = 0;
    }

}
