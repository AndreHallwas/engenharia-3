/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javafx.scene.control.Accordion;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;

/**
 *
 * @author Aluno
 */
public class Acesso
{

    public HBox _pndados;
    public Label _txUsuarioInterface;
    public Label _txNivelInterface;
    public ProgressBar _pbProgresso;
    public HBox _Menu;
    public Accordion _MenuLateral;
    public TitledPane _TabCadastro;
    public Label _opcUsuario;
    public Label _opcCliente;
    public Label _opcFornecedor;
    public Label _opcProduto;
    public TitledPane _TabFuncoes;
    public TitledPane _TabRelatorio;
    public Label opcBackup;
    public Label opcRestore;
    public TitledPane TabGerenciamento;

    public Acesso(HBox _pndados, Label _txUsuarioInterface, Label _txNivelInterface, ProgressBar _pbProgresso, HBox _Menu, Accordion _MenuLateral, TitledPane _TabCadastro, Label _opcUsuario, Label _opcCliente, Label _opcFornecedor, Label _opcProduto, TitledPane _TabFuncoes, TitledPane _TabRelatorio, Label opcBackup, Label opcRestore, TitledPane TabGerenciamento)
    {
        this._pndados = _pndados;
        this._txUsuarioInterface = _txUsuarioInterface;
        this._txNivelInterface = _txNivelInterface;
        this._pbProgresso = _pbProgresso;
        this._Menu = _Menu;
        this._MenuLateral = _MenuLateral;
        this._TabCadastro = _TabCadastro;
        this._opcUsuario = _opcUsuario;
        this._opcCliente = _opcCliente;
        this._opcFornecedor = _opcFornecedor;
        this._opcProduto = _opcProduto;
        this._TabFuncoes = _TabFuncoes;
        this._TabRelatorio = _TabRelatorio;
        this.opcBackup = opcBackup;
        this.opcRestore = opcRestore;
        this.TabGerenciamento = TabGerenciamento;
    }

    public Acesso()
    {
    }

    public Acesso(HBox _pndados, Label _txUsuarioInterface, Label _txNivelInterface, ProgressBar _pbProgresso, HBox _Menu, Accordion _MenuLateral, TitledPane _TabCadastro, Label _opcUsuario, Label _opcCliente, Label _opcFornecedor, Label _opcProduto, TitledPane _TabFuncoes, TitledPane _TabRelatorio)
    {
        this._pndados = _pndados;
        this._txUsuarioInterface = _txUsuarioInterface;
        this._txNivelInterface = _txNivelInterface;
        this._pbProgresso = _pbProgresso;
        this._Menu = _Menu;
        this._MenuLateral = _MenuLateral;
        this._TabCadastro = _TabCadastro;
        this._opcUsuario = _opcUsuario;
        this._opcCliente = _opcCliente;
        this._opcFornecedor = _opcFornecedor;
        this._opcProduto = _opcProduto;
        this._TabFuncoes = _TabFuncoes;
        this._TabRelatorio = _TabRelatorio;
    }

    public HBox getPndados()
    {
        return _pndados;
    }

    public void setPndados(HBox _pndados)
    {
        this._pndados = _pndados;
    }

    public void setTxUsuarioInterface(Label _txUsuarioInterface)
    {
        this._txUsuarioInterface = _txUsuarioInterface;
    }

    public void setTxNivelInterface(Label _txNivelInterface)
    {
        this._txNivelInterface = _txNivelInterface;
    }

    public void setPbProgresso(ProgressBar _pbProgresso)
    {
        this._pbProgresso = _pbProgresso;
    }

    public void setMenu(HBox _Menu)
    {
        this._Menu = _Menu;
    }

    public void setMenuLateral(Accordion _MenuLateral)
    {
        this._MenuLateral = _MenuLateral;
    }

    public void setOpcBackup(Label opcBackup)
    {
        this.opcBackup = opcBackup;
    }

    public void setOpcRestore(Label opcRestore)
    {
        this.opcRestore = opcRestore;
    }

    public void setTabGerenciamento(TitledPane TabGerenciamento)
    {
        this.TabGerenciamento = TabGerenciamento;
    }

    public Label getOpcBackup()
    {
        return opcBackup;
    }

    public Label getOpcRestore()
    {
        return opcRestore;
    }

    public TitledPane getTabGerenciamento()
    {
        return TabGerenciamento;
    }

    public void setTabCadastro(TitledPane _TabCadastro)
    {
        this._TabCadastro = _TabCadastro;
    }

    public void setOpcUsuario(Label _opcUsuario)
    {
        this._opcUsuario = _opcUsuario;
    }

    public void setOpcCliente(Label _opcCliente)
    {
        this._opcCliente = _opcCliente;
    }

    public void setOpcFornecedor(Label _opcFornecedor)
    {
        this._opcFornecedor = _opcFornecedor;
    }

    public void setOpcProduto(Label _opcProduto)
    {
        this._opcProduto = _opcProduto;
    }

    public void setTabFuncoes(TitledPane _TabFuncoes)
    {
        this._TabFuncoes = _TabFuncoes;
    }

    public void setTabRelatorio(TitledPane _TabRelatorio)
    {
        this._TabRelatorio = _TabRelatorio;
    }

    public Label getTxUsuarioInterface()
    {
        return _txUsuarioInterface;
    }

    public Label getTxNivelInterface()
    {
        return _txNivelInterface;
    }

    public ProgressBar getPbProgresso()
    {
        return _pbProgresso;
    }

    public HBox getMenu()
    {
        return _Menu;
    }

    public Accordion getMenuLateral()
    {
        return _MenuLateral;
    }

    public TitledPane getTabCadastro()
    {
        return _TabCadastro;
    }

    public Label getOpcUsuario()
    {
        return _opcUsuario;
    }

    public Label getOpcCliente()
    {
        return _opcCliente;
    }

    public Label getOpcFornecedor()
    {
        return _opcFornecedor;
    }

    public Label getOpcProduto()
    {
        return _opcProduto;
    }

    public TitledPane getTabFuncoes()
    {
        return _TabFuncoes;
    }

    public TitledPane getTabRelatorio()
    {
        return _TabRelatorio;
    }

}
