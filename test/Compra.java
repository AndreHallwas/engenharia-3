
import Fornecedor.Entidade.Fornecedor;
import Pessoa.Entidade.Usuario;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Drake
 */
public class Compra
{

    private String Codigo;
    private Fornecedor fornecedor;
    private Usuario usuario;
    private double compra_valor;
    private int compra_quantLotes;
    private String For;

    public Compra()
    {

    }

    public Compra(String Codigo, Fornecedor fornecedor, Usuario usuario, double compra_valor, int compra_quantLotes)
    {
        this.Codigo = Codigo;
        this.fornecedor = fornecedor;
        this.usuario = usuario;
        this.compra_valor = compra_valor;
        this.compra_quantLotes = compra_quantLotes;
        if (fornecedor != null)
            For = fornecedor.getCNPJ();
        else
            For = "Nenhum";
    }

    public Compra(Fornecedor fornecedor, Usuario usuario, double compra_valor, int compra_quantLotes)
    {
        this.Codigo = Codigo;
        this.fornecedor = fornecedor;
        this.usuario = usuario;
        this.compra_valor = compra_valor;
        this.compra_quantLotes = compra_quantLotes;
        if (fornecedor != null)
            For = fornecedor.getCNPJ();
        else
            For = "Nenhum";
    }

    public void setFor(String For)
    {
        this.For = For;
    }

    public String getFor()
    {
        return For;
    }

    public void setCodigo(String Codigo)
    {
        this.Codigo = Codigo;
    }

    public void setFornecedor(Fornecedor fornecedor)
    {
        this.fornecedor = fornecedor;
        if (fornecedor != null)
            For = fornecedor.getCNPJ();
        else
            For = "Nenhum";
    }

    public void setUsuario(Usuario usuario)
    {
        this.usuario = usuario;
    }

    public void setCompra_valor(double compra_valor)
    {
        this.compra_valor = compra_valor;
    }

    public void setCompra_quantLotes(int compra_quantLotes)
    {
        this.compra_quantLotes = compra_quantLotes;
    }

    public String getCodigo()
    {
        return Codigo;
    }

    public Fornecedor getFornecedor()
    {
        return fornecedor;
    }

    public Usuario getUsuario()
    {
        return usuario;
    }

    public double getCompra_valor()
    {
        return compra_valor;
    }

    public int getCompra_quantLotes()
    {
        return compra_quantLotes;
    }

}
