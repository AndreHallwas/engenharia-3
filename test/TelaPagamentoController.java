/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import farmacia.controladoras.CtrlPagamento;
import farmacia.entidades.Compra;
import farmacia.entidades.Pagamento;
import farmacia.entidades.controle.CtrPagamento;
import farmacia.ui.Consulta.TelaConsultaCompraController;
import farmacia.ui.MainFXMLController;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaPagamentoController implements Initializable
{

    @FXML
    private TableColumn<Pagamento, String> tcCodigo;
    @FXML
    private TableColumn<Pagamento, String> tcCompra;
    @FXML
    private TableColumn<Pagamento, String> tcFornecedor;
    @FXML
    private TableColumn<Pagamento, String> tcValor;
    @FXML
    private TableColumn<Pagamento, String> tcData;
    @FXML
    private VBox pndados;
    @FXML
    private TextField txbCompra;
    @FXML
    private TextField txbPagamento;
    @FXML
    private Button btnNovo;
    @FXML
    private Button btcancelarPagamento;
    @FXML
    private Button btconfirmar;
    @FXML
    private Button btcancelar;
    @FXML
    private TextField txBusca;
    @FXML
    private TableView<Pagamento> tbPagamento;
    private Pagamento pagamento;
    private Compra compra;
    private int flag;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        tcCodigo.setCellValueFactory(new PropertyValueFactory("Codigo"));
        tcCompra.setCellValueFactory(new PropertyValueFactory("compraC"));
        tcData.setCellValueFactory(new PropertyValueFactory("Data"));
        tcFornecedor.setCellValueFactory(new PropertyValueFactory("fornecedorC"));
        tcValor.setCellValueFactory(new PropertyValueFactory("pag_valor"));
        compra = null;
        pagamento = null;
        flag = 0;
        estadoOriginal();
    }

    public void EstadoEdicao()
    {
        btnNovo.setDisable(true);
        btconfirmar.setDisable(false);
        pndados.setDisable(false);
        btcancelar.setDisable(false);
        btcancelarPagamento.setDisable(true);
    }

    private void CarregaTabela(String filtro, int fl)
    {
        CtrPagamento ctm = new CtrPagamento();
        try
        {
            tbPagamento.setItems(ctm.getPagamento(filtro, fl));
        } catch (Exception ex)
        {

        }
    }

    private void estadoOriginal()
    {
        btcancelar.setDisable(true);
        btconfirmar.setDisable(true);
        btnNovo.setDisable(false);
        pndados.setDisable(true);
        btcancelarPagamento.setDisable(false);
        ObservableList<Node> componentes = pndados.getChildren(); //”limpa” os componentes
        for (Node n : componentes)
        {
            if (n instanceof TextInputControl) // textfield, textarea e htmleditor

                ((TextInputControl) n).setText("");
            if (n instanceof ComboBox)
                ((ComboBox) n).getItems().clear();
        }
        /////CarregaTabela("",2);
        tbPagamento.setItems(null);
        flag = 1;
    }

    @FXML
    private void ClicknaTabela(MouseEvent event)
    {
        int lin = tbPagamento.getSelectionModel().getSelectedIndex();
        pagamento = tbPagamento.getItems().get(lin);
        txbPagamento.setText(pagamento.getCodigo());
        btnNovo.setDisable(true);
        btcancelar.setDisable(false);
    }

    @FXML
    private void evtConsultaVenda(MouseEvent event)
    {
        Parent root = null;
        try
        {
            Stage stage = new Stage();
            root = FXMLLoader.load(getClass().getResource("/farmacia/ui/Consulta/TelaConsultaCompra.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("farmacia/ui/estilo/tema1.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.showAndWait();
            compra = TelaConsultaCompraController.compra;
            if (compra != null)
            {
                txbCompra.setText(compra.getCodigo());
                if (flag == 1)
                    CarregaTabela(compra.getCodigo(), 1);
                else
                    CarregaTabela(compra.getCodigo(), 3);
            }
        } catch (IOException ex)
        {
            Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void evtNovo(ActionEvent event)
    {
        EstadoEdicao();
        flag = 1;
    }

    @FXML
    private void evtCancelarPagamento(ActionEvent event)
    {
        EstadoEdicao();
        flag = 0;
    }

    @FXML
    private void evtConfirmar(ActionEvent event)
    {
        CtrlPagamento cp = new CtrlPagamento();
        if (flag == 1)
            pagamento.setData(LocalDate.now().toString());
        else
            pagamento.setData("");

        cp.Pagar(pagamento);
        if (compra != null)
        {
            txbCompra.setText(compra.getCodigo());
            if (flag == 1)
                CarregaTabela(compra.getCodigo(), 1);
            else
                CarregaTabela(compra.getCodigo(), 3);
        }
    }

    @FXML
    private void evtCancelar(ActionEvent event)
    {
        estadoOriginal();
    }

    @FXML
    private void evtBuscar()
    {
        CarregaTabela(txBusca.getText(), 4);
    }

}
