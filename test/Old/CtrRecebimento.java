package OldTransacao;


import OldTransacao.CtrlVenda;
import OldTransacao.Recebimento;
import OldTransacao.Venda;
import Padroes.Dao.ControledeEntidade;
import Utils.Banco;
import Utils.Mensagem;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


public class CtrRecebimento extends ControledeEntidade
{

    public CtrRecebimento() {
        flag = false;
        pstmt = null;
    }
    
    public boolean Salvar(Recebimento f)
    {
        
         pstmt = Utils.Banco.con.geraStatement(
                "insert into Recebimento(venda_cod, rec_data, rec_valor) values(?,?,?)");

        setParametros(f);
        setMsg(Utils.Banco.con.manipular(pstmt), "Cadastro");

        return flag;
    }

    public boolean Alterar(Recebimento f)
    {
        pstmt = Utils.Banco.con.geraStatement("update Recebimento set venda_cod = ?, rec_data = ?, rec_valor = ? where rec_cod = '" + f.getCodigo() + "'");

        setParametros(f);
        setMsg(Utils.Banco.con.manipular(pstmt), "Alteração");

        return flag;
    }

    public boolean Apagar(Recebimento f)
    {
        pstmt = Utils.Banco.con.geraStatement("delete from Recebimento where venda_cod = '" + f.getVenda().getCodigo() + "'");

        setMsg(Utils.Banco.con.manipular(pstmt), "Exclusão");

        return flag;
    }
    
    private void setParametros(Recebimento n)
    {
        int count = 0;
        try
        {
            pstmt.setString(++count, n.getVenda().getCodigo());
            pstmt.setString(++count, n.getData() == null ? "" : n.getData());
            pstmt.setString(++count, Double.toString(n.getValor()));
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex);
        }
    }

    public ArrayList<Recebimento> get(String filtro, int op)
    {
        String sql = null;
        ArrayList<Recebimento> lista = new ArrayList();
        ResultSet rs;
        
        sql = "select * from Recebimento";
        if (!filtro.isEmpty()){
            if (op == 1)
            {
                    sql += " where venda_cod = '" + filtro + "' and rec_data is null or rec_data = ''";
            } else if (op == 2)
            {
                    sql += " where rec_cod = '" + filtro + "' and rec_data is null or rec_data = ''";
            } else if (op == 3)
            {
                    sql += " where venda_cod = '" + filtro + "' and rec_data is not null and rec_data <> ''";
            } else if (op == 4){
                    sql += " where venda_cod = '" + filtro + "'";
            } else{
                    sql += " where venda_cod = '" + filtro + "' or upper(venda_cod) like upper('" + filtro+ "%')";
            }
        }
        
        rs = Banco.con.consultar(sql);
        try
        {
            CtrlVenda cv = CtrlVenda.create();
            while (rs.next())
            {
                lista.add(new Recebimento(rs.getString("rec_cod"), (Venda) cv.Pesquisar(rs.getString("venda_cod")).get(0), rs.getDouble("rec_valor"), rs.getString("rec_data")));
            }
        } catch (Exception ex)
        {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }
        return lista;
    }
}
