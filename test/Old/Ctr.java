/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Old;

import Old.Entidade;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
interface Ctr
{

    public boolean Salvar(Entidade e);

    public boolean Alterar(Entidade e);

    public boolean Apagar(Entidade e);

    public ArrayList<Object> get(String codigo);
}
