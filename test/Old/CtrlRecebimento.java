package OldTransacao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import OldTransacao.Recebimento;
import OldTransacao.Venda;
import OldTransacao.CtrRecebimento;
import java.util.ArrayList;

/**
 *
 * @author Drake
 */
public class CtrlRecebimento
{

    public static CtrlRecebimento create() {
        return new CtrlRecebimento();
    }
    private CtrRecebimento cr;

    private CtrlRecebimento() {
    }
    public ArrayList<Object> Pesquisar(String filtro)
    {
        cr = new CtrRecebimento();
        ArrayList<Object> Recebimentos = new ArrayList();
        Recebimentos.addAll(cr.get(filtro, 4));
        return Recebimentos;
    }

    public void Concretizar(String Codigo, String venda, double Valor, String Data)
    {
        CtrlVenda cv = CtrlVenda.create();
        Venda ven = (Venda) cv.Pesquisar(venda).get(0);
        cr = new CtrRecebimento();
        cr.Alterar(new Recebimento(Codigo, ven, Valor, Data));
    }
}
