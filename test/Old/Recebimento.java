package OldTransacao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Drake
 */
public class Recebimento //Fatura
{

    protected String Codigo;
    protected Venda venda;
   // protected Compra Compra;
    protected double Valor;
    protected String Data;

    public Recebimento()
    {
    }

    public Recebimento(String Codigo, Venda venda, double Valor, String Data) {
        this.Codigo = Codigo;
        this.venda = venda;
        this.Valor = Valor;
        this.Data = Data;
    }

    public String getCodigo() {
        return Codigo;
    }

    public Venda getVenda() {
        return venda;
    }

    public double getValor() {
        return Valor;
    }

    public String getData() {
        return Data;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    public void setValor(double Valor) {
        this.Valor = Valor;
    }

    public void setData(String Data) {
        this.Data = Data;
    }

   
}
