package OldTransacao;


import Pessoa.Entidade.Cliente;
import Pessoa.Entidade.Usuario;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Drake
 */
public class Venda
{

    private String Codigo;
    private Cliente cliente;
    private Usuario usuario;
    private double Valor;
    // lista dos produtos da venda

    public Venda()
    {
    }

    public Venda(String Codigo, Cliente cliente, Usuario usuario, double Valor)
    {
        this.Codigo = Codigo;
        this.cliente = cliente;
        this.usuario = usuario;
        this.Valor = Valor;/*
        if (cliente != null)
            clienteC = cliente.getRG();
        if (usuario != null)
            usuarioC = usuario.getLogin();*/
    }

    public Venda(String Codigo, Cliente cliente, Usuario usuario)
    {
        this.Codigo = Codigo;
        this.cliente = cliente;
        this.usuario = usuario;/*
        if (cliente != null)
            clienteC = cliente.getRG();
        if (usuario != null)
            usuarioC = usuario.getLogin();*/
    }

    public String getCodigo()
    {
        return Codigo;
    }

    public Cliente getCliente()
    {
        return cliente;
    }

    public Usuario getUsuario()
    {
        return usuario;
    }

    public void setCodigo(String Codigo)
    {
        this.Codigo = Codigo;
    }

    public void setCliente(Cliente cliente)
    {
        this.cliente = cliente;
        /*if (cliente != null)
            clienteC = cliente.getRG();*/
    }

    public void setUsuario(Usuario usuario)
    {
        this.usuario = usuario;/*
        if (usuario != null)
            usuarioC = usuario.getLogin();*/
    }

    public double getValor()
    {
        return Valor;
    }

    public void setValor(double Valor)
    {
        this.Valor = Valor;
    }
/*
    public String getClienteC()
    {
        return clienteC;
    }

    public String getUsuarioC()
    {
        return usuarioC;
    }

    public void setClienteC(String clienteC)
    {
        this.clienteC = clienteC;
    }

    public void setUsuarioC(String usuarioC)
    {
        this.usuarioC = usuarioC;
    }
*/
}
