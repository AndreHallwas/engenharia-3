package OldTransacao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Pessoa.Controladora.CtrlPessoa;
import Pessoa.Entidade.Cliente;
import OldTransacao.ProdutoDaVenda;
import OldTransacao.Recebimento;
import OldTransacao.Venda;
import Pessoa.Entidade.Usuario;
import Produto.Dao.CtrLote;
import OldTransacao.CtrProdutosDaVenda;
import OldTransacao.CtrRecebimento;
import OldTransacao.CtrVenda;
import Interface.Funcional.TelaVenda;
import Utils.Banco;
import Utils.Carrinho;
import java.util.ArrayList;

/**
 *
 * @author Drake
 */
public class CtrlVenda
{

    public static CtrlVenda create() {
        return new CtrlVenda();
    }
    private CtrVenda cv;

    private CtrlVenda() {
    }

    public boolean cancelar(String Vendacod)
    {
        CtrVenda cv = new CtrVenda();
        return cv.Apagar(new Venda(Vendacod, null, null));
    }

    public ArrayList<Object> Pesquisar(String filtro)
    {
        cv = new CtrVenda();
        ArrayList<Object> Vendas = new ArrayList();
        Vendas.addAll(cv.get(filtro, 1));
        return Vendas;
    }

    public boolean registar(String Usr, String cliente, String Parcelas, Carrinho carrinho) {
        CtrlPessoa cp = CtrlPessoa.create();
        ArrayList<Object> pessoas = cp.Pesquisar(Usr, 1);
        if(!pessoas.isEmpty()){
            /////Registra Venda
            CtrVenda cv = new CtrVenda();
            Venda v = new Venda(Parcelas, (Cliente) cp.Pesquisar(cliente, 0).get(0), 
                    (Usuario) pessoas.get(0), carrinho.getValorTotal());
            cv.Salvar(v);
            v.setValor(Banco.con.getMaxPK("venda", "venda_cod"));
            
            CtrProdutosDaVenda cvv = new CtrProdutosDaVenda();
            CtrLote cl = new CtrLote();
            ArrayList<TelaVenda> tv = carrinho.geraLotes();
            ProdutoDaVenda pv;
            for(TelaVenda elem : tv){
                pv = new ProdutoDaVenda(v, elem.getLote(), elem.getProduto(), elem.getEstoque());
                elem.getLote().setEstoque(elem.getLote().getEstoque()-Integer.parseInt(elem.getEstoque()));
                cl.Alterar(elem.getLote(), elem.getProduto());
                cvv.Salvar(pv);
            }
            CtrRecebimento cr = new CtrRecebimento();
            double Valor = carrinho.getValorTotal()/Integer.parseInt(Parcelas);
            for (int i = 0; i < Integer.parseInt(Parcelas); i++) {
                cr.Salvar(new Recebimento(null, v, Valor, null));
            }
            return true;
        }
        return false;
    }
}
