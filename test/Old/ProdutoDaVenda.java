/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OldTransacao;

import Produto.Entidade.Lote;
import Produto.Entidade.Produto;
import OldTransacao.Venda;

/**
 *
 * @author Raizen
 */
public class ProdutoDaVenda {
    private Venda venda;
    private Lote lote;
    private Object produto;
    private String Quantidade;

    public ProdutoDaVenda(Venda venda, Lote lote, Object produto, String Quantidade) {
        this.venda = venda;
        this.lote = lote;
        this.produto = produto;
        this.Quantidade = Quantidade;
    }

    public Venda getVenda() {
        return venda;
    }

    public Lote getLote() {
        return lote;
    }

    public Object getProduto() {
        return produto;
    }

    public String getQuantidade() {
        return Quantidade;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    public void setLote(Lote lote) {
        this.lote = lote;
    }

    public void setProduto(Object produto) {
        this.produto = produto;
    }

    public void setQuantidade(String Quantidade) {
        this.Quantidade = Quantidade;
    }
    
    
}
