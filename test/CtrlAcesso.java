/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import farmacia.entidades.Acesso;
import farmacia.ui.TelaLoginFXMLController;
import farmacia.util.Banco;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;

/**
 *
 * @author Aluno
 */
public class CtrlAcesso
{

    public static Acesso Controle;

    public static void incializa(HBox _Menu, Accordion _MenuLateral,
            Label _opcCliente, Label _opcFornecedor, Label _opcProduto,
            Label _opcUsuario, ProgressBar _pbProgresso, HBox _pndados,
            TitledPane _TabFuncoes, TitledPane _TabCadastro, TitledPane _TabRelatorio,
            Label _txNivelInterface, Label _txUsuarioInterface, Label opcRestore, Label opcBackup, TitledPane TabGerenciamento)
    {

        Controle = new Acesso();
        Controle.setMenu(_Menu);
        Controle.setMenuLateral(_MenuLateral);
        Controle.setOpcCliente(_opcCliente);
        Controle.setOpcFornecedor(_opcFornecedor);
        Controle.setOpcProduto(_opcProduto);
        Controle.setOpcUsuario(_opcUsuario);
        Controle.setPbProgresso(_pbProgresso);
        Controle.setPndados(_pndados);
        Controle.setTabFuncoes(_TabFuncoes);
        Controle.setTabCadastro(_TabCadastro);
        Controle.setTabRelatorio(_TabRelatorio);
        Controle.setTxNivelInterface(_txNivelInterface);
        Controle.setTxUsuarioInterface(_txUsuarioInterface);
        Controle.setTabGerenciamento(TabGerenciamento);
        Controle.setOpcBackup(opcBackup);
        Controle.setOpcRestore(opcRestore);

        Default();
    }

    public static boolean logar(String Senha, String Usuario)
    {
        Boolean auxRetorno = false;
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        String sql = "select *from usuario where usr_login = '" + Usuario
                + "' and usr_senha = '" + Senha + "'";
        ResultSet rs = Banco.con.consultar(sql);
        if (rs != null)
            try
            {
                if (rs.next())
                {
                    String aux = rs.getString("usr_nivel");
                    a.setContentText("Login Efetuado");
                    Controle.getPndados().getChildren().clear();
                    Controle.getTxUsuarioInterface().setText(Usuario);
                    Controle.getTxNivelInterface().setText(
                            aux.charAt(0) == '0' ? "Funcionário"
                            : aux.charAt(0) == '1' ? "Administrador"
                            : aux.charAt(0) == '2' ? "Gerente" : "seila");
                    selecionaPermissao();
                    auxRetorno = true;

                } else
                {
                    a = new Alert(Alert.AlertType.INFORMATION);
                    a.setContentText("Login Não Efetuado");
                }
            } catch (SQLException ex)
            {
                Logger.getLogger(TelaLoginFXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }
        a.showAndWait();
        return auxRetorno;
    }

    public static void MarcaPermissao(boolean TabFuncoes, boolean MenuLateral, boolean TabCadastro, boolean tabRelatorio, boolean opcCliente, boolean opcFornecedor, boolean opcProduto, boolean opcUsuario, boolean Menu, boolean TabGerenciamento, boolean opcBackup, boolean opcRestore)
    {
        MarcaPermissaoFuncoes(TabFuncoes);
        MarcaPermissaoCadastro(TabCadastro, opcCliente, opcFornecedor, opcProduto, opcUsuario);
        MarcaPermissaoMenuLateral(MenuLateral);
        MarcaPermissaoRelatorio(tabRelatorio);
        MarcaPermissao(TabGerenciamento, opcBackup, opcRestore);
        MarcaPermissaoMenu(Menu);
    }

    public static void MarcaPermissaoCadastro(boolean TabCadastro, boolean opcCliente, boolean opcFornecedor, boolean opcProduto, boolean opcUsuario)
    {
        Controle.getTabCadastro().setVisible(!TabCadastro);
        Controle.getOpcCliente().setVisible(!opcCliente);
        Controle.getOpcFornecedor().setVisible(!opcFornecedor);
        Controle.getOpcProduto().setVisible(!opcProduto);
        Controle.getOpcUsuario().setVisible(!opcUsuario);
    }

    public static void MarcaPermissaoRelatorio(boolean tabRelatorio)
    {
        Controle.getTabRelatorio().setVisible(!tabRelatorio);
    }

    public static void MarcaPermissaoFuncoes(boolean TabFuncoes)
    {
        Controle.getTabFuncoes().setVisible(!TabFuncoes);
    }

    public static void MarcaPermissaoMenuLateral(boolean MenuLateral)
    {
        Controle.getMenuLateral().setVisible(!MenuLateral);
    }

    public static void MarcaPermissaoMenu(boolean Menu)
    {
        Controle.getMenu().setVisible(!Menu);
    }

    public static void MarcaPermissao(boolean TabGerenciamento, boolean opcBackup, boolean opcRestore)
    {
        Controle.getTabGerenciamento().setVisible(!TabGerenciamento);
        Controle.getOpcBackup().setVisible(!opcBackup);
        Controle.getOpcRestore().setVisible(!opcRestore);
    }

    public static void selecionaPermissao()
    {
        if (Controle.getTxNivelInterface().getText() == "Funcionario")
            MarcaPermissao(true, false, false, true, false, true, true, false, false, true, true, true);
        else if (Controle.getTxNivelInterface().getText() == "Administrador")
            MarcaPermissao(false, false, false, true, false, false, false, false, false, false, false, false);
        else if (Controle.getTxNivelInterface().getText() == "Gerente")
            MarcaPermissao(false, false, false, false, false, false, false, false, false, false, false, false);
    }

    public static void Default()
    {
        MarcaPermissao(true, false, false, true, true, true, true, false, false, true, true, true);
    }

    public static void Deslogar()
    {
        MarcaPermissao(true, false, false, true, true, true, true, false, false, true, true, true);
    }

    public static Acesso getControle()
    {
        return Controle;
    }

    public static void DefaultEmpresa()
    {
        MarcaPermissao(false, true, false, false, true, true, true, true, true, false, true, true);
    }

}
